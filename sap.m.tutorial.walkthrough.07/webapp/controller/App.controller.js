sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel"
], function (Controller, MessageToast, JSONModel) {
	"use strict";

	return Controller.extend("sap.ui.demo.walkthrough.controller.App", {

		onInit : function (oEvent) {

			alert('init 1');
			// set data model on view
			var oData = {
				recipient : {
					name : "World"
				}
			};
			var oModel = new JSONModel(oData);
			this.getView().setModel(oModel);


			// maps section //
			//load googlemaps library
			sap.ui.getCore().loadLibrary("openui5.googlemaps", "../dist/openui5/googlemaps/");  // api key is in xml
			//UIComponent.prototype.init.apply(this, arguments);

			//alert('init 2');
			// openui5.googlemaps.ScriptsUtil.setApiKey('AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM');

			jQuery.sap.require('openui5.googlemaps.MapUtils');
			this.util = openui5.googlemaps.MapUtils;  // need require for this

			alert('init 3 ' +  JSON.stringify(  this.util) );

			this.util.search({
				'address': '6006 North Fwy, Houston, TX 77076, USA'
			}).done(jQuery.proxy(this.searchResults, this));
alert('test done');


			// Initial place marker
			var places = [{
				name: "Bondi Beach",
				lat: -33.890542,
				lng: 151.274856,
				markerLat: -33.890542,
				markerLng: 151.274856
			}];

			this.oModel = new sap.ui.model.json.JSONModel();

			this.oModel.setData({
				places: places
			});

			sap.ui.getCore().setModel(this.oModel);

//			this.oContext = this.oModel.getContext('/places/0/');
//			this.byId("form1").setBindingContext(this.oContext);

			//

		},

		onShowHello : function () {
			MessageToast.show("Hello World");
		},

		// maps section
		/*
               Lookup address as we type (after 3 chars). List of results is
               presented to the callback.
               */
		onSuggest: function(oEvent) {

			var sValue = oEvent.getParameter("suggestValue");

			alert('onSuggest ' + sValue);


			if (sValue.length > 3) {
				alert('searching');
				this.util.search({
					"address": sValue
				}).done(jQuery.proxy(this.searchResults, this));
			}
		},

		onChange: function(oEvent) {


			let lat = lng = null;
			if (oEvent) {
				var val = oEvent.getParameters("newValue").newValue;
				var oCtxt = oEvent.getSource().getBindingContext();
				this.locations.forEach(function(oLocation) {
					if (oLocation.value === val) {
						oCtxt.getModel().setProperty("lat", oLocation.lat, oCtxt);
						oCtxt.getModel().setProperty("lng", oLocation.lng, oCtxt);
						oCtxt.getModel().setProperty("markerLat", oLocation.lat, oCtxt);
						oCtxt.getModel().setProperty("markerLng", oLocation.lng, oCtxt);
						oCtxt.getModel().setProperty("name", oLocation.value, oCtxt);
						lat = oLocation.lat; lng = oLocation.lng;
					}
				});
			}

			alert("lat: " +  lat + '. lng ' +  lng);
		},

		/*
        Suggestions callback function which shows the user all the
        matching suggestions.
        We can probably bind to this resultset directly in the XML
        view, but I haven't tried yet.
        */
		searchResults: function(results, status) {
			alert('searchResults');

			var that = this;
			this.locations = [];

			this.locations = jQuery.map(results, function(item) {
				var location = {};
				location.value = item.formatted_address;
				location.lat = item.geometry.location.lat();
				location.lng = item.geometry.location.lng();
				return location;
			});

			var queryInput = this.byId("query");
			queryInput.removeAllSuggestionItems();
			this.locations.forEach(function(item) {
				queryInput.addSuggestionItem(new sap.ui.core.ListItem({
					text: item.value
				}));
			});
		},

		/*
        Update model with location and return a lat/long object
        */
		getLocation: function(oPos) {
			this.oModel.setProperty("lat", oPos.lat, this.oContext);
			this.oModel.setProperty("lng", oPos.lng, this.oContext);
			this.oModel.setProperty("markerLat", oPos.lat, this.oContext);
			this.oModel.setProperty("markerLng", oPos.lng, this.oContext);
			this.oModel.setProperty("name", "My Location", this.oContext);
			return this.util.objToLatLng(oPos);
		},

		updateLocation: function(sLocation) {
			this.oModel.setProperty(this.oContext.getPath() + "name", sLocation);
			this.byId("query").setValue(sLocation);
		},

		/*
        When My Location button is pressed, get the current location
        from Google, then geocode it, then update the mmodel and
        address query input element.
        */
		onMyLocation: function(oEvent) {
			this.util.currentPosition()
				.then(this.getLocation.bind(this))
				.then(this.util.geocodePosition)
				.done(this.updateLocation.bind(this));
		},

		/*
        On marker drag end
        */
		onMarkerDragEnd: function(oEvent) {
			this.util.geocodePosition(oEvent.getParameters().position).done(this.updateLocation.bind(this));
		},

		/*
        Map click handler - call Geo stuff in HANA to get postal boundary polygon - TODO
        */
		onClick: function(oEvent) {
			alert("Clicked. Lat: " + oEvent.getParameters().lat + ", Lng: " + oEvent.getParameters().lng);
		},

		/*
        Get the pinkball icon for the marker
        */
		getMarkerIcon: function() {
			return jQuery.sap.getModulePath("openui5.googlemaps.themes." + "base") + "/img/pinkball.png"
		}


		// -- end of oninit functions
	});

});