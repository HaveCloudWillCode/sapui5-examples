/* The base controller is an abstract controller that will not be instantiated in any view. 
 Therefore, the naming convention *.controller.js does not apply, and we can just call the file BaseController.js. 
 By not using the naming convention *.controller.js we can even prevent any usage in views. */

/* global sap */

sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"jquery.sap.global",
	"sap/ui/core/UIComponent"
], function (Controller, JSONModel, History, jQuery, UIComponent) {
	"use strict";

	return Controller.extend("com.owi.customer5.controller.BaseController", {
		//sub controller onInit should use following code to include super onInit code
		//BaseController.prototype.onInit.call(this); 
		onInit: function () {
			jQuery.sap.log.info("**BaseController.onInit: " + this.getMetadata().getName());

			//include webUser to all views which use BaseController
			let webUserJson = this.getWebUser();
			if (webUserJson) {
				this.getView().setModel(new JSONModel(webUserJson), "webUser");
			} else {
				jQuery.sap.log.info("**User not login, redirecting to login page");
				UIComponent.getRouterFor(this).navTo("login");
			}
		},

		getWebUser: function () {
			var sessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
			var webUser = sessionStorage.get("webUser");
			return webUser;
		},

		setWebUser: function (webUserJson) {
			//get working account with name; set emailAddress shortcut
			var workingAccountName = "";
			if (webUserJson) {
				if (webUserJson.name.firstName) {
					workingAccountName = workingAccountName + webUserJson.name.firstName;
				}
				if (webUserJson.workingAccount) {
					workingAccountName = workingAccountName + " - " + webUserJson.workingAccount.accountId;
				}
				webUserJson.workingAccountName = workingAccountName;
				webUserJson.emailAddress= webUserJson.communication.emailAddress.address;
			}

			// save to session
			var sessionStorage = jQuery.sap.storage(jQuery.sap.storage.Type.session);
			sessionStorage.put("webUser", webUserJson);
			
			//when logout user, clear the sesison
			if (!webUserJson) {
				sessionStorage.clear();
			}
		},

		//get a list of customer linked account
		getCustomerAccounts: function () {
			var customerAccounts = new Array();
			
			var webUserJson = this.getWebUser();
			if (!webUserJson) {
				return customerAccounts;
			}
			
			for(var account of webUserJson.linkedAccount){
				if(account.type === "CM"){
					customerAccounts.push(account);
				}
			}
			return customerAccounts;
		},

		/**
		 * Convenience method for accessing the router.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function () {
			return UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @public
		 * @param {string} [sName] the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		onNavBack: function () {
			var oHistory, sPreviousHash;
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("main", {}, true /*no history*/ );
			}
		},

		/* From view pass in an addtional parm "navTo" for next view. 
		 i.e. press="onNav($event,'support')" */
		onNav: function (oEvent, navTo) {
			jQuery.sap.log.info("**BaseController.navTo:" + navTo);
			if (navTo) {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo(navTo);
			} else {
				if (oEvent) {
					jQuery.sap.log.info("**BaseController.navTo is blank for source: " + oEvent.getSource().getId());
				} else {
					jQuery.sap.log.info("**BaseController.navTo is blank");
				}
			}
		}
	});
});