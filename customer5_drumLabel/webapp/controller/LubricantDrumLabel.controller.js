sap.ui.define(["com/owi/customer5/controller/BaseController",
    "sap/ui/core/mvc/Controller", "sap/ui/model/json/JSONModel", "sap/base/i18n/ResourceBundle", "sap/ui/core/routing/History"
], function (BaseController, Controller, JSONModel, ResourceBundle, History) {
    "use strict";

    return BaseController.extend("com.owi.customer5.controller.LubricantDrumLabel", {
        onInit: function (oEvent) {
            try {
                let owi = ResourceBundle.create({url: "./model/owi.properties", async: false});
                let rootUri = owi.getText("rootUri");
                this.uri = rootUri + owi.getText("lubricantDrumLabel") + "?ftpRoot=lubricantDrumLabel&docName=";
<<<<<<< HEAD
                //alert('attachRequestCompleted. this.uri: ' + this.uri);
=======

                //Mike tester
                //this.uri = "https://www.mocky.io/v2/5c6b0947330000e33c7f4e6d";
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e

                let oModel = new JSONModel(this.uri);
                this.getView().setModel(oModel, 'ftp_docs');  // named model
                // test only:
                oModel.attachRequestCompleted(function () {
<<<<<<< HEAD
                  // alert('attachRequestCompleted. json obj: ' + JSON.stringify(oModel.getData()));
=======
                  alert('attachRequestCompleted json obj: ' + JSON.stringify(oModel.getData()));
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e
                });

            } catch (e) {
                alert('Controller error ' + e);
            }
        },

        handlePress: function (oEvent) {
            try {
                const fileName = oEvent.getSource().getText();
                let uri = this.uri + fileName;
                const fileExt =  fileName.split(".").pop().toLowerCase();
<<<<<<< HEAD
                // alert('handlePress uri: ' + uri + '. fileExt: '+ fileExt);

                let oModel = new JSONModel(uri);
                oModel.attachRequestCompleted(function () {
                    //  alert('doc json obj: ' + JSON.stringify(oModel.getData().data));
                    if (oModel.getData().message.indexOf("FAILED") >= 0){
                        alert('File load failed. Contact IT Support');
                    }
                    switch(fileExt) {
                        case "txt":
                            $(window.open().document.body).html(oModel.getData().data);
                            break;
                        case "pdf":
                            window.open("data:application/pdf;base64, " + oModel.getData().data);
                            break;
                        default:
                            $(window.open().document.body).html(oModel.getData());
                    }
                });
/*
                $.ajax({
                    url: uri,
                    success: function (response,status,xhr) {
                        alert('fileExt: '+ fileExt + '. response: ' + JSON.stringify(response));

                        switch(fileExt) {
                            case "txt":
                                $(window.open().document.body).html(response.data);
                                break;
                            case "pdf":
                                window.open("data:application/pdf;base64, " + response.data);
=======
                alert('handlePress uri: ' + uri);

                $.ajax({
                    url: uri,
                    success: function (response,status,xhr) {
                        switch(fileExt) {
                            case "txt":
                                $(window.open().document.body).html(response);
                                break;
                            case "pdf":
                                window.open("data:application/pdf;base64, " + response);
                                break;
                            case "base64":
                                window.open("data:application/pdf;base64, " + response);
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e
                                break;
                            default:
                                $(window.open().document.body).html(response);
                        }
                    }
                });
<<<<<<< HEAD
*/
=======
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e

            } catch (e) {
                alert('handlePress press error ' + e);
            }
        },
        // Page back <
        onNavBack: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            }
        }
        //
    });
});



