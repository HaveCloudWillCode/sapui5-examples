sap.ui.define(["com/owi/customer5/controller/BaseController",
    "sap/ui/core/mvc/Controller", "sap/ui/model/json/JSONModel", "sap/base/i18n/ResourceBundle",
    'sap/ui/core/HTML', 'sap/ui/commons/Panel'

], function (BaseController, Controller, JSONModel, ResourceBundle, HTML,Panel) {
    "use strict";

    return BaseController.extend("com.owi.customer5.controller.ProductVideo", {
        onInit: function (oEvent) {
            try {
                let owi = ResourceBundle.create({url: "./model/owi.properties", async: false});
                let rootUri = owi.getText("rootUri");
                this.uri = rootUri + owi.getText("productVideo") + "?ftpRoot=productVideo&docName=";
                //alert('attachRequestCompleted. this.uri: ' + this.uri);

                let oModel = new JSONModel(this.uri);
                this.getView().setModel(oModel, 'ftp_docs');  // named model
                sap.ui.core.BusyIndicator.show();
                // test only:
                oModel.attachRequestCompleted(function () {
                    sap.ui.core.BusyIndicator.hide();
                      alert('attachRequestCompleted. json obj: ' + JSON.stringify(oModel.getData()));
                });




            } catch (e) {
                alert('Controller error ' + e);
            }
        },

        handlePress: function (oEvent) {

            try {
                const fileName = oEvent.getSource().getText();
                let uri = this.uri + fileName;
                const fileExt = fileName.split(".").pop().toLowerCase();

                let videoId = this.byId("idHTMLContent");
                //alert('uri: ' + uri);
                let oModel = new JSONModel(uri);
                oModel.attachRequestCompleted(function (response) {
                    //   alert('doc json obj.data: ' + JSON.stringify(oModel.getData().data));
                    //  alert('doc json obj: ' + JSON.stringify(oModel.getData()));

                    if (oModel.getData().message.indexOf("FAILED") >= 0) {
                        alert('File load failed. Contact IT Support');
                    }

                    switch (fileExt) {
                        case "txt":
                            $(window.open().document.body).html(oModel.getData().data);
                            break;
                        case "pdf":
                            window.open("data:application/pdf;base64, " + oModel.getData().data);
                            break;
                        case "mp4":
                            // Construct the <a> element
                            var link = document.createElement("a");
                            link.download = fileName;
                            // Construct the uri
                            var uri = 'data:video/mp4;base64,' + oModel.getData().data;
                            link.href = uri;
                            document.body.appendChild(link);
                            // fire the new link to download video
                            link.click();
                            // Cleanup the DOM
                            document.body.removeChild(link);
                            break;
                        default:
                            $(window.open().document.body).html(oModel.getData());
                    }
                });
            } catch (e) {
                alert('handlePress press error ' + e);
            }
        },

        showHidePress:
            function (oEvent) {
                 alert("showHidePress. " + this.byId("idImageReceiptButton").getText());
                let newText = this.byId("idImageReceiptButton").getText()==="Show Uploader" ? "Hide Uploader" : "Show Uploader";
                this.byId("idImageReceiptButton").setText(newText);

                //let showHide = this.byId("idUploaderContent").getVisible()===true ? false : true;
                //this.byId("idUploaderContent").setVisible(showHide);
            },

        onAfterRendering: function () {
           // alert('after render');
        },
        //
    });
});





