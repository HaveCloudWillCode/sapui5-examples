sap.ui.define([
	"sap/ui/core/util/MockServer",
	"sap/ui/model/json/JSONModel",
	"sap/base/util/UriParameters",
	"sap/base/Log"
], function (MockServer, JSONModel, UriParameters, Log) {
	"use strict";

 
    Log.info("Preparing Promise for metadata and mock odata server after time delay");
		    
	var oMockServer,
		_sAppPath = "com/owi/customer5/",
		_dsPathInApp = "/sap.app/dataSources/orderStatusService",
		_sJsonFilesPath = _sAppPath + "localService/mockdata/orderdetail/mockdata";

	var oMockServerInterface = {

		/**
		 * Initializes the mock server asynchronously.
		 * You can configure the delay with the URL parameter "serverDelay".
		 * The local mock data in this folder is returned instead of the real data for testing.
		 * @protected
		 * @param {object} [oOptionsParameter] init parameters for the mockserver
		 * @returns{Promise} a promise that is resolved when the mock server has been started
		 */
		init : function (oOptionsParameter) {
		    
			var oOptions = oOptionsParameter || {};
			/**
			 * a promise sets up an asycnchronse call back to process and object that may be returned later.
             * @link  https://developers.google.com/web/fundamentals/primers/promises#promise-api-reference
			 * Constructor
			 * assepts 2 parms resolve and reject.  
			 * Supported as of As of Chrome 32, Opera 19, Firefox 29, Safari 8 & Microsoft Edge, promises are enabled by default.
			 * so suitable for OWI test use but possibly not production.
			 *
			 * methods
			 *  
			 *  
			 *  Promise.resolve(promise); //Returns promise (only if promise.constructor == Promise)
			 * Promise.resolve(thenable); //Make a new promise from the thenable. A thenable is promise-like in as far as it has a `then()` method.
			 * Can be used for promist chaining 
			 * 
			 * Promise.resolve(obj);  // Make a promise that fulfills to obj. in this situation.
			 * Promise.reject(obj);  // Make a promise that rejects to obj. For consistency and debugging (e.g. stack traces), obj should be an instanceof Error.
			 * Promise.all(array);   // Make a promise that fulfills when every item in the array fulfills, and rejects if (and when) any item rejects. 
			 * Each array item is passed to Promise.resolve, so the array can be a mixture of promise-like objects and other objects. 
			 * The fulfillment value is an array (in order) of fulfillment values. The rejection value is the first rejection value.
			 * 
			 * Promise.race(array);  // Make a Promise that fulfills as soon as any item fulfills, or rejects as soon as any item rejects, whichever happens first.
			 * 
			 * 
			 * 
			 */
			return new Promise(function(fnResolve, fnReject) {
				var sManifestUrl = sap.ui.require.toUrl(_sAppPath + "manifest.json"),
					oManifestModel = new JSONModel(sManifestUrl); 

				oManifestModel.attachRequestCompleted(function ()  {
					var oUriParameters = new UriParameters(window.location.href),
						// parse manifest for local metatadata URI
						sJsonFilesUrl = sap.ui.require.toUrl(_sJsonFilesPath),
						oDataSource = oManifestModel.getProperty(_dsPathInApp),
						sMetadataUrl = sap.ui.require.toUrl(_sAppPath + oDataSource.settings.localUri),
						// ensure there is a trailing slash
						sMockServerUrl = /.*\/$/.test(oDataSource.uri) ? oDataSource.uri : oDataSource.uri + "/";

					// create a mock server instance or stop the existing one to reinitialize
					if (!oMockServer) {
						oMockServer = new MockServer({
							rootUri: sMockServerUrl
						});
					} else {
						oMockServer.stop();
					}

					// configure mock server with the given options or a default delay of 0.5s
					MockServer.config({
						autoRespond : true,
						autoRespondAfter : (oOptions.delay || oUriParameters.get("serverDelay") || 500)
					});

					// simulate all requests using mock data
					oMockServer.simulate(sMetadataUrl, {
						sMockdataBaseUrl : sJsonFilesUrl,
						bGenerateMissingMockData : true
					});

					var aRequests = oMockServer.getRequests();

					// compose an error response for each request
					var fnResponse = function (iErrCode, sMessage, aRequest) {
						aRequest.response = function(oXhr){
							oXhr.respond(iErrCode, {"Content-Type": "text/plain;charset=utf-8"}, sMessage);
						};
					};

					// simulate metadata errors
					if (oOptions.metadataError || oUriParameters.get("metadataError")) {
						aRequests.forEach(function (aEntry) {
							if (aEntry.path.toString().indexOf("$metadata") > -1) {
								fnResponse(500, "metadata Error", aEntry);
							}
						});
					}

					// simulate request errors
					var sErrorParam = oOptions.errorType || oUriParameters.get("errorType"),
						iErrorCode = sErrorParam === "badRequest" ? 400 : 500;
					if (sErrorParam) {
						aRequests.forEach(function (aEntry) {
							fnResponse(iErrorCode, sErrorParam, aEntry);
						});
					}

					// custom mock behaviour may be added here

					// set requests and start the server
					oMockServer.setRequests(aRequests);
					oMockServer.start();

					Log.info("Running the app with mock data");
					fnResolve();
				});

				oManifestModel.attachRequestFailed(function () {
					var sError = "Failed to load application manifest";

					Log.error(sError);
					fnReject(new Error(sError));
				});
			});
		},

		/**
		 * @public returns the mockserver of the app, should be used in integration tests
		 * @returns {sap.ui.core.util.MockServer} the mockserver instance
		 */
		getMockServer : function () {
			return oMockServer;
		}
	};

	return oMockServerInterface;
});