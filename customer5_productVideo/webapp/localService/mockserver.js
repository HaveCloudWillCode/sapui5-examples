sap.ui.define(
  ["sap/ui/core/util/MockServer",
    "sap/ui/model/json/JSONModel",
    "sap/base/i18n/ResourceBundle",
    "sap/base/Log"
  ],
  function (MockServer, JSONModel,ResourceBundle, Log) {
    "use strict";
    return {
      init: function () {
        jQuery.sap.log.setLevel(jQuery.sap.log.Level.DEBUG);
        jQuery.sap.log.info("**init before get atu services");
        
        let owi = ResourceBundle.create({url : "../model/owi.properties", async : false});
        let rootUri = owi.getText("rootUri");
        jQuery.sap.log.info("**rootUri:" + rootUri);

        var mockServer = new MockServer({
          rootUri: rootUri,
          requests: [{
              method: "GET",
              path: owi.getText("distOrderPath"),
              response: function (xhr) {
                jQuery.sap.log.info("**Mock response for distributor open orders returned");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/distributor/DistributorOpenOrders.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("fileDownloadPath"),
              response: function (xhr) {
                jQuery.sap.log.info("**Mock response for distributor open orders returned");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/distributor/base64BOL.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("distributorServiceLocationPath"),
              response: function (xhr) {
                //sap.base.Log.info("**Mock response for serviceLocation returned");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/distributor/distributorServiceLocations.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("distributorCreditMemoPath"),
              response: function (xhr) {
                //sap.base.Log.info("**Mock response for distributroCreditMemo returned");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/distributor/distributorCreditMemo_mock_data.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("oneAccountWebUserUri"),
              response: function (xhr) {
                jQuery.sap.log.info("**Mock response for one@owi.com");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/webuser/one@owi.com.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("manyAccountWebUserUri"),
              response: function (xhr) {
                jQuery.sap.log.info("**Mock response for many@owi.com");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/webuser/many@owi.com.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
                method: "POST",
                path: owi.getText("updateWebUser"),
                response: function (xhr) {
                  jQuery.sap.log.info("**Update webUser Mock responsed");
                  let oMockModel = new JSONModel();
                  oMockModel.loadData(
                    "../localService/mockdata/webuser/updateWebUser.json", {},
                    false
                  );
                  xhr.respondJSON(200, {}, oMockModel.getJSON());
                }
              },
            {
              method: "GET",
              path: owi.getText("distributorPricingPath"),
              response: function (xhr) {
                //sap.base.Log.info("** Mock response for distributorPricing returned");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/distributor/distributorPricing_mockdata.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("productCatalog") + ".?ftpRoot=(.*)&docName=(.*)",
              response: function (xhr, ftpRoot, docName) {
                Log.info("** Mock response for productCatalog. Parms ftpRoot: " + ftpRoot + ", docName: " + docName);

                if (docName !== "") {
                  docName += ".json";
                  var url = "../localService/mockdata/ftpListing/" + ftpRoot + "/" + docName ;
                } else {
                  url = "../localService/mockdata/ftpListing/" + ftpRoot + "/productCatalog.json";
                }
                //alert('mocked url: '+ url);
                let oMockModel = new JSONModel();
                oMockModel.loadData(url, {}, false);
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("productVideo") + ".?ftpRoot=(.*)&docName=(.*)",
              response: function (xhr, ftpRoot, docName) {
                Log.info("** Mock response for productVideo. Parms ftpRoot: " + ftpRoot + ", docName: " + docName);

                if (docName !== "") {
                  docName += ".json";
                  var url = "../localService/mockdata/ftpListing/" + ftpRoot + "/" + docName ;
                } else {
                  url = "../localService/mockdata/ftpListing/" + ftpRoot + "/productVideo.json";
                }
                let oMockModel = new JSONModel();
                oMockModel.loadData(url, {}, false);
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("orderListPath"),
              response: function (xhr) {
                //sap.base.Log.info("** Mock response for distributorPricing returned");
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/openorder/mockdata/ZCDSV_OTC_SFDC_ORDER_DATA.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("orderWithDetailPath"),
              response: function (xhr) {
                //sap.base.Log.info("** Mock response for distributorPricing returned");
                // mocks orderheader capture by entity by order number and account and bring expanded detail
                // normal odatq path would be ZP_OTC_I0246_OPEN_ORDERS_SRV/GetOrdHeadSet(IvOrder='2000076501',IvWerks='2029')?$expand=NavOrdHeadtoItem
                //  json only with many models greatly complicates this unfortunately and looses binding from the entity.
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/opendetail/mockdata/GetOrdHeadSet.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: owi.getText("accountUri") + ".?lib=(.*)&id=(.*)",
              response: function (xhr,lib,id) {
                jQuery.sap.log.info("**Account Mock responsed:"+ lib +id);
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/account/" + lib + "-" + id + ".json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "POST",
              path: owi.getText("sendMailUri"),
              response: function (xhr) {
                jQuery.sap.log.info("**sendMail Mock responsed");
                let oMockModel = new JSONModel();
                oMockModel.loadData("../localService/mockdata/support/sendmail-ok.json",{},false);
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            }
          ]
        });

        // start
        jQuery.sap.log.info("**start mock services");
        mockServer.start();

        sap.m.MessageToast.show("Running the app with mock data", {
          closeOnBrowserNavigation: false
        });
      }
    };
  }
);
