sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    //"com.owi.customer5/model/models"
], function (UIComponent, Device, models) {
    "use strict";

    return UIComponent.extend("com.owi.customer5.Component", {

        metadata: {
            manifest: "json"
        },

        /**
         * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
         * @public
         * @override
         */
        init: function () {
            try {

                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // set the device model
                //this.setModel(models.createDeviceModel(), "device");

                this.getRouter().initialize();
            } catch (e) {
                console.log(e);
                let err = new Error('failed.');
                if (e.message) {
                    alert('error');
                    err.message += '\n' + e.message + '. \n';
                    if (e.line) {
                        err.message += 'Line ' + e.line + '\n';
                    }
                }
            }
        }
    });
});