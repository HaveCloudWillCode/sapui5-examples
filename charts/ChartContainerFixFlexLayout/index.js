sap.ui.require([
	"sap/m/Shell",
	"sap/m/App",
	"sap/m/Page",
	"sap/ui/core/ComponentContainer"
], function(
	Shell, App, Page, ComponentContainer) {
	"use strict";

	sap.ui.getCore().attachInit(function() {
		new Shell ({
			app : new App ({
				pages : [
					new Page({
						title : "ChartContainer - Simple Toolbar with FixFlex Layout",
						enableScrolling : true,
						content : [
							new ComponentContainer({
								name : "sap.suite.ui.commons.sample.ChartContainerFixFlexLayout",
								settings : {
									id : "sap.suite.ui.commons.sample.ChartContainerFixFlexLayout"
								}
							})
						]
					})
				]
			})
		}).placeAt("content");
	});
});