sap.ui.define(['sap/ui/core/UIComponent'],
    function(UIComponent) {
    "use strict";

    var Component = UIComponent.extend("sap.viz.sample.CombinedColumnLine.Component", {

        metadata : {
            rootView : {
             "viewName": "sap.viz.sample.CombinedColumnLine.CombinedColumnLine",
               "type": "XML",
              "async": true
            },
            includes : ["../../css/exploredStyle.css"],
            dependencies : {
                libs : [
                    "sap.viz",
                    "sap.m"
                ]
            },
            config : {
                sample : {
                    stretch : true,
                    files : [
                        "CombinedColumnLine.view.xml",
                        "CombinedColumnLine.controller.js",
                        "InitPage.js"
                    ]
                }
            }
        }
    });

    return Component;

});
