sap.ui.define([], function () {
	"use strict";

	return {

		// i.e pass in A, B, C.....
		statusText: function (sStatus) {
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();

			switch (sStatus) {
				case "A":
					return oResourceBundle.getText("invoiceStatusA");  // returns value from i18n file
				case "B":
					return oResourceBundle.getText("invoiceStatusB");
				case "C":
					return oResourceBundle.getText("invoiceStatusC");
				default:
					// just return what was passed in if no case stmt exists
					return sStatus;
			}
		}
	};
});