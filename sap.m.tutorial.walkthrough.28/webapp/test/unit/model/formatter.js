/*global QUnit*/

sap.ui.define([
	"sap/ui/demo/walkthrough/model/formatter",
	"sap/ui/model/resource/ResourceModel"
], function (formatter, ResourceModel) {
	"use strict";

	QUnit.module("Formatting functions", {
		beforeEach: function () {
			this._oResourceModel = new ResourceModel({
				bundleUrl: sap.ui.require.toUrl("sap/ui/demo/walkthrough") + "/i18n/i18n.properties"
			});
		},
		afterEach: function () {
			this._oResourceModel.destroy();
		}
	});


	QUnit.test("Should return the translated texts", function (assert) {

		jQuery.sap.log.info('unit model formatter.js running asserts');

		// Arrange
		// this.stub() does not support chaining and always returns the right data
		// even if a wrong or empty parameter is passed.
		var oModel = this.stub();
		oModel.withArgs("i18n").returns(this._oResourceModel);  // get i18 model

		var oViewStub = {
			getModel: oModel
		};
		var oControllerStub = {
			getView: this.stub().returns(oViewStub)
		};

		// System under test. Gets values from model/formatter.js  not this test/formatter.js  -- not confusing. not
		var fnIsolatedFormatter = formatter.statusText.bind(oControllerStub);  // bind function to i18 model

		// i18n example: invoiceStatusA=New
		jQuery.sap.log.error('i18 value for invoiceStatusA: '+ fnIsolatedFormatter("A"));

		// Asserts --- converts passed param to i18 var. i.e A gets invoiceStatusA via function: fnIsolatedFormatter
		// return oResourceBundle.getText("invoiceStatusA");  // returns value from i18n file
		assert.strictEqual(fnIsolatedFormatter("A"), "New", "The long text for status A is correct");
		assert.strictEqual(fnIsolatedFormatter("NEW"), "New", "The long text for status A is false");  // fail. Not in case so returns NEW vs New

		assert.strictEqual(fnIsolatedFormatter("B"), "In Progress", "The long text for status B is correct");

		assert.strictEqual(fnIsolatedFormatter("C"), "Done", "The long text for status C is correct");

		assert.strictEqual(fnIsolatedFormatter("Foo"), "Foo", "The long text for status Foo is correct");
	});

});