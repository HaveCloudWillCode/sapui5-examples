sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
    "use strict";

    return BaseController.extend("sap.ui.demo.masterdetail.controller.DetailDetail", {
        onInit: function () {
            // create view model - and name it detailView
            var oViewModel = new JSONModel({
                busy: false,
                delay: 0,
                lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading"),
                noDataText: this.getResourceBundle().getText("detailListNoDataText")
            });

            // detailDetail = detail details route named in manifest
            this.getRouter().getRoute("detailDetail").attachPatternMatched(this._onObjectMatched, this);
            this.setModel(oViewModel, "detailDetailView");

            this.myChart = null;
        },
// not setup
        handleAboutPress: function () {
            var oNextUIState = this.oOwnerComponent.getHelper().getNextUIState(3);
            this.oRouter.navTo("page2", {layout: oNextUIState.layout});
        },

        // called from this onInit
        _onObjectMatched: function (oEvent) {
             // alert("DetailDetail::_onObjectMatched");

            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
            let lineItemID = oEvent.getParameter("arguments").lineItemID;
            //alert('DetailDetail. lineItemID: ' + lineItemID);

            let allRows = this.getOwnerComponent().getModel('LineItems').getData();  //alert(JSON.stringify(allRows));
            let row = [];
            let oFound = allRows.filter(function (el, n) {
                //console.log('el.LineItemID: ' + el.LineItemID);
                if (el.LineItemID === lineItemID) {
                    //alert('found one: ' +  JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                    row.push(allRows[n]);
                    return;
                }
            });

            //alert('row: ' + JSON.stringify(row[0]) );
            // set view header section - master row data selected
            let detailDetailModel = new sap.ui.model.json.JSONModel();
            detailDetailModel.setData(row[0]);  // can not be indexed obj [{}}] must be {}
            this.getView().setModel(detailDetailModel, "DetailItem");

            //this.myChart = testChart('chart3','bar', 'line');
            // type="Emphasized"
            //this.byId("barLine").setType("Emphasized");
        },
        /*
        onExit: function () {
            this.oRouter.getRoute("detailDetail").detachPatternMatched(this._onObjectMatched, this);
        },
        */
        onNavBack: function () {
            history.go(-1);
        },

        // one time load at app time
        onAfterRendering: function () {
           //  alert('DetailDetail. after rendering');

            this.myChart = testChart('chart3','bar', 'line');
            // type="Emphasized"
            this.byId("barLine").setType("Emphasized");

        },
        onChartShow: function (oEvent, chartType) {
            //alert ('chart type: ' + chartType);
            //this.myChart.clear();
            this.myChart.destroy();

            // this does not work if you use this.byId since treated as a callback (TypeError: this.byId is not a function)
            let that = this;
            $('.chartButton').each(function () {
                let n = this.id.lastIndexOf("-");
                let buttonId = this.id.substring(n + 1);
                //console.log(buttonId);
                that.byId(buttonId).setType("Default");
            });
            this.byId(chartType).setType("Emphasized");

            switch (chartType) {
                case 'barLine':
                    this.myChart = testChart('chart3','bar', 'line');
                    break;
                default:
                    this.myChart = testChart('chart3',chartType);
            }
        }
        // end ---------
    });
}, true);
