sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/ActionSheet",
	"sap/m/Button",
	"sap/ui/core/syncStyleClass",

], function (BaseController, JSONModel, ActionSheet,			 Button,			 syncStyleClass) {
	"use strict";

	return BaseController.extend("sap.ui.demo.masterdetail.controller.MasterApp", {

		onInit : function () {
			//alert('app');

			// i assume this could be moved to master controller   
			var oViewModel;
			var	fnSetAppNotBusy;
			var	iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

			// 2 col does not work here - still shows 1 col 			// TwoColumnsMidExpanded  vs OneColumn
			oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				layout : "OneColumn",
				previousLayout : "",
				actionButtonsInfo : {
					midColumn : {
						fullScreen : false
					}
				}
			});

			//alert('*** setting appView ****');
			this.setModel(oViewModel, "appView");  // global model inherited by all views - <App..../>  App level view

			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());


			// route to master now - since app view is setup as manifest rootView
			this.getRouter().navTo("master", {}, true);

			//this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
		},

		headerActionPress: function (oEvent) {
			//alert('headerActionPress');

			var profileSelected = function(oEvent){
				alert('route to My Account page');
				//this.onNav(oEvent, "myProfile");
			};

			var logout = function(oEvent){
				alert('log me out');
				/*
				this.setWebUser(null);
				//remove from model
				var webUser = this.getView().getModel("webUser");
				if(webUser){
					webUser.setData(null);
					webUser.updateBindings(true);
				}
				this.onNav(oEvent, "login");
				//reload the whole page/app to clear onInit content when logout
				window.location.reload();
				 */
			};


			var oActionSheet = new ActionSheet(this.getView().createId("headerActionSheet"), {
				title: 'My Account',
				showCancelButton: true,
				buttons: [
					new Button({
						text: 'My Account',
						type: "Transparent",
						icon:"sap-icon://customer",
						press: [profileSelected,this]
					})
				],
				afterClose: function () {
					oActionSheet.destroy();
				}
			});



			//Add Logout
			oActionSheet.addButton(new Button({
				text: "logout",
				type: "Transparent",
				icon:"sap-icon://log",
				press: [logout,this]
			}));

			syncStyleClass(this.getView().getController().getOwnerComponent().getContentDensityClass(), this.getView(), oActionSheet);
			// this.getView().addDependent(oActionSheet);
			oActionSheet.openBy(oEvent.getSource());


		},

		//onAfterRendering: function() {
		//	jQuery("#customer5container-customer5---app--navPage-cont").css("height","calc(100% - 80px)");
		//},

	});
});