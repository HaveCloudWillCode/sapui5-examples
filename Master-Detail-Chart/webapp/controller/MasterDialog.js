sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment"
], function (ManagedObject, Fragment) {
    "use strict";

    return ManagedObject.extend("sap.ui.demo.masterdetail.controller.MasterDialog", {
        constructor: function (oParentView) {
            //alert('oParentView: ' + oParentView);  // oParentView: Element sap.ui.core.mvc.XMLView#container-masterdetail---app
            this._oParentView = oParentView;  // root view - global view. bound to app view in Master ctrl
        },

        onInit: function (oEvent) {
        },

        // record is reference to json obj ele
        open: function (record) {
            // be careful here - make sure record in not indexed - i.e wrapped with []
            //alert("MasterDialog::open. record: " + JSON.stringify( record));
            const oParentView = this._oParentView;
            const theView = oParentView.byId("idMasterDialog");  // this is the dialog fragment view

            function populateAndOpen(theView, record) {
                //alert('record: ' + JSON.stringify(record)); // must be non-indexed json row

                function clone(obj) {
                    return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj)
                }

                const temp = clone(record);  // record is a mutable referenced object to Objects json obj ele. Make a clone incase changes canceled.
                temp.target = record;
                let viewModel = new sap.ui.model.json.JSONModel(temp);
                theView.setModel(viewModel);  // set cloned record

                //alert('test view data: ' + JSON.stringify(viewModel.getData()));
                theView.open()
            }

            // Create dialog lazily
            if (!theView) {
                //alert('create dialog');
                const oFragmentController = {

                    onCloseDialog: function () {
                        oParentView.byId("idMasterDialog").close();
                    },

                    onSaveDialog: function (oEvent) {
                        // alert("save pressed");
                        //alert('ui5MasterItem before: ' + JSON.stringify(ui5MasterItem.get().getData()));

                        const oParentViewModel = oParentView.getModel('Objects');  // named model instantiated in component. Master rows
                        //alert('oParentViewModel 1 - all rows: ' + JSON.stringify(oParentViewModel.getData()));

                        //console.log('oEvent.getSource(): ' + oEvent.getSource().getModel());  // save button event. assume below is way to get form data/model
                        const oDialogData = oEvent.getSource().getModel().getData();  // this un-named model created in open gets form data clone of record - not referenced
                       // alert('oDialogData (form changes): ' + JSON.stringify(oDialogData));   // shows new changed data

                        // create binding to form obj
                        const target = oDialogData.target;
                        //alert('target row before: ' + JSON.stringify(target));
                        //alert('orig: ' + JSON.stringify(oParentViewModel.getData()));  // all Master List rows
                        const target_id = target.ObjectID; //alert('target_id: '+ target_id);

                        if (oDialogData.bNewRecord) {
                            oDialogData.bNewRecord = false;

                            // todo - this needs to be a unique id like rowid
                            //oDialogData.ObjectID = ###;

                            oParentViewModel.getData().push(oDialogData);      // add the JSON parent element here
                            //alert('push record.  ' + JSON.stringify(oDialogData));
                        } else {
                            //alert('update record'); oDialogData[key] is each json field value
                            // loop thru all fields of cloned view record and update linked referenced record object. this will update the master model
                            Object.keys(oDialogData).forEach(function (key) {
                                //console.log('key: '+ key + '. oDialogData value: ' + oDialogData[key]);
                                //console.log('key: '+ key + '. target value: ' + target[key]);
                                // target key caused cyclic error to abend this process. skipping it works.
                                // target is original value, oDialogData is new value
                                // && target_id === oDialogData[key].ObjectID
                                if(key !== 'target' ) {
                                  //  alert( 'target_id: ' + target_id + ' dialog id: '+ oDialogData[key].ObjectID);
                                  //  alert(JSON.stringify(oDialogData[key]));
                                  //  alert('1: ' + JSON.stringify(ui5MasterItem.get().getData()));
                                    target[key] = oDialogData[key];  // updates json obj field's value (Master List/row)
                                  //  alert('2: ' + JSON.stringify(ui5MasterItem.get().getData()));
                                }
                            });

                        }
                        //alert('target row after: ' + JSON.stringify(target));
                        //alert('after: ' + JSON.stringify(ui5MasterItem.get().getData()));
/*
                        try {
                            var data = oParentViewModel.getData();
                            for (let property1 in data) {
                                console.log("oParentViewModel. item 2: " + property1 + '. value: ' + JSON.stringify(data[property1]));

                            }
                        } catch (e) {
                            alert('catch error: '+e);
                        }

 */
                        //alert('oParentViewModel 1 - all rows (updated): ' + JSON.stringify(oParentViewModel.getData()));
                        // update the parent json data model view binding - view only not actual json data model. so to update data base - pull from view or call db update webservice from here
                        oParentViewModel.setData(oParentViewModel.getData()); // this will refresh master list page view - maintains its link to master view

                        // func is not set/needed unless edit pressed from detail page
                        // this gets a reference to an element of Master list json object: Objects
                        // this is a reference to Detail view 'masteview' which is a reference to master Objects json element
                        // this closure func maintains its reference to Objects and maintains its link to Detail View ("MasterItem")
                        if(ui5MasterItem.get() != "") {
                            ui5MasterItem.get().setData(ui5MasterItem.get().getData());  // this will refresh detail page view 'MasterItem>/UnitNumber' - master top section
                        }

                        oParentView.byId("idMasterDialog").close(); // close popup page
                    }
                };

                // load asynchronous XML fragment
                Fragment.load({
                    id: this._oParentView.getId(),
                    name: "sap.ui.demo.masterdetail.view.MasterDialog",
                    controller: oFragmentController
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    // oParentView.addDependent(oDialog);

                    populateAndOpen(oDialog, record)
                });
            } else {
                populateAndOpen(theView, record)
            }
        },
    });
});