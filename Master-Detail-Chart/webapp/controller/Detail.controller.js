sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "./DetailDialog",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/Sorter",
    "sap/ui/model/FilterOperator",
    "sap/m/GroupHeaderListItem",
], function (BaseController, JSONModel, formatter, mobileLibrary, DetailDialog, Fragment, Filter, Sorter, FilterOperator, GroupHeaderListItem) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("sap.ui.demo.masterdetail.controller.Detail", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        onInit: function () {
            sap.ui.core.BusyIndicator.show();

            this._organizationDialog = new DetailDialog(this.getView());

            // create view model - and name it detailView
            var oViewModel = new JSONModel({
                busy: false,
                delay: 0,
                lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading"),
                noDataText: this.getResourceBundle().getText("detailListNoDataText")
            });

            // object = detail route name in manifest
            this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

            this.setModel(oViewModel, "detailView");

            this.next_master_row_objectID = null;

            // keeps the filter and search state
            this._oListFilterState = {
                aFilter: [],
                aSearch: []
            };

            // set up groupBy logic - 2 groups defined below
            this._oGroupFunctions = {
                UnitNumber: function (oContext) {
                    var iNumber = oContext.getProperty('UnitNumber'),
                        key, text;
                    if (iNumber <= 20) {
                        key = "LE20";
                        text = this.getResourceBundle().getText("masterGroup1Header1");
                    } else {
                        key = "GT20";
                        text = this.getResourceBundle().getText("masterGroup1Header2");
                    }
                    return {
                        key: key,
                        text: text
                    };
                }.bind(this)
            };
        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Event handler when the share by E-Mail button has been clicked
         * @public
         */
        onSendEmailPress: function () {
            var oViewModel = this.getModel("detailView");

            URLHelper.triggerEmail(
                null,
                oViewModel.getProperty("/shareSendEmailSubject"),
                oViewModel.getProperty("/shareSendEmailMessage")
            );
        },


        /**
         * Updates the item count within the line item table's header
         * @param {object} oEvent an event containing the total number of items in the list
         * @private
         */
        onListUpdateFinished: function (oEvent) {
            console.log('Detail::onListUpdateFinished. fired from xml component');
            var sTitle,
                iTotalItems = oEvent.getParameter("total"),
                oViewModel = this.getModel("detailView");  // set in this oninit

            // only update the counter if the length is final
            if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
                if (iTotalItems) {
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
                } else {
                    //Display 'Line Items' instead of 'Line items (0)'
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
                }
                oViewModel.setProperty("/lineItemListTitle", sTitle);
            }
        },

        onChartButtonPress: function(oEvent){
            this.getRouter().navTo("detailDetail", {lineItemID: 0})
        },

        /* =========================================================== */
        /* begin: internal methods                                     */
        /* =========================================================== */

        /**
         * Binds the view to the object path and expands the aggregated line items.
         * @function
         * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
         * @private
         */

        // called from this onInit
        _onObjectMatched: function (oEvent) {
            //alert("Detail::_onObjectMatched---- next_master_row_objectID: " + this.next_master_row_objectID);
            //let sObjectId = oEvent.getParameter("arguments").objectId;

            if (this.next_master_row_objectID !== null) {
                //alert('*** changing object id: ' + this.next_master_row_objectID);
                var sObjectId = this.next_master_row_objectID;
                this.next_master_row_objectID = null
            } else {
                sObjectId = oEvent.getParameter("arguments").objectId;  // comes from master list row selected
            }

            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");

            var allLineItems = this.getOwnerComponent().getModel('LineItems').getData();
            //alert("***allLineItems: " +  JSON.stringify( allLineItems) );

            var detailLineItems = [];
            console.log('allLineItems count: ' + allLineItems.length);
            if(allLineItems != undefined && allLineItems.length !=0) {
                var dFound = allLineItems.filter(function (el, n) {
                    if (el.ObjectID === sObjectId) {
                        //alert(JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                        detailLineItems.push(allLineItems[n]);
                        return;
                    }
                });
            }
            this.LineItems = allLineItems;
            //console.log("detailRow: " +  JSON.stringify( detailLineItems) );
            //alert("***detailRows: " +  JSON.stringify( detailLineItems) );

            var allObjectRows = this.getOwnerComponent().getModel('Objects').getData();
            var objectRow = [];
            var oFound = allObjectRows.filter(function (el, n) {
                if (el.ObjectID === sObjectId) {
                    //alert(JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                    objectRow.push(allObjectRows[n]);
                    return;
                }
            });
            //console.log("objectRow: " +  JSON.stringify( objectRow) );
            //alert("***objectRow: " +  JSON.stringify( objectRow) );

            // set view detail rows
            var detailsModel = new sap.ui.model.json.JSONModel();
            detailsModel.setData(detailLineItems);
            this.getView().setModel(detailsModel, "LineItems");

            // set view header section - master row data selected
            var masterModel = new sap.ui.model.json.JSONModel();
            masterModel.setData(objectRow[0]);  // can not be indexed obj. sets as pointer to ele
            // create detail view model for single Master Item (row)
            this.getView().setModel(masterModel, "MasterItem");  // binds model to view
            // ***objectRow: [{"ObjectID":"ObjectID_10","Name":"Object 10","Attribute1":"Attribute S","Attribute2":"Attribute T","UnitOfMeasure":"UoM","UnitNumber":106}]

            // set closure global func for this data model of a single row. Adds as a reference to Objects json obj element (object row)
            ui5MasterItem.set(masterModel);  // this sets as a reference to master json Objects object. Maintains its binding to view with get
        },


        /**
         * Set the full screen mode to false and navigate to master page
         */
        onCloseDetailPress: function () {
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
            // No item should be selected on master after detail page is closed
            this.getOwnerComponent().oListSelector.clearMasterListSelection();
            this.getRouter().navTo("master");
        },

        /**
         * Toggle between full and non full screen mode.
         */
        toggleFullScreen: function () {
            var bFullScreen = this.getModel("appView").getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
            if (!bFullScreen) {
                // store current layout and go full screen
                this.getModel("appView").setProperty("/previousLayout", this.getModel("appView").getProperty("/layout"));
                this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
            } else {
                // reset to previous layout
                this.getModel("appView").setProperty("/layout", this.getModel("appView").getProperty("/previousLayout"));
            }
        },

        onAfterRendering: function () {
            sap.ui.core.BusyIndicator.hide();

            //alert('detail. is mobile: '+ this.isMobile() );

            if (!this.isMobile()) {
                testChart('chart1', 'bar', 'line');
            }

            testinitMap();

        },

        onDeleteMaster: function (oEvent, param1) {
            //alert('onDeleteMaster pressed. parm1 '+ param1);

            // todo refactor this to get from top of json list ????//
            let objectId = this.byId("masterObjectId").getText();  // get id from view hidden input field

            let masterModel = this.getOwnerComponent().getModel("Objects");
            let masterData = masterModel.getData();
            let matchedRow = masterData.filter((row => row.ObjectID == objectId));
            let unMatchedRows = masterData.filter((row => row.ObjectID !== objectId));
            // alert('matchedRow: ' + JSON.stringify( matchedRow) );
            // console.log('unMatchedRows: ' + JSON.stringify( unMatchedRows) ); //alert( JSON.stringify( allData) );

            //alert('step1 find objectId: ' + objectId);

            // get json index
            let master_index_deleted = masterData.findIndex(function (item, i) {
                return item.ObjectID === objectId
            });

            //alert('step2');
            let master_rows_count = Object.keys(unMatchedRows).length;

            //alert('step3 ' + master_index_deleted + ' ' + master_rows_count);  // dies here
            try {
                if (master_index_deleted < master_rows_count) {
                    this.next_master_row_objectID = masterData[master_index_deleted + 1].ObjectID;
                    console.log(JSON.stringify(masterData[master_index_deleted + 1]));
                    //alert('next_master_row_objectID: ' + this.next_master_row_objectID);
                }
            } catch (err) {
                alert(err.message + ' ' + master_index_deleted + 1 + ' of ' + master_rows_count);
                this.next_master_row_objectID = masterData[master_rows_count].ObjectID
            }


            // update master Data model - 'NOT' view model since master view binds directly to the data model set in component
            // can not do this with the LineItems model since it is a subset of the details model set in the component and defined in this view
            masterModel.setData(unMatchedRows);  // issues as masterData vs masterModel


            //let test = this.getOwnerComponent().getModel("Objects");
            //console.log('test: ' + JSON.stringify(test.getData()));
            //alert('step6 - objects obj should now be changed');

            // delete by index
            //masterData.splice(master_index_deleted,1); // delete by index for x number of rows.
            //masterData.setData(masterData); // update view model data

            //todo: call webservice to update source of data - master and detail
            //var allObjectRows = this.getOwnerComponent().getModel('Objects').getData();
            //alert('master index deleted: '+ master_index_deleted);
            //this.getOwnerComponent().getModel('Objects').getData().splice(master_index_deleted,1);


            // update detail model
            //          let detailModel = this.getOwnerComponent().getModel("LineItems");
            let detailModel = this.getView().getModel("LineItems");  // set in this class
            let detailData = detailModel.getData();
            matchedRow = detailData.filter((row => row.ObjectID == objectId));
            unMatchedRows = detailData.filter((row => row.ObjectID !== objectId));

            // this works - delete by key
            detailModel.setData(detailData.filter((row => row.ObjectID !== objectId)));
            //         this.getView().setModel(detailModel, "LineItems");  // NOTE: only need this if reference controller model vs this view model.this will add next json index to view
            //         this.getView().setModel(deletedModel, "LineItems");  // NOTE: only need this if reference controller model vs this view model


            // clear header section of detail view
            //let deletedModel = new JSONModel();
            //this.getView().setModel(deletedModel, "MasterItem");
            //console.log('detail rows deleted');

            // update master view and bind to detail rows
            this._onObjectMatched(oEvent);

        },

// edit icon fired
        onEditMaster: function (oEvent) {
            //alert('onEditMaster');  // edit icon on item row

            // alert('this view master model data for single row: ' + JSON.stringify( this.getView().getModel("MasterItem").getData()) );
            // let objectId = this.byId("masterObjectId").getText();  // get id from view hidden input field
            let objectId = this.getView().getModel("MasterItem").getData().ObjectID;  // get id from view hidden input field
            //alert('objectId: ' + objectId);

            //  alert(JSON.stringify(oEvent.getSource().getBindingContext("Objects").getObject()));  // ?must be from table . row data un-named model/context
            //alert( JSON.stringify(this.getView().getModel("Objects").getData()));  // this works - gets all json data

            //let objectId = this.byId("masterObjectId").getText();  // get id from view hidden input field
            let matchedRow = this.getView().getModel("Objects").getData().filter((row => row.ObjectID == objectId));
            //alert('matched Master Row: ' +  JSON.stringify( matchedRow) );

            // this is the json row data. pass row to openSDialog component function
            // opens in own thread
            this.openMasterSDialog(matchedRow[0]); // named model/context  -- can not pass indexed json obj [{}], must be {}

        },
        // called from onEditMaster - above
        // record is passed by reference - points to json obj element
        openMasterSDialog: function (record) {
            //alert("Detail. master openMasterSDialog");             //alert(JSON.stringify(record));
            this.getOwnerComponent()._organizationDialog.open(record); // master created this component dialog
        },

// detail edit icon fired
        onEditDetail: function (oEvent) {
            //alert('onEditDetail');

            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );

            this.openSDialog(itemObject);
        },

// detail edit icon fired
        onAddDetail: function (oEvent) {
            //alert('onAddDetail');

            let objectId = this.getView().getModel("MasterItem").getData().ObjectID;
            this.openSDialog({bNewRecord: true, "ObjectID": objectId});

        },
        onDeleteDetail: function (oEvent) {
            //alert('onDeleteDetail');

            // get json row index
            let path = oEvent.getSource().getBindingContext("LineItems").getPath();
            let path_index = path.split("/").slice(-1).pop();

            // Cool code. This gets the json data for the selected row - Table/Item
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();  // gets active table row - xml
            let lineItemId = itemObject.LineItemID;
            //alert("item row data: " + JSON.stringify( itemObject ) );  // get LineItemID from this
            //alert('path: ' + path + '. path_index: '+ path_index + '. LineItemID: ' + itemObject.LineItemID); // gets item row index Tables/item

            // get json view model (not actual json source data model)
            let detailModel = this.getView().getModel("LineItems");  // set in this class
            let detailData = detailModel.getData();
            // 1) delete via filter - by element value
            //detailModel.setData(detailData.filter((row => row.LineItemID !== itemObject.LineItemID)));  // all except one deleted
            // or delete via delete
            //alert('delete index: ' + path_index + ' for json obj: ' + JSON.stringify(detailData));
            //delete detailData[path_index];  // this just nulls the json row - does not remove the row
            // 2) delete by json index value
            detailData.splice(path_index, 1); // delete by index for x number of rows.
            detailModel.setData(detailData); // update "view" model data

            //todo: call webservice to update source of data
            //var allLineItems = this.getOwnerComponent().getModel('LineItems').getData();
            //alert('delete from source. lineItemId: ' + lineItemId + '. path_index: '+ path_index);
            //this.LineItems.filter((row => row.LineItemID !== lineItemId));  // delete orignal data source set in component for LineItems
            //this.getOwnerComponent().getModel('LineItems').getData().splice(path_index,1);
            //this.getOwnerComponent().getModel('LineItems').getData().filter((row => row.LineItemID !== lineItemId));
            // get json index
            let detail_index_deleted = this.LineItems.findIndex(function (item, i) {
                return item.LineItemID === lineItemId
            });
            //alert('detail_index_deleted: ' + detail_index_deleted);
            this.LineItems.splice(detail_index_deleted, 1);

        },

        onObjectListItemPress: function (oEvent) {
            //alert("onObjectListItemPress PRESS");
            //let objectId = this.getView().getModel("MasterItem").getData().ObjectID;  // get id from view model vs from hidden input field
            //alert('objectId: ' + objectId);

            // cool code - gets row json fields
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );
            let lineItemID = itemObject.LineItemID;

            // this breaks it
            //let lineItemID = this.getView().getModel("LineItems").getData().LineItemID;  // get id from view model vs from hidden input field
            //alert('lineItemID: ' + lineItemID);


            //let supplierPath = oEvent.getSource().getBindingContext("LineItems").getPath();
            //let    supplier = supplierPath.split("/").slice(-1).pop();
            //let   oNextUIState = 'MidColumnFullScreen';  // endColumnPages

            // TwoColumnsMidExpanded vs
            //this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
            // No item should be selected on master after detail page is closed
            //this.getOwnerComponent().oListSelector.clearMasterListSelection();
            //this.getRouter().navTo("master");

            //console.log('detail route to detailDetail');
            //alert('detail route to detailDetail. lineItemID: ' + lineItemID);
            this.getRouter().navTo("detailDetail", {lineItemID: lineItemID});


        },
        openSDialog: function (record) {
            //alert("openSDialog. record: " + JSON.stringify( record) );
            this._organizationDialog.open(record); // calls js file instantiated in component.js
        },

        onOpenViewSettings: function (oEvent) {
            //alert("onOpenViewSettings");

            var sDialogTab = "filter";
            if (oEvent.getSource() instanceof sap.m.Button) {
                var sButtonId = oEvent.getSource().getId();
                if (sButtonId.match("sort")) {
                    sDialogTab = "sort";
                } else if (sButtonId.match("group")) {
                    sDialogTab = "group";
                }
            }
            // load asynchronous XML fragment. Fragment name: ViewSettingsDialog
            // pull by fragment id = <ViewSettingsDialog	id="viewSettingsDialog"
            if (!this.byId("idDetailFilterDialog")) {
                console.log('creating fragment');
                Fragment.load({
                    id: this.getView().getId(),
                    name: "sap.ui.demo.masterdetail.view.DetailFilterDialog",
                    controller: this
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    this.getView().addDependent(oDialog);  // bind fragment view to this view (Master view)
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.open(sDialogTab);
                }.bind(this));
            } else {
                console.log('fragment already created');
                this.byId("idDetailFilterDialog").open(sDialogTab);
            }

        },
        onConfirmViewSettingsDialog: function (oEvent) {
            // alert('onConfirmViewSettingsDialog');

            var aFilterItems = oEvent.getParameters().filterItems,
                aFilters = [],
                aCaptions = [];

            // update filter state:
            // combine the filter array and the filter string
            aFilterItems.forEach(function (oItem) {
                switch (oItem.getKey()) {
                    case "Filter1" :
                        aFilters.push(new Filter("UnitNumber", FilterOperator.LE, 100));  // fragment key="Filter1"
                        break;
                    case "Filter2" :
                        aFilters.push(new Filter("UnitNumber", FilterOperator.GT, 100));
                        break;
                    default :
                        break;
                }
                aCaptions.push(oItem.getText());
            });

            this._oListFilterState.aFilter = aFilters;  // apply filter to List. Add to class variable _oListFilterState for use below
            console.log('detail filter captions: ' + JSON.stringify(aCaptions));
            // this._updateFilterBar(aCaptions.join(", "));
            this._applyFilterSearch();
            this._applySortGroup(oEvent);
        },

        _applySortGroup: function (oEvent) {
            //groupBy
            var mParams = oEvent.getParameters(),
                sPath,
                bDescending,
                aSorters = [];
            // apply sorter to binding
            // (grouping comes before sorting)
            if (mParams.groupItem) {
                sPath = mParams.groupItem.getKey();
                bDescending = mParams.groupDescending;
                var vGroup = this._oGroupFunctions[sPath];  // 2 groups defined above
                aSorters.push(new Sorter(sPath, bDescending, vGroup));
            }
            sPath = mParams.sortItem.getKey();
            bDescending = mParams.sortDescending;
            aSorters.push(new Sorter(sPath, bDescending));

            console.log('aSorters: ' + JSON.stringify(aSorters));

            // Bind Sort. this._oList.getBinding("items").sort(aSorters);
            this.byId("lineItemsList").getBinding("items").sort(aSorters);
        },
        _applyFilterSearch: function () {
            var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
                oViewModel = this.getModel("detailView");
            // Bind Filter
            this.byId("lineItemsList").getBinding("items").filter(aFilters, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aFilters.length !== 0) {
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("detailListNoDataWithFilterOrSearchText"));
            } else if (this._oListFilterState.aSearch.length > 0) {
                // only reset the no data text to default when no new search was triggered
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("detailListNoDataText"));
            }
        },

        _updateFilterBar: function (sFilterBarText) {
            var oViewModel = this.getModel("detailView");
            oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
            oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
        },

        onFocusMapSearch: function (oEvent) {
            //console.log('onFocusMapSearch. search val: ' + this.byId("searchTextField").getValue());
            let searchInputEle = this.byId("searchTextField");
            let searchInputId = searchInputEle.getId();
            //console.log(searchInputEle); // Element sap.m.Input#container-masterdetail---detail--searchTextField
            //console.log('id: ' + searchInputId);
            let searchInputInnerId =   searchInputId + '-inner';
            //console.log ('searchInputInnerId: ' + searchInputInnerId);
            //let myid = document.getElementById('container-masterdetail---detail--searchTextField-inner');//alert(document.getElementById(viewDivId));
            // must be object HTMLInputElement
            let htmlInputElement = document.getElementById(searchInputInnerId);
            //
            map_search_initialize(htmlInputElement); // custom js
        },

        //
    });

});

