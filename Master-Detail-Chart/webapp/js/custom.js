$(document).ready(function () {
    // alert('doc ready');
});


// Generic Chart builder
function testChart(chartId = 'chart1', type = 'line', type2 = null) {
    //alert('testChart');
    let data1 = [12, 19, 3, 17, 6, 3, 7];
    let data2 = [2, 29, 5, 5, 2, 3, 10];
    let labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July"];
    let datasets = [{
        label: 'apples',
        data: data1,
        borderColor: "rgba(153,255,51,1)",
        backgroundColor: "rgba(153,255,51,0.4)"
    }, {
        label: 'oranges',
        data: data2,
        borderColor: "rgba(255,153,0,1)",
        backgroundColor: "rgba(255,153,0,0.4)"
    }];

    if (type2 !== null) {
        // alert('add a line. type2: '+ type2);
        let data = [15, 15, 15, 15, 15, 15, 15];
        datasets.push({
            label: 'Average',
            data: data,
            type: type2
        });
    }

    let ctx = document.getElementById(chartId).getContext('2d');
    let myChart = new Chart(ctx, {
        type: type,
        data: {
            labels: labels,
            datasets: datasets
        }
    });
    return myChart;
}

// called from ui5 xml listener on search input field's on focus function (liveChange): onFocusMapSearch
function map_search_initialize(searchInput) {
    var autocomplete = new google.maps.places.Autocomplete((searchInput), {types: ['geocode']});
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        //address;
        var place = autocomplete.getPlace();
        console.log('lat: ' + place.geometry.location.lat() + '   lng: ' + place.geometry.location.lng());
    });
    return;
}

function testinitMap() {
    // NOTE: need custom css for #map to work. locations test data define at bottom of this js as global var.
    //alert(JSON.stringify(locations));   // alert('map doc ' +  document.getElementById('map')); // must not be null    console.log('map doc ' + document.getElementById('map'));
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: new google.maps.LatLng(locations[0]['latitude'], locations[0]['longitude']),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        scaleControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        fullscreenControl: true
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
            map: map,
            title: locations[i]['store_name']
        });

        console.log("item " + i + ": " + JSON.stringify(locations[i]));
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {

                let directions = encodeURI('https://www.google.com/maps?saddr=current+location&daddr=' + locations[i]['store_name']
                    + ' ' + locations[i]['address'] + ',+' + locations[i]['city'] + ',+' + locations[i]['state']
                    + ',+' + locations[i]['zip']);
                let contentString = '<div class="marker-info-win">' +
                    '<div class="marker-inner-win"><span class="info-content">' +
                    '<h1 class="marker-heading"> ' + locations[i]['store_name'] + '</h1>'
                    + locations[i]['address'] + '<br>' + locations[i]['city'] + ', '
                    + locations[i]['state'] + '  ' + locations[i]['zip']
                    + "</br> phone: " + locations[i]['phone']
                    + "</br>   "
                    + '<a target="_blank" href="' + directions + '">Get Directions</a>'
                    + '</span></div></div>';
                console.log('step2');
                infowindow.setContent(contentString);
                console.log('pin content: ' + contentString);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

}


// test data //
var locations = [
    {
        "id": 1,
        "store_name": "1 Pep Boys",
        "store_number": "PB1",
        "address": "1555 Mangrove Ave",
        "city": "Chico",
        "state": "CA",
        "country": "US",
        "zip": "95926",
        "phone": "5308951336",
        "latitude": 39.746002,
        "longitude": -121.841370
    },
    {
        "id": 2,
        "store_name": "2 O'Reilly Auto Parts",
        "store_number": "OR1",
        "address": "1551 West Main Street",
        "city": "Ripon",
        "state": "CA",
        "country": "US",
        "zip": "95366-3012",
        "phone": "2095991546",
        "latitude": 37.739578,
        "longitude": -121.141403
    },
    {
        "id": 3,
        "store_name": "3 Autozone",
        "store_number": "AZ1",
        "address": "5160 Vineland Ave",
        "city": "N Hollywood",
        "state": "CA",
        "country": "US",
        "zip": "91601",
        "phone": "(818) 762-1183",
        "latitude": 34.164165,
        "longitude": -118.370529
    },
    {
        "id": 4,
        "store_name": "4 Pep Boys",
        "store_number": "PB1",
        "address": "125 W Imperial Hwy",
        "city": "La Habra",
        "state": "CA",
        "country": "US",
        "zip": "90631",
        "phone": "7144470601",
        "latitude": 33.917923,
        "longitude": -117.947144
    },


    {
        "id": 5,
        "store_name": "5 Pep Boys",
        "store_number": "PB1",
        "address": "902 N Imperial Ave",
        "city": "El Centro",
        "state": "CA",
        "country": "US",
        "zip": "92243",
        "phone": "7603538565",
        "latitude": 32.792858,
        "longitude": -115.565552
    },

    {
        "id": 6,
        "store_name": "6 O'Reilly Auto Parts",
        "store_number": "OR1",
        "address": "1608 West Rowan Avenue",
        "city": "Spokane",
        "state": "WA",
        "country": "US",
        "zip": "99205-5533",
        "phone": "5093261002",
        "latitude": 47.708145,
        "longitude": -117.435303
    },
];



