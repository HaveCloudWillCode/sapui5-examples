sap.ui.define(['sap/ui/core/UIComponent'],
	function(UIComponent) {
	"use strict";

	var Component = UIComponent.extend("sap.ui.layout.sample.Form480_12120.Component", {

		/* rootview fires first view - Page.view.xml - which fires Page.controller*/
		metadata : {
			rootView : {
				"viewName": "sap.ui.layout.sample.Form480_12120.Page",
				"type": "XML",
				"async": true
			},
			x_dependencies : {
				libs : [
					"sap.m",
					"sap.ui.layout"
				]
			},
			x_config : {
				sample : {
					stretch : true,
					files : [
						"Page.view.xml",
						"Page.controller.js",
						"Change.fragment.xml",
						"Display.fragment.xml"
					]
				}
			}
		}
	});

	return Component;

});
