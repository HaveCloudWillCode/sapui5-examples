/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
	"use strict";

	sap.ui.require([
		"customer5/customer5-webrules/test/integration/AllJourneys"
	], function() {
		QUnit.start();
	});
});