sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models",
	"./controller/ListSelector",
	"./controller/ErrorHandler",
	'sap/ui/model/json/JSONModel',
	"./controller/MasterDialog"
], function (UIComponent, Device, models, ListSelector, ErrorHandler, JSONModel, MasterDialog) {
	"use strict";

	return UIComponent.extend("sap.ui.demo.masterdetail.Component", {

		metadata : {
			manifest : "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * In this method, the device models are set and the router is initialized.
		 * @public
		 * @override
		 */
		init : function () {
			//alert('component');

			// set as class variable for use in all obj functions
			this.oListSelector = new ListSelector();  // not sure why we need this vs default List functionality - ListSelector.js type of BaseObject

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

            // had to add this so error class does not fail
			let oModel = new JSONModel();
			this.setModel(oModel);
			// set error handler
			this._oErrorHandler = new ErrorHandler(this);


			// set products demo model on this sample
			/*
			let oLineItems, oObjects, portFolioSummaries;
			oObjects = new JSONModel(sap.ui.require.toUrl("sap/ui/demo/masterdetail/mock") + "/dt_master.json");
			this.setModel(oObjects, 'Objects');
			oLineItems = new JSONModel(sap.ui.require.toUrl('sap/ui/demo/masterdetail/mock') + "/dt_details.json");
			this.setModel(oLineItems, 'LineItems');
			*/

			let customerInfo, portFolioSummaries;
			customerInfo = new JSONModel(sap.ui.require.toUrl("sap/ui/demo/masterdetail/mock") + "/dt_customer_info.json");
			this.setModel(customerInfo, 'CustomerInfo');

			portFolioSummaries = new JSONModel(sap.ui.require.toUrl('sap/ui/demo/masterdetail/mock') + "/dt_master_summary.json");
			this.setModel(portFolioSummaries, 'PortFolioSummaries');

			customerInfo.attachRequestCompleted(function () {
				// alert('attachRequestCompleted. oObjects json obj: ' + JSON.stringify(customerInfo.getData()));
			});
			//oLineItems.attachRequestCompleted(function () {
			//	 //alert('attachRequestCompleted. oLineItems json obj: ' + JSON.stringify(oLineItems.getData()));
			//});


			// call the base component's init function and create the App view
			UIComponent.prototype.init.apply(this, arguments);


			// set dialog. Note: will not work unless UIComponent is run first.
			this._organizationDialog = new MasterDialog(this.getRootControl());

			// create the views based on the url/hash
			this.getRouter().initialize();
		},

		/**
		 * The component is destroyed by UI5 automatically.
		 * In this method, the ListSelector and ErrorHandler are destroyed.
		 * @public
		 * @override
		 */
		destroy : function () {
			this.oListSelector.destroy();
			this._oErrorHandler.destroy();
			// call the base component's destroy function
			UIComponent.prototype.destroy.apply(this, arguments);
		},

		/**
		 * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
		 * design mode class should be set, which influences the size appearance of some controls.
		 * @public
		 * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
		 */
		getContentDensityClass : function() {
			if (this._sContentDensityClass === undefined) {
				// check whether FLP has already set the content density class; do nothing in this case
				// eslint-disable-next-line sap-no-proprietary-browser-api
				if (document.body.classList.contains("sapUiSizeCozy") || document.body.classList.contains("sapUiSizeCompact")) {
					this._sContentDensityClass = "";
				} else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					// "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		},

		openSDialog: function (record) {
			//alert("openSDialog"); alert( JSON.stringify( record) );
			this._organizationDialog.open(record); // calls js file instantiated in component.js
		}

	});
});