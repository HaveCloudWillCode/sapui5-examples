$(document).ready(function () {
    // alert('doc ready');

});



// Generic Chart builder
function genericChart(chartId = 'chart1', labels, type = 'line', datasets, type2 = null, data2 = null, label2 = null ) {
    // alert('gen chart');
    // let data1 = [12, 19, 3, 17, 6, 3, 7];
    // let data2 = [2, 29, 5, 5, 2, 3, 10];
    // let labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July"];
    /*
    let datasets = [{
        label: 'apples',
        data: data1,
        borderColor: "rgba(153,255,51,1)",
        backgroundColor: "rgba(153,255,51,0.4)"
    }, {
        label: 'oranges',
        data: data2,
        borderColor: "rgba(255,153,0,1)",
        backgroundColor: "rgba(255,153,0,0.4)"
    }]; */


    if (type2 !== null) {
        // alert('add a line. type2: '+ type2);
        //let data2 = [15, 15, 15, 15, 15, 15, 15];
        //let label2 = 'Average';
        datasets.push({
            label: label2,
            data: data2,
            type: type2
        });
    }

    //alert('datasets: ' +  JSON.stringify(datasets));

    let ctx = document.getElementById(chartId).getContext('2d');
    let myChart = new Chart(ctx, {
        type: type,
        data: {
            labels: labels,
            datasets: datasets
        }
    });
    return myChart;
}

// Generic Chart tester
function testChart(chartId = 'chart1', type = 'line', type2 = null) {
    let data1 = [12, 19, 3, 17, 6, 3, 7];
    let data2 = [2, 29, 5, 5, 2, 3, 10];
    let labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July"];
    let datasets = [{
        label: 'apples',
        data: data1,
        borderColor: "rgba(153,255,51,1)",
        backgroundColor: "rgba(153,255,51,0.4)"
    }, {
        label: 'oranges',
        data: data2,
        borderColor: "rgba(255,153,0,1)",
        backgroundColor: "rgba(255,153,0,0.4)"
    }];


    if (type2 !== null) {
        // alert('add a line. type2: '+ type2);
        let data = [15, 15, 15, 15, 15, 15, 15];
        datasets.push({
            label: 'Average',
            data: data,
            type: type2
        });
    }

    //alert('datasets: ' +  JSON.stringify(datasets));

    let ctx = document.getElementById(chartId).getContext('2d');
    let myChart = new Chart(ctx, {
        type: type,
        data: {
            labels: labels,
            datasets: datasets
        }
    });
    return myChart;
}





// 'http://dividendtrakr.devlocal.com/addPortfolio?PortfolioName=billsPortfolio'

let data=null;

    //create a function to set header
    function setHeader(data){
        //alert('setHeader. data: ' + data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': data
            }
        });

        //runAjax();
    }
    //in first load of the page set the header
    //setHeader($('meta[name="csrf-token"]').attr('content'));



    //create function to do the ajax request  we need to recall it when token is expired
    function runAjax(url){
        alert('testWebService runAjax. url: ' + url);

        //let url = 'http://dividendtrakr.devlocal.com/addPortfolio
        $.ajax({
            url: url,
            data: {'PortfolioName': 'billsPortfolio'},
            type: 'POST',
            datatype: 'JSON',
            success: function (response) {
                alert('success');
                //handle data
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error. '+ jqXHR.status);
                //if(jqXHR.status==419){
                    //if you get 419 error which meantoken expired
                    refreshToken(function(){
                        //refresh the token
                        //alert('xxx data: ' + data);
                        runAjax(url);//send ajax again
                    });
                //}
            }
        });
    }

    //token refresh function
    function refreshToken(callback){
        console.log('refreshToken');

        let url = 'http://dividendtrakr.devlocal.com/refresh-csrf';

        $.get(url).done(function(data){
            console.log('refreshToken: ' + data);
            setHeader(data);
            callback(true);
        });
    }
    //Optional: keep token updated every hour
    setInterval(function(){
        refreshToken(function(){
            console.log("Token refreshed!");
        });

    }, 3600000); // 1 hour




