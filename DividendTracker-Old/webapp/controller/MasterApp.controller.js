sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("sap.ui.demo.masterdetail.controller.MasterApp", {

		onInit : function () {
			//alert('app');

			// i assume this could be moved to master controller
			var oViewModel;
			var	fnSetAppNotBusy;
			var	iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

			// 2 col does not work here - still shows 1 col 			// TwoColumnsMidExpanded  vs OneColumn
			oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				layout : "OneColumn",
				previousLayout : "",
				actionButtonsInfo : {
					midColumn : {
						fullScreen : false
					}
				}
			});

			//alert('*** setting appView ****');
			this.setModel(oViewModel, "appView");  // global model inherited by all views - <App..../>  App level view

			/*
			fnSetAppNotBusy = function() {
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			};
			*/

			// since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
	 //		this.getOwnerComponent().getModel().metadataLoaded().then(fnSetAppNotBusy);

		 //	alert('step a1 fnSetAppNotBusy');
	// 		this.getOwnerComponent().getModel().attachMetadataFailed(fnSetAppNotBusy);

		 //	sap.ui.core.BusyIndicator.hide();

			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());


			// route to master now - since app view is setup as manifest rootView
			this.getRouter().navTo("master", {}, true);
		}

	});
});