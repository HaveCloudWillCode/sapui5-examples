sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "./DetailDialog",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/Sorter",
    "sap/ui/model/FilterOperator",
    "sap/m/GroupHeaderListItem",
], function (BaseController, JSONModel, formatter, mobileLibrary, DetailDialog, Fragment, Filter, Sorter, FilterOperator, GroupHeaderListItem) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("sap.ui.demo.masterdetail.controller.Detail", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        onInit: function () {
            sap.ui.core.BusyIndicator.show();

            // Model used to manipulate control states. The chosen values make sure,
            // detail page is busy indication immediately so there is no break in
            // between the busy indication for loading the view's meta data


// call the base component's init function and create the App view
           // UIComponent.prototype.init.apply(this, arguments);

            // set dialog. Note: will not work unless UIComponent is run first.
            //this.getView().setModel(detailsModel, "LineItems");
            this._organizationDialog = new DetailDialog(this.getView());



            // create view model - and name it detailView
            var oViewModel = new JSONModel({
                busy: false,
                delay: 0,
                lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading"),
                noDataText: this.getResourceBundle().getText("detailListNoDataText")
            });

            // object = detail route name in manifest
            this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

            this.setModel(oViewModel, "detailView");

            //this.getOwnerComponent().getModel("Objects").metadataLoaded().then(this._onMetadataLoaded.bind(this));
            this.next_master_row_masterId = null;

            // keeps the filter and search state
            this._oListFilterState = {
                aFilter: [],
                aSearch: []
            };

            // set up groupBy logic - 2 groups defined below
            this._oGroupFunctions = {
                UnitNumber: function (oContext) {
                    var iNumber = oContext.getProperty('Buy Price'),
                        key, text;
                    if (iNumber <= 20) {
                        key = "LE20";
                        text = this.getResourceBundle().getText("masterGroup1Header1");
                    } else {
                        key = "GT20";
                        text = this.getResourceBundle().getText("masterGroup1Header2");
                    }
                    return {
                        key: key,
                        text: text
                    };
                }.bind(this)
            };

            this._oGroupFunction1 = {
                Group_Id: function (oContext) {
                    var name = oContext.getProperty('Group_Id') ;
//alert(name);
                    return {
                        key: name,
                        text: name
                    };
                }.bind(this)
            };
        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Event handler when the share by E-Mail button has been clicked
         * @public
         */
        onSendEmailPress: function () {
            var oViewModel = this.getModel("detailView");

            URLHelper.triggerEmail(
                null,
                oViewModel.getProperty("/shareSendEmailSubject"),
                oViewModel.getProperty("/shareSendEmailMessage")
            );
        },


        /**
         * Updates the item count within the line item table's header
         * @param {object} oEvent an event containing the total number of items in the list
         * @private
         */
        onListUpdateFinished: function (oEvent) {
            console.log('Detail::onListUpdateFinished. fired from xml component');
            var sTitle,
                iTotalItems = oEvent.getParameter("total"),
                oViewModel = this.getModel("detailView");  // set in this oninit

            itemCount = iTotalItems;  // update master global var

            // only update the counter if the length is final
            if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
                if (iTotalItems) {
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
                } else {
                    //Display 'Line Items' instead of 'Line items (0)'
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
                }
                oViewModel.setProperty("/lineItemListTitle", sTitle);
            }
        },

        /* =========================================================== */
        /* begin: internal methods                                     */
        /* =========================================================== */

        /**
         * Binds the view to the object path and expands the aggregated line items.
         * @function
         * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
         * @private
         */

        // called from this onInit
        _onObjectMatched: function (oEvent) {
            // alert("Detail::_onObjectMatched");
            //alert( oEvent.getParameter("arguments").masterId );  // must be name in manifest target

            if(this.next_master_row_masterId !== null){
                //alert('*** changing object id: ' + this.next_master_row_masterId);
                var masterId = this.next_master_row_masterId;
                this.next_master_row_masterId = null
            } else {
                masterId = Number(oEvent.getParameter("arguments").masterId);  // comes from master list row selected
            }

            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");


            // todo build this on the fly
            /*
            let portfolioSummaries = this.getOwnerComponent().getModel('PortFolioSummaries').getData();
             alert("masterId: " + masterId +".***portfolioSummaries: " +  JSON.stringify( portfolioSummaries) );
            let getSummaryRow = portfolioSummaries.filter(function (el, n) {
                 alert( ' does [' + el.Portfolio_Id + '] = [' + masterId + '] --- ' +  JSON.stringify(el));
                if (el.Portfolio_Id === masterId) {
                    return portfolioSummaries[n];
                }
            });
            //alert('get summary row: ' + JSON.stringify(getSummaryRow));
            */
            let getSummaryRow = this.getOwnerComponent().getModel('PortFolioSummaries').getData();

            var allLineItems = this.getOwnerComponent().getModel('CustomerInfo').getData().PortfolioDetails;
            //alert("Detail::_onObjectMatched. masterId: " + masterId +".***allLineItems: " +  JSON.stringify( allLineItems) );

            var detailLineItems = [];
            var dFound = allLineItems.filter(function (el, n) {
                //alert( ' does [' + el.Portfolio_Id + '] = [' + masterId + '] --- ' +  JSON.stringify(el));
                //alert( typeof(el.Portfolio_Id) + ' - ' + typeof(masterId) );
                if (el.Portfolio_Id === masterId) {
                    //alert('found');
                    detailLineItems.push(allLineItems[n]);
                    return;
                }
            });
            this.LineItems = allLineItems;
            //console.log("detailRow: " +  JSON.stringify( detailLineItems) );
            // alert("***selected detailRows: " +  JSON.stringify( detailLineItems) );

            var allMasterRows = this.getOwnerComponent().getModel('CustomerInfo').getData().Portfolios;
            var objectRow = [];
            var oFound = allMasterRows.filter(function (el, n) {
                //alert(JSON.stringify(el));
                if (el.Portfolio_Id === masterId) {
                    //alert(JSON.stringify(el)); //alert('n: ' + n + lineItems[n]);
                    objectRow.push(allMasterRows[n]);
                    return;
                }
            });
            //console.log("objectRow: " +  JSON.stringify( objectRow) );
            //  alert("***master row: " +  JSON.stringify( objectRow) );
            var masterModel = new sap.ui.model.json.JSONModel();
            masterModel.setData(objectRow[0]);  // can not be indexed obj
            this.getView().setModel(masterModel, "MasterItem");

            //alert('get summary row: ' + JSON.stringify(getSummaryRow));

            // set view detail rows
            var detailsModel = new sap.ui.model.json.JSONModel();
            detailsModel.setData(detailLineItems);
            this.getView().setModel(detailsModel, "LineItems");

            // set view header section - master row data selected
            //var masterModel = new sap.ui.model.json.JSONModel();
            //let masterViewData = objectRow[0];  // convert indexed to element
            //masterViewData.summaryTableData = getSummaryRow;  // table items must be indexed obj
            //alert('masterViewData: ' + JSON.stringify(masterViewData));
            // alert('objectRow[0]: ' + JSON.stringify(objectRow[0]));
            //alert('getSummaryRow: ' + JSON.stringify(getSummaryRow));
            //masterModel.setData(masterViewData);  // can not be indexed obj
            //this.getView().setModel(masterModel, "MasterItem");

            var masterTableModel = new sap.ui.model.json.JSONModel();
            //masterTableModel.setData(getSummaryRow.tableData);
            masterTableModel.setData(getSummaryRow);
            this.getView().setModel(masterTableModel, "masterTableModel");
            //alert('masterTableModel: ' + JSON.stringify(this.getView().getModel("masterTableModel").getData()));

            // add chart  TODO get this from webservice - calc monthly amounts and mo avg
            // https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol=MSFT&apikey=demo
            // https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol=PCI&apikey=48MG0FRFRFY5ASGR
            // calc yield = last divinend amt * 12 / Last Closing Share Price
            let div_total = 0;
            let item_yield = "";
            for (let item in detailLineItems) {
                item_yield = detailLineItems[item]['Current Dividend Yield'].replace("%","");
                //alert( JSON.stringify( detailLineItems[item]));

                console.log('Symbol: ' + detailLineItems[item].Symbol + '. Shares: ' + detailLineItems[item].Shares
                    + '. Dividend Amount: '+ detailLineItems[item]['Dividend Amount']);

                div_total += Number(item_yield) ;
                //console.log('val: ' + detailLineItems[item]['Current Dividend Yield'] + '. item_yield: '+ item_yield + '. div_total: '+ div_total);
                // call function to build out total amt by month
                //console.log(" detailLineItems item: " + item  + '. value: ' + JSON.stringify( detailLineItems[item] ));
            }
            //alert(div_total.toFixed(2));


            let monthly_dividend_average = [];
            for ( var i = 0; i < 12; i++ ) {
                monthly_dividend_average[i]=17;
            }

            let monthly_dividend_data = [12, 19, 3, 17, 6, 3, 7, 10, 15, 10, 5, 25];
            let labels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
            //let monthly_dividend_average = [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15];
            let label2 = 'Average';
            let datasets = [{
                label: 'Monthly Total',
                data: monthly_dividend_data,
                borderColor: "rgba(153,255,51,1)",
                backgroundColor: "rgba(153,255,51,0.4)"
            }];
            //mychart.js function
            genericChart('chart1', labels ,'bar', datasets, 'line', monthly_dividend_average, label2);
            //testChart('chart2','pie');


        },

        /**
         * Binds the view to the object path. Makes sure that detail view displays
         * a busy indicator while data for the corresponding element binding is loaded.
         * @function
         * @param {string} sObjectPath path to the object to be bound to the view.
         * @private
         */
        _bindView: function (sObjectPath) {
            alert("bind view");
            // Set busy indicator during view binding
            var oViewModel = this.getModel("detailView");

            // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
            oViewModel.setProperty("/busy", false);

            this.getView().bindElement({
                path: sObjectPath,
                events: {
                    change: this._onBindingChange.bind(this),
                    dataRequested: function () {
                        oViewModel.setProperty("/busy", true);
                    },
                    dataReceived: function () {
                        oViewModel.setProperty("/busy", false);
                    }
                }
            });
        },

        _onBindingChange: function () {
            alert('on bind change');
            var oView = this.getView(),
                oElementBinding = oView.getElementBinding();

            // No data for the binding
            if (!oElementBinding.getBoundContext()) {
                this.getRouter().getTargets().display("detailObjectNotFound");
                // if object could not be found, the selection in the master list
                // does not make sense anymore.
                this.getOwnerComponent().oListSelector.clearMasterListSelection();
                return;
            }

            var sPath = oElementBinding.getPath(),
                oResourceBundle = this.getResourceBundle(),
                oObject = oView.getModel().getObject(sPath),
                sPortfolio_Id = oObject.Portfolio_Id,
                sObjectName = oObject.Name,
                oViewModel = this.getModel("detailView");

            this.getOwnerComponent().oListSelector.selectAListItem(sPath);

            oViewModel.setProperty("/shareSendEmailSubject",
                oResourceBundle.getText("shareSendEmailObjectSubject", [sPortfolio_Id]));
            oViewModel.setProperty("/shareSendEmailMessage",
                oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sPortfolio_Id, location.href]));
        },

        _onMetadataLoaded: function () {
            // Store original busy indicator delay for the detail view
            var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
                oViewModel = this.getModel("detailView"),
                oLineItemTable = this.byId("lineItemsList"),
                iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

            // Make sure busy indicator is displayed immediately when
            // detail view is displayed for the first time
            oViewModel.setProperty("/delay", 0);
            oViewModel.setProperty("/lineItemTableDelay", 0);

            oLineItemTable.attachEventOnce("updateFinished", function () {
                // Restore original busy indicator delay for line item table
                oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
            });

            // Binding the view will set it to not busy - so the view is always busy if it is not bound
            oViewModel.setProperty("/busy", true);
            // Restore original busy indicator delay for the detail view
            oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
        },

        /**
         * Set the full screen mode to false and navigate to master page
         */
        onCloseDetailPress: function () {
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
            // No item should be selected on master after detail page is closed
            this.getOwnerComponent().oListSelector.clearMasterListSelection();
            this.getRouter().navTo("master");
        },

        /**
         * Toggle between full and non full screen mode.
         */
        toggleFullScreen: function () {
            var bFullScreen = this.getModel("appView").getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
            if (!bFullScreen) {
                // store current layout and go full screen
                this.getModel("appView").setProperty("/previousLayout", this.getModel("appView").getProperty("/layout"));
                this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
            } else {
                // reset to previous layout
                this.getModel("appView").setProperty("/layout", this.getModel("appView").getProperty("/previousLayout"));
            }
        },

        onAfterRendering: function () {
            sap.ui.core.BusyIndicator.hide();

        },

        onDeleteMaster: function (oEvent, param1) {
        //console.log('this: ' + this); // this: EventProvider sap.ui.demo.masterdetail.controller.Detail
        //                       vs [maps] this: EventProvider testapp.controller.Master
           //  alert('delete master');

            let Portfolio_Id = Number(this.byId("masterPortfolio_Id").getText());  // get id from view hidden input field
            //alert('onDeleteMaster pressed. parm1 '+ param1 +'. Portfolio_Id: '+ Portfolio_Id);

            let customerInfoModel = this.getOwnerComponent().getModel("CustomerInfo");  // has both master and detail data json objs
            let customerInfoData = customerInfoModel.getData();

            //alert('customerInfoData.Portfolios: ' + JSON.stringify(customerInfoData.Portfolios));
            let matchedRow    = customerInfoData.Portfolios.filter((row => row.Portfolio_Id  == Portfolio_Id));
            let unMatchedRows =   customerInfoData.Portfolios.filter((row => row.Portfolio_Id !== Portfolio_Id));
            // alert('matchedRow: ' + JSON.stringify( matchedRow) );
           //  alert('unMatchedRows: ' + JSON.stringify( unMatchedRows) );

             alert('step1 ' );

            // get json index
            let master_index_deleted = customerInfoData.Portfolios.findIndex(function(item, i){
                return item.Portfolio_Id === Portfolio_Id ; // returns back id from this callback
            });

            alert('step2 = ' + master_index_deleted);

            //let master_rows_count = Object.keys(customerInfoData.Portfolios).length;
            let master_rows_count = Object.keys(unMatchedRows).length;

            // alert('step3');

            if ( master_index_deleted < master_rows_count){
                //this.next_master_row_masterId = customerInfoData.Portfolios[master_index_deleted +1].Portfolio_Id;
                this.next_master_row_masterId = customerInfoData.Portfolios[master_index_deleted +1].Portfolio_Id;
                 alert('next_master_row_masterId: ' + this.next_master_row_masterId);
            }

           // alert('step4');


            // update master Data model - 'NOT' view model since master view binds directly to the data model set in component
            // can not do this with the LineItems model since it is a subset of the details model set in the component and defined in this view
            //alert('  customerInfoData: ' + JSON.stringify( customerInfoData) );
            customerInfoData.Portfolios = unMatchedRows;
            customerInfoModel.setData(customerInfoData);  // issues as masterData vs masterModel
            console.log('master deleted');

            //alert('update rows: ' + JSON.stringify( customerInfoData.Portfolios) );

            // delete by index
            //masterData.splice(master_index_deleted,1); // delete by index for x number of rows.
            //masterData.setData(masterData); // update view model data

            //todo: call webservice to update source of data - master and detail
            //var allObjectRows = this.getOwnerComponent().getModel('Objects').getData();

            //this.getOwnerComponent().getModel('Objects').getData().splice(master_index_deleted,1);

            // update detail model
            //          let detailModel = this.getOwnerComponent().getModel("LineItems");
            let detailModel = this.getView().getModel("LineItems");  // set in this class  // this is the view model not data model
            let detailData = detailModel.getData();
            matchedRow = detailData.filter((row => row.Portfolio_Id == Portfolio_Id));
            unMatchedRows = detailData.filter((row => row.Portfolio_Id !== Portfolio_Id));
            //alert('matchedRow: ' + JSON.stringify( matchedRow) ); alert('unMatchedRows: ' + JSON.stringify( unMatchedRows) ); //alert( JSON.stringify( allData) );
            // delete detail rows
            //alert('detailmodel before: ' + JSON.stringify(detailModel.getData()));

            // this works - delete by key
            detailModel.setData(detailData.filter((row => row.Portfolio_Id !== Portfolio_Id)));
            //         this.getView().setModel(detailModel, "LineItems");  // NOTE: only need this if reference controller model vs this view model.this will add next json index to view
            //         this.getView().setModel(deletedModel, "LineItems");  // NOTE: only need this if reference controller model vs this view model


            // clear header section of detail view
            //let deletedModel = new JSONModel();
            //this.getView().setModel(deletedModel, "MasterItem");
            console.log('detail rows deleted');

            // update master view and bind to detail rows
            this._onObjectMatched(oEvent);

        },

// edit icon fired
        onEditMaster: function (oEvent) {
            //alert('onEditMaster');  // edit icon on item row

            //alert('this view master model data: ' + JSON.stringify( this.getView().getModel("MasterItem").getData()) );
            // let Portfolio_Id = this.byId("masterPortfolio_Id").getText();  // get id from view hidden input field
            let Portfolio_Id = this.getView().getModel("MasterItem").getData().Portfolio_Id;  // get id from view hidden input field
            //alert('Portfolio_Id: ' + Portfolio_Id);

            //  alert(JSON.stringify(oEvent.getSource().getBindingContext("Objects").getObject()));  // ?must be from table . row data un-named model/context
            //alert( JSON.stringify(this.getView().getModel("Objects").getData()));  // this works - gets all json data

            //let Portfolio_Id = this.byId("masterPortfolio_Id").getText();  // get id from view hidden input field
            let matchedRow = this.getView().getModel("CustomerInfo").getData().Portfolios.filter((row => row.Portfolio_Id == Portfolio_Id));
            //alert('matched Master Row: ' +  JSON.stringify( matchedRow) );

            // this is the json row data. pass row to openSDialog component function
            this.getOwnerComponent().openSDialog(matchedRow[0]); // named model/context  -- can not pass indexed json obj [{}], must be {}
        },

// detail edit icon fired
        onEditDetail: function (oEvent) {
            //alert('onEditDetail');

            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );

            this.openSDialog(itemObject);
        },

// detail edit icon fired
        onAddDetail: function (oEvent) {
            //alert('onAddDetail');

            let Portfolio_Id = this.getView().getModel("MasterItem").getData().Portfolio_Id;
            this.openSDialog({bNewRecord: true, "Portfolio_Id": Portfolio_Id});

        },
        onDeleteDetail: function (oEvent) {
            //alert('onDeleteDetail');

            // get json row index
            let path = oEvent.getSource().getBindingContext("LineItems").getPath();
            let path_index = path.split("/").slice(-1).pop();

            // Cool code. This gets the json data for the selected row - Table/Item
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();  // gets active table row - xml
            //let lineItemId = itemObject.LineItemID;
            //alert("item row data: " + JSON.stringify( itemObject ) );  // get LineItemID from this
            //alert('path: ' + path + '. path_index: '+ path_index + '. LineItemID: ' + itemObject.LineItemID); // gets item row index Tables/item
            let lineItemId = path_index;

            // get json view model (not actual json source data model)
            let detailModel = this.getView().getModel("LineItems");  // set in this class
            let detailData = detailModel.getData();
            // 1) delete via filter - by element value
            //detailModel.setData(detailData.filter((row => row.LineItemID !== itemObject.LineItemID)));  // all except one deleted
            // or delete via delete
            //alert('delete index: ' + path_index + ' for json obj: ' + JSON.stringify(detailData));
            //delete detailData[path_index];  // this just nulls the json row - does not remove the row
            // 2) delete by json index value
            detailData.splice(path_index,1); // delete by index for x number of rows.
            detailModel.setData(detailData); // update "view" model data

            //todo: call webservice to update source of data
            //var allLineItems = this.getOwnerComponent().getModel('LineItems').getData();
            //alert('delete from source. lineItemId: ' + lineItemId + '. path_index: '+ path_index);
            //this.LineItems.filter((row => row.LineItemID !== lineItemId));  // delete orignal data source set in component for LineItems
            //this.getOwnerComponent().getModel('LineItems').getData().splice(path_index,1);
            //this.getOwnerComponent().getModel('LineItems').getData().filter((row => row.LineItemID !== lineItemId));
            // get json index
            //let detail_index_deleted = this.LineItems.findIndex(function(item, i){
            //    return item.LineItemID === lineItemId
            //});
            //alert('detail_index_deleted: ' + lineItemId);
            //this.LineItems.splice(detail_index_deleted,1);

        },

        onObjectListItemPress: function (oEvent) {
            //alert("onObjectListItemPress PRESS");
            let Portfolio_Id = this.getView().getModel("MasterItem").getData().Portfolio_Id;  // get id from view model vs from hidden input field
            //alert('Portfolio_Id: ' + Portfolio_Id);

            // cool code - gets row json fields
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );
            let lineItemID = itemObject.LineItemID;

            // this breaks it
            //let lineItemID = this.getView().getModel("LineItems").getData().LineItemID;  // get id from view model vs from hidden input field
            //alert('lineItemID: ' + lineItemID);



            //let supplierPath = oEvent.getSource().getBindingContext("LineItems").getPath();
            //let    supplier = supplierPath.split("/").slice(-1).pop();
            //let   oNextUIState = 'MidColumnFullScreen';  // endColumnPages

            // TwoColumnsMidExpanded vs
            //this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
            // No item should be selected on master after detail page is closed
            //this.getOwnerComponent().oListSelector.clearMasterListSelection();
            //this.getRouter().navTo("master");

            //console.log('detail route to detailDetail');
            //alert('detail route to detailDetail. lineItemID: ' + lineItemID);
            this.getRouter().navTo("detailDetail", {lineItemID: lineItemID});


        },
        openSDialog: function (record) {
            console.log("openSDialog. record: " + JSON.stringify( record) );
            this._organizationDialog.open(record); // calls js file instantiated in component.js
        },

        onOpenViewSettings: function (oEvent) {
            //alert("onOpenViewSettings: Sort or Filter icon clicked");

            var sDialogTab = "filter";
            if (oEvent.getSource() instanceof sap.m.Button) {
                var sButtonId = oEvent.getSource().getId();
                if (sButtonId.match("sort")) {
                    sDialogTab = "sort";
                } else if (sButtonId.match("group")) {
                    sDialogTab = "group";
                }
            }
            // load asynchronous XML fragment. Fragment name: ViewSettingsDialog
            // pull by fragment id = <ViewSettingsDialog	id="viewSettingsDialog"
            if (!this.byId("idDetailFilterDialog")) {
                console.log('creating fragment');
                Fragment.load({
                    id: this.getView().getId(),
                    name: "sap.ui.demo.masterdetail.view.DetailFilterDialog",
                    controller: this
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    this.getView().addDependent(oDialog);  // bind fragment view to this view (Master view)
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.open(sDialogTab);
                }.bind(this));
            } else {
                console.log('fragment already created');
                this.byId("idDetailFilterDialog").open(sDialogTab);
            }

        },
        onConfirmViewSettingsDialog: function (oEvent) {
           // alert('onConfirmViewSettingsDialog');

            var aFilterItems = oEvent.getParameters().filterItems,
                aFilters = [],
                aCaptions = [];

            // update filter state:
            // combine the filter array and the filter string
            aFilterItems.forEach(function (oItem) {
                //alert(oItem.getKey());  // filter1 and filter2
                switch (oItem.getKey()) {
                    case "Filter1" :
                        aFilters.push(new Filter("Buy Price", FilterOperator.LE, 100));  // fragment key="Filter1"
                        break;
                    case "Filter2" :
                        aFilters.push(new Filter("Buy Price", FilterOperator.GT, 100));
                        break;
                    default :
                        break;
                }
                aCaptions.push(oItem.getText());
            });

            this._oListFilterState.aFilter = aFilters;  // apply filter to List. Add to class variable _oListFilterState for use below
            console.log('detail filter captions: ' + JSON.stringify(aCaptions) + '. aFilters: ' + JSON.stringify( aFilters));
           // this._updateFilterBar(aCaptions.join(", "));
            this._applyFilterSearch();
            this._applySortGroup(oEvent);
        },

        _applySortGroup: function (oEvent) {
            //groupBy
            var mParams = oEvent.getParameters(),
                sPath,
                bDescending,
                aSorters = [];
            // apply sorter to binding
            // (grouping comes before sorting)
            if (mParams.groupItem) {
                sPath = mParams.groupItem.getKey();
                bDescending = mParams.groupDescending;
                //var vGroup = this._oGroupFunctions[sPath];  // 2 groups defined above
                var vGroup = this._oGroupFunction1[sPath];


                aSorters.push(new Sorter(sPath, bDescending, vGroup));
            }
            sPath = mParams.sortItem.getKey();
            bDescending = mParams.sortDescending;
            aSorters.push(new Sorter(sPath, bDescending));

            console.log('aSorters: ' + JSON.stringify(aSorters));

           // Bind Sort. this._oList.getBinding("items").sort(aSorters);
            this.byId("lineItemsList").getBinding("items").sort(aSorters);
        },
        _applyFilterSearch: function () {
            var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
                oViewModel = this.getModel("detailView");
            // Bind Filter
            this.byId("lineItemsList").getBinding("items").filter(aFilters, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aFilters.length !== 0) {
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("detailListNoDataWithFilterOrSearchText"));
            } else if (this._oListFilterState.aSearch.length > 0) {
                // only reset the no data text to default when no new search was triggered
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("detailListNoDataText"));
            }
        },

        _updateFilterBar: function (sFilterBarText) {
            var oViewModel = this.getModel("detailView");
            oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
            oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
        }

        //
    });

});