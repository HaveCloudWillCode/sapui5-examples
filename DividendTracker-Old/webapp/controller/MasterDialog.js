let csv_to_json=null;

sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment"
], function (ManagedObject, Fragment) {
    "use strict";

    return ManagedObject.extend("sap.ui.demo.masterdetail.controller.MasterDialog", {
        constructor: function (oParentView) {
            // alert('oParentView: ' + oParentView);
            this._oParentView = oParentView;
            // nope this.csv_to_json=null;
        },

        onInit: function (oEvent) {
            // nope this.csv_to_json=null;
        },

        open: function (record) {
            // be careful here - make sure record in not indexed - i.e wrapped with []
            //alert("MasterDialog::open. record: " + JSON.stringify( record));
            const oParentView = this._oParentView;
            const theView = oParentView.byId("idMasterDialog");


            //alert('dialog count '+ masterCount);

            function populateAndOpen(theView, record) {
                //alert('record: ' + JSON.stringify(record)); // must be non-indexed json row

                function clone(obj) {
                    return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj)
                }

                const temp = clone(record);  // i guess this is due to it being mutable
                temp.target = record;
                let viewModel = new sap.ui.model.json.JSONModel(temp);
                theView.setModel(viewModel);

                //alert('test view data: ' + JSON.stringify(viewModel.getData()));
                theView.open()
            }

            // Create dialog lazily  --- all xml function calls must go inside here
            if (!theView) {
                //alert('create dialog');
                // nope this.csv_to_json=null;

                const oFragmentController = {

                    onCloseDialog: function () {
                        oParentView.byId("idMasterDialog").close();
                    },

                    onSaveDialog: function (oEvent) {
                        //alert("save pressed");
                        alert("save csv_to_json: " + JSON.stringify(csv_to_json) );

                        const oParentViewModel = oParentView.getModel('CustomerInfo');  // named data model instantiated in component
                        //alert('oParentViewModel 1 - all rows: ' + JSON.stringify(oParentViewModel.getData()));

                        console.log('oEvent.getSource(): ' + oEvent.getSource().getModel());  // save button event. assume below is way to get form data/model
                        const oDialogData = oEvent.getSource().getModel().getData();  // ?? works with un-named model ?? gets form data
                        // alert('oDialogData (form changes): ' + JSON.stringify(oDialogData));   // shows new changed data

                        // create binding to form obj
                        const target = oDialogData.target;
                        //console.log('target before: ' + JSON.stringify(target));

                        if (oDialogData.bNewRecord) {
                            //alert('Adding row. orig: ' + JSON.stringify(oParentViewModel.getData()));
                            oDialogData.bNewRecord = false;

                            // TODO code to insert new row and get back row id value for masterId  -- move this to new funct
                            addNewRow();
                            // simulating
                            oDialogData.Portfolio_Id = masterCount + 1;

                            alert('new row: ' + JSON.stringify(oDialogData));
                            // this is a bit tricky since CustomerInfo has Master and Detail json objects. So must specify below which one.
                            oParentViewModel.getData().Portfolios.push(oDialogData);      // add the JSON parent element here
                            //alert('push record.  ' + JSON.stringify(oDialogData));

                            // todo: create portf json and detail json objs

                            /*
                             "Portfolios": [
    {
      "Portfolio_Id": 1,
      "Portfolio_Name": "Hdo",
      "Portfolio_Desc": "My Hdo"
    },
    ...
"PortfolioDetails": [
    {
      "Portfolio_Id": 4,
      "Group_Id": "",
      "Symbol": "cash",
      "Name": "My Cash",
      "Shares": 1,
      "Buy Price": 10000
    },
                             */

                        } else {
                            //alert('update record');
                            Object.keys(oDialogData).forEach(function (key) {
                                console.log('key: ' + key + '. value: ' + oDialogData[key]);
                                // target key caused cyclic error to abend this process. skipping it works.
                                if (key !== 'target') {
                                    target[key] = oDialogData[key];  // update json obj
                                }
                            });
                            //console.log('target after: ' +  target);
                        }
                        //alert('crud action done');

                        try {
                            var data = oParentViewModel.getData();
                            for (let property1 in data) {
                                console.log(" item 2: " + property1 + '. value: ' + JSON.stringify(data[property1]));

                            }
                        } catch (e) {
                            alert('catch error: ' + e);
                        }

                        //alert('oParentViewModel 1 - all rows: ' + JSON.stringify(oParentViewModel.getData()));

                        // update the parent json data model view binding - view only not actual json data model. so to update data base - pull from view or call db update webservice from here
                        oParentViewModel.setData(oParentViewModel.getData());

                        oParentView.byId("idMasterDialog").close(); // close popup page


                        function addNewRow(theView, record) {
                            alert('addNewRow');

                        }
                    }
                    //

                    //
                    , handleFiles: function (oEvent) {
                        //alert('handleFiles');
                        var oFileToRead = oEvent.getParameters().files["0"];
                        var reader = new FileReader();
                        // Read file into memory as UTF-8
                        reader.readAsText(oFileToRead);
                        // Handle errors load
                        reader.onload = loadHandler;
                        reader.onerror = errorHandler;

                        function loadHandler(event) {
                            var csv = event.target.result;
                            csv_to_json = processData(csv);

                            // todo: save to websvc

                            console.log('json  importCsv: ' + JSON.stringify(csv_to_json));

                        }


                        function processData(csv) {
                            /* build json detail info
                            "PortfolioDetails": [
                                {
                                  "Portfolio_Id": 4,
                                  "Group_Id": "",
                                  "Symbol": "cash",
                                  "Name": "My Cash",
                                  "Shares": 1,
                                  "AvgCost": 10000
                                },
                             */

                            var lines = [];
                            var allTextLines = csv.split(/\r\n|\n/);

                            for (var i = 0; i < allTextLines.length; i++) {

                                //alert('allTextLines: ' +  allTextLines[i]);
                                //var data = allTextLines[i].split(';');
                                var data = allTextLines[i].split(',');

                                //alert('data: ' + data);
                                //var tarr = [];
                                //for (var j = 0; j < data.length; j++) {
                                //    tarr.push(data[j]);
                                //}
                                //lines.push(tarr);

                                if(data[1]) {
                                    data[0] = data[0].replace(/"/g, '');
                                    console.log('symbol: ' + data[0]);
                                    lines.push({"Symbol": data[0], "Shares": data[1], "AvgCost": data[2]})
                                }
                            }
                            //console.log('lines obj: ' + lines);
                            return lines;
                        }

                        function errorHandler(evt) {
                            if (evt.target.error.name == "NotReadableError") {
                                alert("Cannot read file !");
                            }
                        }


                    }
//


                    // all controller functions go above this
                };  // end of fragment controller

                // load asynchronous XML fragment
                Fragment.load({
                    id: this._oParentView.getId(),
                    name: "sap.ui.demo.masterdetail.view.MasterDialog",
                    controller: oFragmentController
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    // oParentView.addDependent(oDialog);

                    populateAndOpen(oDialog, record)
                });
            } else {
                populateAndOpen(theView, record)
            }
        },


        //
    });
});