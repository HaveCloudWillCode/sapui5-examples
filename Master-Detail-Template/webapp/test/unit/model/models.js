/*global QUnit*/

sap.ui.define([
	"sap/ui/demo/masterdetail/model/models"
], function (models) {
	"use strict";

	// createDeviceModel
	QUnit.module("createDeviceModel", {
		afterEach : function () {
			this.oDeviceModel.destroy();
		}
	});

	// models test function 1
	function isPhoneTestCase(assert, bIsPhone) {
		// Arrange
		this.stub(sap.ui.Device, "system", { phone : bIsPhone });

		// System under test
		this.oDeviceModel = models.createDeviceModel();

		// Assert
		assert.strictEqual(this.oDeviceModel.getData().system.phone, bIsPhone, "IsPhone property is correct");
	}

	// test case 1
	QUnit.test("Should initialize a device model for desktop", function (assert) {
		isPhoneTestCase.call(this, assert, false);
	});

    // test case 2
	QUnit.test("Should initialize a device model for phone", function (assert) {
		isPhoneTestCase.call(this, assert, true);
	});

	// models test function 2
	function isTouchTestCase(assert, bIsTouch) {
		// Arrange
		this.stub(sap.ui.Device, "support", { touch : bIsTouch });

		// System under test
		this.oDeviceModel = models.createDeviceModel();

		// Assert
		assert.strictEqual(this.oDeviceModel.getData().support.touch, bIsTouch, "IsTouch property is correct");
	}

	// test case 3
	QUnit.test("Should initialize a device model for non touch devices", function (assert) {
		isTouchTestCase.call(this, assert, false);
	});

	// test case 4
	QUnit.test("Should initialize a device model for touch devices", function (assert) {
		isTouchTestCase.call(this, assert, true);
	});

	// test case 5
	QUnit.test("The binding mode of the device model should be one way", function (assert) {

		// System under test
		this.oDeviceModel = models.createDeviceModel();

		// Assert
		assert.strictEqual(this.oDeviceModel.getDefaultBindingMode(), "OneWay", "Binding mode is correct");
	});
});