sap.ui.define(
  ["sap/ui/core/util/MockServer",
    "sap/base/Log"
  ],
  function (MockServer, Log) {
    "use strict";
    return {
      init: function () {

        // Defaults to look for the json data in webapp directory: "mock" and no GETS below are needed.
        // looks for this: "sap/ui/demo/masterdetail/mock"   from component.js

        var mockServer = new MockServer({
          rootUri: "http://ui5.devlocal.com/examples/Master-Detail-Template/webapp/localService/mockdata/",
          requests: [

              /*
            {
              method: "GET",
              path: "Objects.json",
              response: function (xhr) {
                alert('mocked');
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                  "../localService/mockdata/Objects.json", {},
                  false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            },
            {
              method: "GET",
              path: "LineItems.json",
              response: function (xhr) {
                alert('mocked');
                let oMockModel = new JSONModel();
                oMockModel.loadData(
                    "../localService/mockdata/LineItems.json", {},
                    false
                );
                xhr.respondJSON(200, {}, oMockModel.getJSON());
              }
            }
            */

          ]
        });

        // start
        Log.info("**start mock services");
        mockServer.start();

        sap.m.MessageToast.show("Running the app with mock data", {
          closeOnBrowserNavigation: false
        });
      }
    };
  }
);
