sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment"
], function (ManagedObject, Fragment) {
    "use strict";

    return ManagedObject.extend("sap.ui.demo.masterdetail.controller.MasterDialog", {
        constructor: function (oParentView) {
            // alert('oParentView: ' + oParentView);
            this._oParentView = oParentView;
        },

        onInit: function (oEvent) {
        },

        open: function (record) {
            // be careful here - make sure record in not indexed - i.e wrapped with []
            //alert("MasterDialog::open. record: " + JSON.stringify( record));
            const oParentView = this._oParentView;
            const theView = oParentView.byId("idMasterDialog");

            //record = record[0];

            function populateAndOpen(theView, record) {
                //alert('record: ' + JSON.stringify(record)); // must be non-indexed json row

                function clone(obj) {
                    return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj)
                }

                const temp = clone(record);  // i guess this is due to it being mutable
                temp.target = record;
                let viewModel = new sap.ui.model.json.JSONModel(temp);
                theView.setModel(viewModel);

                //alert('test view data: ' + JSON.stringify(viewModel.getData()));
                theView.open()
            }

            // Create dialog lazily
            if (!theView) {
                //alert('create dialog');
                const oFragmentController = {

                    onCloseDialog: function () {
                        oParentView.byId("idMasterDialog").close();
                    },

                    onSaveDialog: function (oEvent) {
                        // alert("save pressed");
                        const oParentViewModel = oParentView.getModel('Objects');  // named model instantiated in component
                        //alert('oParentViewModel 1 - all rows: ' + JSON.stringify(oParentViewModel.getData()));

                        console.log('oEvent.getSource(): ' + oEvent.getSource().getModel());  // save button event. assume below is way to get form data/model
                        const oDialogData = oEvent.getSource().getModel().getData();  // ?? works with un-named model ?? gets form data
                       // alert('oDialogData (form changes): ' + JSON.stringify(oDialogData));   // shows new changed data

                        // create binding to form obj
                        const target = oDialogData.target;
                        //console.log('target before: ' + JSON.stringify(target));

                        if (oDialogData.bNewRecord) {
                            //alert('orig: ' + JSON.stringify(oParentViewModel.getData()));
                            oDialogData.bNewRecord = false;
                            oParentViewModel.getData().push(oDialogData);      // add the JSON parent element here
                            //alert('push record.  ' + JSON.stringify(oDialogData));
                        } else {
                            //alert('update record');
                            Object.keys(oDialogData).forEach(function (key) {
                                console.log('key: '+ key + '. value: ' + oDialogData[key]);
                                // target key caused cyclic error to abend this process. skipping it works.
                                if(key !== 'target') {
                                    target[key] = oDialogData[key];  // update json obj
                                }
                            });
                            //console.log('target after: ' +  target);
                        }
                        //alert('crud action done');

                        try {
                            var data = oParentViewModel.getData();
                            for (let property1 in data) {
                                console.log(" item 2: " + property1 + '. value: ' + JSON.stringify(data[property1]));

                            }
                        } catch (e) {
                            alert('catch error: '+e);
                        }

                        //alert('oParentViewModel 1 - all rows: ' + JSON.stringify(oParentViewModel.getData()));

                        // update the parent json data model view binding - view only not actual json data model. so to update data base - pull from view or call db update webservice from here
                        oParentViewModel.setData(oParentViewModel.getData());

                        oParentView.byId("idMasterDialog").close(); // close popup page
                    }
                };

                // load asynchronous XML fragment
                Fragment.load({
                    id: this._oParentView.getId(),
                    name: "sap.ui.demo.masterdetail.view.MasterDialog",
                    controller: oFragmentController
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    // oParentView.addDependent(oDialog);

                    populateAndOpen(oDialog, record)
                });
            } else {
                populateAndOpen(theView, record)
            }
        },
    });
});