sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("sap.ui.demo.masterdetail.controller.DetailDetail", {
		onInit: function () {
			//alert('DetailDetail');
			//this.oOwnerComponent = this.getOwnerComponent();
			//this.oRouter = this.oOwnerComponent.getRouter();
			//this.oModel = this.oOwnerComponent.getModel();
			//this.oRouter.getRoute("detailDetail").attachPatternMatched(this._onSupplierMatched, this);

			/*
			// set view header section - master row data selected
            var masterModel = new sap.ui.model.json.JSONModel();
            masterModel.setData(objectRow[0]);  // can not be indexed obj
            this.getView().setModel(masterModel, "MasterItem");
			 */

			// create view model - and name it detailView
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0,
				lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading"),
				noDataText: this.getResourceBundle().getText("detailListNoDataText")
			});

			// object = detail route name in manifest
			this.getRouter().getRoute("detailDetail").attachPatternMatched(this._onObjectMatched, this);

			this.setModel(oViewModel, "detailDetailView");



		},

		handleAboutPress: function () {
			var oNextUIState = this.oOwnerComponent.getHelper().getNextUIState(3);
			this.oRouter.navTo("page2", {layout: oNextUIState.layout});
		},

		// called from this onInit
		_onObjectMatched: function (oEvent) {
			//alert("Detail::_onObjectMatched");

			this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");

			let lineItemID = oEvent.getParameter("arguments").lineItemID;
			//alert('DetailDetail. lineItemID: ' + lineItemID);

			let allRows = this.getOwnerComponent().getModel('LineItems').getData();  //alert(JSON.stringify(allRows));

			let row = [];
			let oFound = allRows.filter(function (el, n) {
				console.log('el.LineItemID: ' + el.LineItemID);
				if (el.LineItemID === lineItemID) {
					 //alert('found one: ' +  JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
					row.push(allRows[n]);
					return;
				}
			});

			//alert('row: ' + JSON.stringify(row[0]) );
			// set view header section - master row data selected
			let detailDetailModel = new sap.ui.model.json.JSONModel();
			detailDetailModel.setData(row[0]);  // can not be indexed obj [{}}] must be {}
			this.getView().setModel(detailDetailModel, "DetailItem");


			/*

			let path = oEvent.getSource().getBindingContext("LineItems").getPath();
            let path_index = path.split("/").slice(-1).pop();

            // Cool code. This gets the json data for the selected row - Table/Item
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );  // get LineItemID from this
            //alert('path: ' + path + '. path_index: '+ path_index + '. LineItemID: ' + itemObject.LineItemID); // gets item row index

            let detailModel = this.getView().getModel("LineItems");  // set in this class
            let detailData = detailModel.getData();
            detailModel.setData(detailData.filter((row => row.LineItemID !== itemObject.LineItemID)));  // all except one deleted

			 */


			// Cool code. This gets the json data for the selected row - Table/Item
		//	let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
		//	alert("item row data: " + JSON.stringify( itemObject ) );  // get LineItemID from this
			//alert('path: ' + path + '. path_index: '+ path_index + '. LineItemID: ' + itemObject.LineItemID); // gets item row index

			// set view detail rows
			/*
			var detailsModel = new sap.ui.model.json.JSONModel();
			detailsModel.setData(detailLineItems);
			this.getView().setModel(detailsModel, "LineItems");

			// set view header section - master row data selected
			var masterModel = new sap.ui.model.json.JSONModel();
			masterModel.setData(objectRow[0]);  // can not be indexed obj
			this.getView().setModel(masterModel, "MasterItem");
			*/

		},

		/*
		_onSupplierMatched: function (oEvent) {
			this._supplier = oEvent.getParameter("arguments").supplier || this._supplier || "0";
			this.getView().bindElement({
				path: "/ProductCollectionStats/Filters/1/values/" + this._supplier,
				model: "products"
			});
		},
		*/

		/*
		handleFullScreen: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/endColumn/fullScreen");
			this.oRouter.navTo("detailDetail", {  objectId: this._objectId});
		},

		handleExitFullScreen: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/endColumn/exitFullScreen");
			this.oRouter.navTo("detailDetail", { objectId: this._objectId});
		},

		handleClose: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/endColumn/closeColumn");
			this.oRouter.navTo("master",  );
		},

		onExit: function () {
			this.oRouter.getRoute("detailDetail").detachPatternMatched(this._onObjectMatched, this);
		},
		*/

		onNavBack : function() {
			history.go(-1);
		}

	});
}, true);
