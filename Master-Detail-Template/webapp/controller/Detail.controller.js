sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library",
    "./DetailDialog",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/Sorter",
    "sap/ui/model/FilterOperator",
    "sap/m/GroupHeaderListItem",
], function (BaseController, JSONModel, formatter, mobileLibrary, DetailDialog, Fragment, Filter, Sorter, FilterOperator, GroupHeaderListItem) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("sap.ui.demo.masterdetail.controller.Detail", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        onInit: function () {
            sap.ui.core.BusyIndicator.show();

            // Model used to manipulate control states. The chosen values make sure,
            // detail page is busy indication immediately so there is no break in
            // between the busy indication for loading the view's meta data


// call the base component's init function and create the App view
           // UIComponent.prototype.init.apply(this, arguments);

            // set dialog. Note: will not work unless UIComponent is run first.
            //this.getView().setModel(detailsModel, "LineItems");
            this._organizationDialog = new DetailDialog(this.getView());



            // create view model - and name it detailView
            var oViewModel = new JSONModel({
                busy: false,
                delay: 0,
                lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading"),
                noDataText: this.getResourceBundle().getText("detailListNoDataText")
            });

            // object = detail route name in manifest
            this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

            this.setModel(oViewModel, "detailView");

            //this.getOwnerComponent().getModel("Objects").metadataLoaded().then(this._onMetadataLoaded.bind(this));
            this.next_master_row_objectID = null;

            // keeps the filter and search state
            this._oListFilterState = {
                aFilter: [],
                aSearch: []
            };

            // set up groupBy logic - 2 groups defined below
            this._oGroupFunctions = {
                UnitNumber: function (oContext) {
                    var iNumber = oContext.getProperty('UnitNumber'),
                        key, text;
                    if (iNumber <= 20) {
                        key = "LE20";
                        text = this.getResourceBundle().getText("masterGroup1Header1");
                    } else {
                        key = "GT20";
                        text = this.getResourceBundle().getText("masterGroup1Header2");
                    }
                    return {
                        key: key,
                        text: text
                    };
                }.bind(this)
            };
        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Event handler when the share by E-Mail button has been clicked
         * @public
         */
        onSendEmailPress: function () {
            var oViewModel = this.getModel("detailView");

            URLHelper.triggerEmail(
                null,
                oViewModel.getProperty("/shareSendEmailSubject"),
                oViewModel.getProperty("/shareSendEmailMessage")
            );
        },


        /**
         * Updates the item count within the line item table's header
         * @param {object} oEvent an event containing the total number of items in the list
         * @private
         */
        onListUpdateFinished: function (oEvent) {
            console.log('Detail::onListUpdateFinished. fired from xml component');
            var sTitle,
                iTotalItems = oEvent.getParameter("total"),
                oViewModel = this.getModel("detailView");  // set in this oninit

            // only update the counter if the length is final
            if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
                if (iTotalItems) {
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
                } else {
                    //Display 'Line Items' instead of 'Line items (0)'
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
                }
                oViewModel.setProperty("/lineItemListTitle", sTitle);
            }
        },

        /* =========================================================== */
        /* begin: internal methods                                     */
        /* =========================================================== */

        /**
         * Binds the view to the object path and expands the aggregated line items.
         * @function
         * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
         * @private
         */

        // called from this onInit
        _onObjectMatched: function (oEvent) {
            //alert("Detail::_onObjectMatched---- next_master_row_objectID: " + this.next_master_row_objectID);
            //let sObjectId = oEvent.getParameter("arguments").objectId;

            if(this.next_master_row_objectID !== null){
                //alert('*** changing object id: ' + this.next_master_row_objectID);
                var sObjectId = this.next_master_row_objectID;
                this.next_master_row_objectID = null
            } else {
                sObjectId = oEvent.getParameter("arguments").objectId;  // comes from master list row selected
            }

            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");


            var allLineItems = this.getOwnerComponent().getModel('LineItems').getData();
            var detailLineItems = [];
            var dFound = allLineItems.filter(function (el, n) {
                if (el.ObjectID === sObjectId) {
                    //alert(JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                    detailLineItems.push(allLineItems[n]);
                    return;
                }
            });
            //console.log("detailRow: " +  JSON.stringify( detailLineItems) );
            //alert("***detailRows: " +  JSON.stringify( detailLineItems) );

            var allObjectRows = this.getOwnerComponent().getModel('Objects').getData();
            var objectRow = [];
            var oFound = allObjectRows.filter(function (el, n) {
                if (el.ObjectID === sObjectId) {
                    //alert(JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                    objectRow.push(allObjectRows[n]);
                    return;
                }
            });
            //console.log("objectRow: " +  JSON.stringify( objectRow) );
            //alert("***objectRow: " +  JSON.stringify( objectRow) );

            // set view detail rows
            var detailsModel = new sap.ui.model.json.JSONModel();
            detailsModel.setData(detailLineItems);
            this.getView().setModel(detailsModel, "LineItems");

            // set view header section - master row data selected
            var masterModel = new sap.ui.model.json.JSONModel();
            masterModel.setData(objectRow[0]);  // can not be indexed obj
            this.getView().setModel(masterModel, "MasterItem");
            // ***objectRow: [{"ObjectID":"ObjectID_10","Name":"Object 10","Attribute1":"Attribute S","Attribute2":"Attribute T","UnitOfMeasure":"UoM","UnitNumber":106}]

        },

        /**
         * Binds the view to the object path. Makes sure that detail view displays
         * a busy indicator while data for the corresponding element binding is loaded.
         * @function
         * @param {string} sObjectPath path to the object to be bound to the view.
         * @private
         */
        _bindView: function (sObjectPath) {
            alert("bind view");
            // Set busy indicator during view binding
            var oViewModel = this.getModel("detailView");

            // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
            oViewModel.setProperty("/busy", false);

            this.getView().bindElement({
                path: sObjectPath,
                events: {
                    change: this._onBindingChange.bind(this),
                    dataRequested: function () {
                        oViewModel.setProperty("/busy", true);
                    },
                    dataReceived: function () {
                        oViewModel.setProperty("/busy", false);
                    }
                }
            });
        },

        _onBindingChange: function () {
            alert('on bind change');
            var oView = this.getView(),
                oElementBinding = oView.getElementBinding();

            // No data for the binding
            if (!oElementBinding.getBoundContext()) {
                this.getRouter().getTargets().display("detailObjectNotFound");
                // if object could not be found, the selection in the master list
                // does not make sense anymore.
                this.getOwnerComponent().oListSelector.clearMasterListSelection();
                return;
            }

            var sPath = oElementBinding.getPath(),
                oResourceBundle = this.getResourceBundle(),
                oObject = oView.getModel().getObject(sPath),
                sObjectId = oObject.ObjectID,
                sObjectName = oObject.Name,
                oViewModel = this.getModel("detailView");

            this.getOwnerComponent().oListSelector.selectAListItem(sPath);

            oViewModel.setProperty("/shareSendEmailSubject",
                oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
            oViewModel.setProperty("/shareSendEmailMessage",
                oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
        },

        _onMetadataLoaded: function () {
            // Store original busy indicator delay for the detail view
            var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
                oViewModel = this.getModel("detailView"),
                oLineItemTable = this.byId("lineItemsList"),
                iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

            // Make sure busy indicator is displayed immediately when
            // detail view is displayed for the first time
            oViewModel.setProperty("/delay", 0);
            oViewModel.setProperty("/lineItemTableDelay", 0);

            oLineItemTable.attachEventOnce("updateFinished", function () {
                // Restore original busy indicator delay for line item table
                oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
            });

            // Binding the view will set it to not busy - so the view is always busy if it is not bound
            oViewModel.setProperty("/busy", true);
            // Restore original busy indicator delay for the detail view
            oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
        },

        /**
         * Set the full screen mode to false and navigate to master page
         */
        onCloseDetailPress: function () {
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
            // No item should be selected on master after detail page is closed
            this.getOwnerComponent().oListSelector.clearMasterListSelection();
            this.getRouter().navTo("master");
        },

        /**
         * Toggle between full and non full screen mode.
         */
        toggleFullScreen: function () {
            var bFullScreen = this.getModel("appView").getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
            if (!bFullScreen) {
                // store current layout and go full screen
                this.getModel("appView").setProperty("/previousLayout", this.getModel("appView").getProperty("/layout"));
                this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
            } else {
                // reset to previous layout
                this.getModel("appView").setProperty("/layout", this.getModel("appView").getProperty("/previousLayout"));
            }
        },

        onAfterRendering: function () {
            sap.ui.core.BusyIndicator.hide();
        },

        onDeleteMaster: function (oEvent, param1) {
            //alert('onDeleteMaster pressed. parm1 '+ param1);
            let objectId = this.byId("masterObjectId").getText();  // get id from view hidden input field

            let masterModel = this.getOwnerComponent().getModel("Objects");
            let masterData = masterModel.getData();
            let matchedRow = masterData.filter((row => row.ObjectID == objectId));
            let unMatchedRows = masterData.filter((row => row.ObjectID !== objectId));
            //alert('matchedRow: ' + JSON.stringify( matchedRow) ); alert('unMatchedRows: ' + JSON.stringify( unMatchedRows) ); //alert( JSON.stringify( allData) );


            let master_index_deleted = masterData.findIndex(function(item, i){
                return item.ObjectID === objectId
            });
            let master_rows_count = Object.keys(masterData).length;

            //alert('master_index_deleted: ' + master_index_deleted + '. total master row count: ' + master_rows_count);
            //alert('test - next master ' +  JSON.stringify( this.getView().getModel("Objects").getData()[master_index_deleted+1] ));


            if ( master_index_deleted < master_rows_count){
                this.next_master_row_objectID = masterData[master_index_deleted +1].ObjectID;
                //alert('next_master_row_objectID: ' + this.next_master_row_objectID);
            }

            // update master view model
            masterModel.setData(unMatchedRows);
            //console.log('master deleted');

            // update detail model
            //          let detailModel = this.getOwnerComponent().getModel("LineItems");
            let detailModel = this.getView().getModel("LineItems");  // set in this class
            let detailData = detailModel.getData();
            matchedRow = detailData.filter((row => row.ObjectID == objectId));
            unMatchedRows = detailData.filter((row => row.ObjectID !== objectId));
            //alert('matchedRow: ' + JSON.stringify( matchedRow) ); alert('unMatchedRows: ' + JSON.stringify( unMatchedRows) ); //alert( JSON.stringify( allData) );
            // delete detail rows
            //alert('detailmodel before: ' + JSON.stringify(detailModel.getData()));

            detailModel.setData(detailData.filter((row => row.ObjectID !== objectId)));  // all except one deleted
            //         this.getView().setModel(detailModel, "LineItems");  // NOTE: only need this if reference controller model vs this view model.this will add next json index to view
            //         this.getView().setModel(deletedModel, "LineItems");  // NOTE: only need this if reference controller model vs this view model


            // clear header section of detail view
            //let deletedModel = new JSONModel();
            //this.getView().setModel(deletedModel, "MasterItem");
            //console.log('detail rows deleted');

            // update master view and bind to detail rows
            this._onObjectMatched(oEvent);

        },

// edit icon fired
        onEditMaster: function (oEvent) {
            //alert('onEditMaster');  // edit icon on item row

            //alert('this view master model data: ' + JSON.stringify( this.getView().getModel("MasterItem").getData()) );
            // let objectId = this.byId("masterObjectId").getText();  // get id from view hidden input field
            let objectId = this.getView().getModel("MasterItem").getData().ObjectID;  // get id from view hidden input field
            //alert('objectId: ' + objectId);

            //  alert(JSON.stringify(oEvent.getSource().getBindingContext("Objects").getObject()));  // ?must be from table . row data un-named model/context
            //alert( JSON.stringify(this.getView().getModel("Objects").getData()));  // this works - gets all json data

            //let objectId = this.byId("masterObjectId").getText();  // get id from view hidden input field
            let matchedRow = this.getView().getModel("Objects").getData().filter((row => row.ObjectID == objectId));
            //alert('matched Master Row: ' +  JSON.stringify( matchedRow) );

            // this is the json row data. pass row to openSDialog component function
            this.getOwnerComponent().openSDialog(matchedRow[0]); // named model/context  -- can not pass indexed json obj [{}], must be {}
        },

// detail edit icon fired
        onEditDetail: function (oEvent) {
            //alert('onEditDetail');

            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );

            this.openSDialog(itemObject);
        },

// detail edit icon fired
        onAddDetail: function (oEvent) {
            alert('onAddDetail');
            this.openSDialog({bNewRecord: true});

        },
        onDeleteDetail: function (oEvent) {
            //alert('onDeleteDetail');
            let path = oEvent.getSource().getBindingContext("LineItems").getPath();
            let path_index = path.split("/").slice(-1).pop();

            // Cool code. This gets the json data for the selected row - Table/Item
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();  // gets active table row - xml
            //alert("item row data: " + JSON.stringify( itemObject ) );  // get LineItemID from this
            //alert('path: ' + path + '. path_index: '+ path_index + '. LineItemID: ' + itemObject.LineItemID); // gets item row index Tables/item

            let detailModel = this.getView().getModel("LineItems");  // set in this class
            let detailData = detailModel.getData();
            detailModel.setData(detailData.filter((row => row.LineItemID !== itemObject.LineItemID)));  // all except one deleted
        },

        onObjectListItemPress: function (oEvent) {
            //alert("onObjectListItemPress PRESS");
            let objectId = this.getView().getModel("MasterItem").getData().ObjectID;  // get id from view model vs from hidden input field
            //alert('objectId: ' + objectId);

            // cool code - gets row json fields
            let itemObject = oEvent.getSource().getBindingContext("LineItems").getObject();
            //alert("item row data: " + JSON.stringify( itemObject ) );
            let lineItemID = itemObject.LineItemID;

            // this breaks it
            //let lineItemID = this.getView().getModel("LineItems").getData().LineItemID;  // get id from view model vs from hidden input field
            //alert('lineItemID: ' + lineItemID);



            //let supplierPath = oEvent.getSource().getBindingContext("LineItems").getPath();
            //let    supplier = supplierPath.split("/").slice(-1).pop();
            //let   oNextUIState = 'MidColumnFullScreen';  // endColumnPages

            // TwoColumnsMidExpanded vs
            //this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
            // No item should be selected on master after detail page is closed
            //this.getOwnerComponent().oListSelector.clearMasterListSelection();
            //this.getRouter().navTo("master");

            //console.log('detail route to detailDetail');
            //alert('detail route to detailDetail. lineItemID: ' + lineItemID);
            this.getRouter().navTo("detailDetail", {lineItemID: lineItemID});


        },
        openSDialog: function (record) {
            //alert("openSDialog"); alert( JSON.stringify( record) );
            this._organizationDialog.open(record); // calls js file instantiated in component.js
        },

        onOpenViewSettings: function (oEvent) {
            //alert("onOpenViewSettings");

            var sDialogTab = "filter";
            if (oEvent.getSource() instanceof sap.m.Button) {
                var sButtonId = oEvent.getSource().getId();
                if (sButtonId.match("sort")) {
                    sDialogTab = "sort";
                } else if (sButtonId.match("group")) {
                    sDialogTab = "group";
                }
            }
            // load asynchronous XML fragment. Fragment name: ViewSettingsDialog
            // pull by fragment id = <ViewSettingsDialog	id="viewSettingsDialog"
            if (!this.byId("idDetailFilterDialog")) {
                console.log('creating fragment');
                Fragment.load({
                    id: this.getView().getId(),
                    name: "sap.ui.demo.masterdetail.view.DetailFilterDialog",
                    controller: this
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    this.getView().addDependent(oDialog);  // bind fragment view to this view (Master view)
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.open(sDialogTab);
                }.bind(this));
            } else {
                console.log('fragment already created');
                this.byId("idDetailFilterDialog").open(sDialogTab);
            }

        },
        onConfirmViewSettingsDialog: function (oEvent) {
           // alert('onConfirmViewSettingsDialog');

            var aFilterItems = oEvent.getParameters().filterItems,
                aFilters = [],
                aCaptions = [];

            // update filter state:
            // combine the filter array and the filter string
            aFilterItems.forEach(function (oItem) {
                switch (oItem.getKey()) {
                    case "Filter1" :
                        aFilters.push(new Filter("UnitNumber", FilterOperator.LE, 100));  // fragment key="Filter1"
                        break;
                    case "Filter2" :
                        aFilters.push(new Filter("UnitNumber", FilterOperator.GT, 100));
                        break;
                    default :
                        break;
                }
                aCaptions.push(oItem.getText());
            });

            this._oListFilterState.aFilter = aFilters;  // apply filter to List. Add to class variable _oListFilterState for use below
            console.log('detail filter captions: ' + JSON.stringify(aCaptions));
           // this._updateFilterBar(aCaptions.join(", "));
            this._applyFilterSearch();
            this._applySortGroup(oEvent);
        },

        _applySortGroup: function (oEvent) {
            //groupBy
            var mParams = oEvent.getParameters(),
                sPath,
                bDescending,
                aSorters = [];
            // apply sorter to binding
            // (grouping comes before sorting)
            if (mParams.groupItem) {
                sPath = mParams.groupItem.getKey();
                bDescending = mParams.groupDescending;
                var vGroup = this._oGroupFunctions[sPath];  // 2 groups defined above
                aSorters.push(new Sorter(sPath, bDescending, vGroup));
            }
            sPath = mParams.sortItem.getKey();
            bDescending = mParams.sortDescending;
            aSorters.push(new Sorter(sPath, bDescending));

            console.log('aSorters: ' + JSON.stringify(aSorters));

           // Bind Sort. this._oList.getBinding("items").sort(aSorters);
            this.byId("lineItemsList").getBinding("items").sort(aSorters);
        },
        _applyFilterSearch: function () {
            var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
                oViewModel = this.getModel("detailView");
            // Bind Filter
            this.byId("lineItemsList").getBinding("items").filter(aFilters, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aFilters.length !== 0) {
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("detailListNoDataWithFilterOrSearchText"));
            } else if (this._oListFilterState.aSearch.length > 0) {
                // only reset the no data text to default when no new search was triggered
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("detailListNoDataText"));
            }
        },

        _updateFilterBar: function (sFilterBarText) {
            var oViewModel = this.getModel("detailView");
            oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
            oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
        }

        //
    });

});