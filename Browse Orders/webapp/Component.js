sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models",
	"./controller/ListSelector",
	"./controller/ErrorHandler",
	'sap/ui/model/json/JSONModel'
], function (UIComponent, Device, models, ListSelector, ErrorHandler, JSONModel) {
	"use strict";

	return UIComponent.extend("sap.ui.demo.orderbrowser.Component", {

		metadata : {
			manifest : "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * In this method, the device models are set and the router is initialized.
		 * @public
		 * @override
		 */
		init : function () {
			this.oListSelector = new ListSelector();
			this._oErrorHandler = new ErrorHandler(this);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			/* removed manifest odata entry and added below local pull
				// set products demo model on this sample
			oProductsModel = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.mock', '/products.json'));
			oProductsModel.setSizeLimit(1000);
			this.setModel(oProductsModel, 'products');

			removed this from manifest

			,
		"dataSources": {
			"mainService": {
				"uri": "/here/goes/your/serviceUrl/",
				"type": "OData",
				"settings": {
					"odataVersion": "2.0",
					"localUri": "localService/metadata.xml"
				}
			}
		}

			 */
			// set products demo model on this sample
			var oOrder_Details, oOrders, oCustomer_Details, oEmployee, oProduct;
		//	oProductsModel = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mock'
		//		, '/Product.json'));
		//	this.setModel(oProductsModel, 'Product');

			alert(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mockdata'
				, '/Orders.json'));
			oOrders = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mockdata'
				, '/Orders.json'));
			this.setModel(oOrders, 'Orders');
			alert('1 done');
			oOrder_Details = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mockdata'
				, '/Order_Details.json'));
			this.setModel(oOrder_Details, 'Order_Details');
			oCustomer_Details = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mockdata'
				, '/Customer.json'));
			this.setModel(oCustomer_Details, 'Customer');
			oEmployee = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mockdata'
				, '/Employee.json'));
			this.setModel(oEmployee, 'Employee');
			oProduct = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.orderbrowser.localService.mockdata'
				, '/Product.json'));
			this.setModel(oProduct, 'Product');
			alert('all done');

			// test only:
			oOrders.attachRequestCompleted(function () {
				  alert('attachRequestCompleted. json obj: ' + JSON.stringify(oOrders.getData()));
			});

			// call the base component's init function and create the App view
			UIComponent.prototype.init.apply(this, arguments);

			// create the views based on the url/hash
			this.getRouter().initialize();
		},

		/**
		 * The component is destroyed by UI5 automatically.
		 * In this method, the ListSelector and ErrorHandler are destroyed.
		 * @public
		 * @override
		 */
		destroy : function () {
			this.oListSelector.destroy();
			this._oErrorHandler.destroy();
			// call the base component's destroy function
			UIComponent.prototype.destroy.apply(this, arguments);
		},

		/**
		 * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
		 * design mode class should be set, which influences the size appearance of some controls.
		 * @public
		 * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
		 */
		getContentDensityClass : function() {
			if (this._sContentDensityClass === undefined) {
				// check whether FLP has already set the content density class; do nothing in this case
				if (document.body.classList.contains("sapUiSizeCozy") || document.body.classList.contains("sapUiSizeCompact")) {
					this._sContentDensityClass = "";
				} else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					// "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}

	});
});
