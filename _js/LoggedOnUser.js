sap.ui.define([
    "sap/ui/base/Object"
], function (Object) {
    "use strict";

    alert('LoggedOnUser.js loaded');

    return Object.extend("_js.LoggedOnUser", {
        getLoggedOnUser : function () {
            console.log("Inside LoggedOnUser.getLoggedOnUser");
            // your code here
            return "Bill Steiner is logged on" ;
        }
    });
});