sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "./model/models",
    "./controller/ErrorHandler",
    "sap/ui/model/json/JSONModel",
], function (UIComponent, Device, models, ErrorHandler, JSONModel) {
    "use strict";

    return UIComponent.extend("test1.odata-test1.Component", {

        metadata: {
            manifest: "json"
        },

        /**
         * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
         * In this function, the device models are set and the router is initialized.
         * @public
         * @override
         */
        init: function () {
            // call the base component's init function
            UIComponent.prototype.init.apply(this, arguments);

            // initialize the error handler with the component
            this._oErrorHandler = new ErrorHandler(this);

            // set the device model
            this.setModel(models.createDeviceModel(), "device");

            // create the views based on the url/hash
            this.getRouter().initialize();

            //alert("component");


// cors issue
            // https://hanatests0020313550trial.hanatrial.ondemand.com/owi/customer5/webrule/webrule_services.xsodata/WebRules?$format=json
            console.log('test odata model');
            //console.log(btoa('API_USER' + ":" + 'billX2222222222'));    //QVBJX1VTRVI6YmlsbFgyMjIyMjIyMjIy

            /* test
            var xoModel = new sap.ui.model.odata.v2.ODataModel({
                serviceUrl: "https://hanatests0020313550trial.hanatrial.ondemand.com/owi/customer5/webrule/webrule_services.xsodata",
                headers: {
                    "Authorization": "Basic U1lTVEVNOkhhbmF0ZXN0YTExMTIyMjMzMw==",
                    "Access-Control-Allow-Origin": "*",
                    "Content-Type": "application/json",
                    "accept": "application/json",
                    "X-CSRF-Token": "Fetch"
                }
            });
            xoModel.attachRequestCompleted(function () {
                alert('attachRequestCompleted. xoModel: ' + xoModel);
            });

             */


            let oModel = new JSONModel();
            let url = "https://hanatests0020313550trial.hanatrial.ondemand.com/owi/customer5/webrule/webrule_services.xsodata/WebRules?$format=json";
            //let basic_auth =  'x'; // 'U1lTVEVNOkhhbmF0ZXN0YTExMTIyMjMzMw==';  //  btoa(USERNAME + ":" + PASSWORD)
            let basic_auth = 'QVBJX1VTRVI6YmlsbFgyMjIyMjIyMjIy';

//alert('get json');

            // NOTE: remove header below if you want login basic form presented for hana user login

            $.ajax({
                url: url,
                type: 'GET',
               // async: true,
                dataType: 'json',
                xhrFields: {withCredentials: true},
                accept: 'application/json',
                headers: {"Authorization": "Basic " + basic_auth},

            }).done(function (data) {
                alert('success');
                console.log('data: ' + JSON.stringify(data));
                //oModel.setData(data );
                oModel.setData(data.d.results);
                //this.setModel(oModel,"WebRules");
                //console.log('json oModel: ' + JSON.stringify(oModel.getData()));
                // oModel = new JSONModel({"WebRules":data});
                // alert('json oModel: ' + JSON.stringify(oModel.getData()));


                console.log('row 1 value: ' + JSON.stringify(data.d.results[0].RULEVALUE));
                console.log('row 2 value: ' + JSON.stringify(data.d.results[1].RULEVALUE));

                //let html = ('Row 1 value: ' + JSON.stringify(data.d.results[0].RULEVALUE) + "<br> " + 'Row 2 value: ' + JSON.stringify(data.d.results[1].RULEVALUE)
                //+ '<br><br> oData: <br>' +  JSON.stringify(data));
                //$('#api_response').html( html ) ;

            })
                .fail(function (xhr, textStatus, error) {
                    var title, message;
                    switch (xhr.status) {
                        case 403 :
                            if (xhr.responseJSON) {
                                title = xhr.responseJSON.errorSummary;
                            } else {
                                title = "Forbidden Error 403"
                            }
                            message = 'Please login to your SAP account and try again.';
                            break;
                        default :
                            title = 'Invalid URL or Cross-Origin Request Blocked (CORS)';
                            message = 'You must explicitly add this site (' + window.location.origin + ') to the list of allowed websites.';
                            break;
                    }
                    alert(title + ': ' + message + " " + textStatus + ' ' + error);
                });

            // set data model - WebRules
            this.setModel(oModel, "WebRules");
            //alert('oModel WebRules after ajax: ' + JSON.stringify(oModel.getData("WebRules")));


// end
        },

        /**
         * The component is destroyed by UI5 automatically.
         * In this method, the ErrorHandler is destroyed.
         * @public
         * @override
         */
        destroy: function () {
            this._oErrorHandler.destroy();
            // call the base component's destroy function
            UIComponent.prototype.destroy.apply(this, arguments);
        },

        /**
         * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
         * design mode class should be set, which influences the size appearance of some controls.
         * @public
         * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
         */
        getContentDensityClass: function () {
            if (this._sContentDensityClass === undefined) {
                // check whether FLP has already set the content density class; do nothing in this case
                // eslint-disable-next-line sap-no-proprietary-browser-api
                if (document.body.classList.contains("sapUiSizeCozy") || document.body.classList.contains("sapUiSizeCompact")) {
                    this._sContentDensityClass = "";
                } else if (!Device.support.touch) { // apply "compact" mode if touch is not supported
                    this._sContentDensityClass = "sapUiSizeCompact";
                } else {
                    // "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
                    this._sContentDensityClass = "sapUiSizeCozy";
                }
            }
            return this._sContentDensityClass;
        }

    });


});