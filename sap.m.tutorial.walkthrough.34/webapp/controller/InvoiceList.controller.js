sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/UIComponent"
], function (Controller, JSONModel, formatter, Filter, FilterOperator, UIComponent) {
	"use strict";

	return Controller.extend("sap.ui.demo.walkthrough.controller.InvoiceList", {

		formatter: formatter,

		onInit: function () {
			var oViewModel = new JSONModel({
				currency: "EUR"
			});
			this.getView().setModel(oViewModel, "view");

// start app using: http://ui5.devlocal.com/examples/sap.m.tutorial.walkthrough.34/webapp/

			//http://ui5.devlocal.com/examples/sap.m.tutorial.walkthrough.34/webapp/localService/mockdata/Invoices.json
			let listModel = new JSONModel("http://ui5.devlocal.com/examples/sap.m.tutorial.walkthrough.34/webapp/localService/mockdata/Invoices.json");
			this.getView().setModel(listModel, 'invoice');

			// Testing jModel data response. This is used for testing only // similar to ajax.complete function. can only see data once ajax thread is done.
			listModel.attachRequestCompleted(function () {
			    var data = JSON.stringify(listModel.getData());
			    // alert('listModel attachRequestCompleted json obj: ' + JSON.stringify(data));
			 	//let inv = {"Invoices": listModel.getData()};   alert('listModel inv: ' + JSON.stringify(inv));
			});

		},

		onAfterRendering(){
			//alert('onAfterRendering');  // this varies by browser firefox vs chrome with attachRequestCompleted timing.
		},

		onFilterInvoices: function (oEvent) {
			// model testing. test it
			let test = this.getView().getModel('invoice').getData();
			alert('test invoice: ' + JSON.stringify(test));
			let test2 = this.getView().getModel('view').getData();
			alert('test view: ' + JSON.stringify(test2));


			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("ProductName", FilterOperator.Contains, sQuery));
			}

			// filter binding
			var oList = this.byId("invoiceList");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		},

		onPress: function (oEvent) {

			var oItem = oEvent.getSource();
			var oRouter = UIComponent.getRouterFor(this);
			oRouter.navTo("detail", {
				invoicePath: oItem.getBindingContext("invoice").getPath().substr(1)
			});
		}
	});

});