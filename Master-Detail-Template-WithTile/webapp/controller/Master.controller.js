sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/model/Sorter",
    "sap/ui/model/FilterOperator",
    "sap/m/GroupHeaderListItem",
    "sap/ui/Device",
    "sap/ui/core/Fragment",
    "../model/formatter"
], function (BaseController, JSONModel, Filter, Sorter, FilterOperator, GroupHeaderListItem, Device, Fragment, formatter) {
    "use strict";

    return BaseController.extend("sap.ui.demo.masterdetail.controller.Master", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        /**
         * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
         * @public
         */
        onInit: function () {

            sap.ui.core.BusyIndicator.show();


            // Control state model
            // get id=list (List element)
            var oList = this.byId("list"),
                oViewModel = this._createViewModel(),
                // Put down master list's original value for busy indicator delay,
                // so it can be restored later on. Busy handling on the master list is
                // taken care of by the master list itself.
                iOriginalBusyDelay = oList.getBusyIndicatorDelay();

// set up groupBy logic - 2 groups defined below
            this._oGroupFunctions = {
                UnitNumber: function (oContext) {
                    var iNumber = oContext.getProperty('UnitNumber'),
                        key, text;
                    if (iNumber <= 20) {
                        key = "LE20";
                        text = this.getResourceBundle().getText("masterGroup1Header1");
                    } else {
                        key = "GT20";
                        text = this.getResourceBundle().getText("masterGroup1Header2");
                    }
                    return {
                        key: key,
                        text: text
                    };
                }.bind(this)
            };

            this._oList = oList;
            // keeps the filter and search state
            this._oListFilterState = {
                aFilter: [],
                aSearch: []
            };

            this.setModel(oViewModel, "masterView");
            // Make sure, busy indication is showing immediately so there is no
            // break after the busy indication for loading the view's meta data is
            // ended (see promise 'oWhenMetadataIsLoaded' in AppController)
            oList.attachEventOnce("updateFinished", function () {
                // Restore original busy indicator delay for the list
                oViewModel.setProperty("/delay", iOriginalBusyDelay);
            });

            this.getView().addEventDelegate({
                onBeforeFirstShow: function () {
                    this.getOwnerComponent().oListSelector.setBoundMasterList(oList);  // component class variable of ListSelector.js
                }.bind(this)
            });

            this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
            this.getRouter().attachBypassed(this.onBypassed, this);
            //alert('master onInit done');
        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * After list data is available, this handler method updates the
         * master list counter
         * @param {sap.ui.base.Event} oEvent the update finished event
         * @public
         */
        onUpdateFinished: function (oEvent) {
            // update the master list object counter after new data is loaded
            this._updateListItemCount(oEvent.getParameter("total"));
        },

        /**
         * Event handler for the master search field. Applies current
         * filter value and triggers a new search. If the search field's
         * 'refresh' button has been pressed, no new search is triggered
         * and the list binding is refresh instead.
         * @param {sap.ui.base.Event} oEvent the search event
         * @public
         */
        onSearch: function (oEvent) {
            if (oEvent.getParameters().refreshButtonPressed) {
                // Search field's 'refresh' button has been pressed.
                // This is visible if you select any master list item.
                // In this case no new search is triggered, we only
                // refresh the list binding.
                this.onRefresh();
                return;
            }

            var sQuery = oEvent.getParameter("query");

            if (sQuery) {
                this._oListFilterState.aSearch = [new Filter("Name", FilterOperator.Contains, sQuery)];
            } else {
                this._oListFilterState.aSearch = [];
            }
            this._applyFilterSearch();

        },

        /**
         * Event handler for refresh event. Keeps filter, sort
         * and group settings and refreshes the list binding.
         * @public
         */
        onRefresh: function () {
            this._oList.getBinding("items").refresh();
        },

        /**
         * Event handler for the filter, sort and group buttons to open the ViewSettingsDialog.
         * @param {sap.ui.base.Event} oEvent the button press event
         * @public
         */
        onOpenViewSettings: function (oEvent) {
            console.log('open filter fragment');

            var sDialogTab = "filter";
            if (oEvent.getSource() instanceof sap.m.Button) {
                var sButtonId = oEvent.getSource().getId();
                if (sButtonId.match("sort")) {
                    sDialogTab = "sort";
                } else if (sButtonId.match("group")) {
                    sDialogTab = "group";
                }
            }
            // load asynchronous XML fragment. Fragment name: ViewSettingsDialog
            // pull by fragment id = <ViewSettingsDialog	id="viewSettingsDialog"
            if (!this.byId("viewSettingsDialog")) {
                console.log('creating fragment');
                Fragment.load({
                    id: this.getView().getId(),
                    name: "sap.ui.demo.masterdetail.view.ViewSettingsDialog",
                    controller: this
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    this.getView().addDependent(oDialog);  // bind fragment view to this view (Master view)
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.open(sDialogTab);
                }.bind(this));
            } else {
                console.log('fragment already created');
                this.byId("viewSettingsDialog").open(sDialogTab);
            }
        },

        /**
         * Event handler called when ViewSettingsDialog has been confirmed, i.e.
         * has been closed with 'OK'. In the case, the currently chosen filters, sorters or groupers
         * are applied to the master list, which can also mean that they
         * are removed from the master list, in case they are
         * removed in the ViewSettingsDialog.
         * @param {sap.ui.base.Event} oEvent the confirm event
         * @public
         */
        onConfirmViewSettingsDialog: function (oEvent) {
            var aFilterItems = oEvent.getParameters().filterItems,
                aFilters = [],
                aCaptions = [];

            // update filter state:
            // combine the filter array and the filter string
            aFilterItems.forEach(function (oItem) {
                switch (oItem.getKey()) {
                    case "Filter1" :
                        aFilters.push(new Filter("UnitNumber", FilterOperator.LE, 100));  // fragment key="Filter1"
                        break;
                    case "Filter2" :
                        aFilters.push(new Filter("UnitNumber", FilterOperator.GT, 100));
                        break;
                    default :
                        break;
                }
                aCaptions.push(oItem.getText());
            });

            this._oListFilterState.aFilter = aFilters;  // apply filter to List. Add to class variable _oListFilterState for use below
            this._updateFilterBar(aCaptions.join(", "));
            this._applyFilterSearch();
            this._applySortGroup(oEvent);
        },

        /**
         * Apply the chosen sorter and grouper to the master list
         * @param {sap.ui.base.Event} oEvent the confirm event
         * @private
         */
        _applySortGroup: function (oEvent) {
            //groupBy
            var mParams = oEvent.getParameters(),
                sPath,
                bDescending,
                aSorters = [];
            // apply sorter to binding
            // (grouping comes before sorting)
            if (mParams.groupItem) {
                sPath = mParams.groupItem.getKey();
                bDescending = mParams.groupDescending;
                var vGroup = this._oGroupFunctions[sPath];  // 2 groups defined above
                aSorters.push(new Sorter(sPath, bDescending, vGroup));
            }
            sPath = mParams.sortItem.getKey();
            bDescending = mParams.sortDescending;
            aSorters.push(new Sorter(sPath, bDescending));
            this._oList.getBinding("items").sort(aSorters);
        },

        /**
         * Event handler for the list selection event
         * @param {sap.ui.base.Event} oEvent the list selectionChange event
         * @public
         */
        onSelectionChange: function (oEvent) {
            //alert('on selection changed');

            //alert( JSON.stringify( this._oList.getBinding("items")) ); // cyclic error
            var    mParams = oEvent.getParameters();
            for (let property in mParams) {
                console.log("onSelectionChange: " + property  + '. value: ' + mParams[property]);
                // has listItem, and listItems - clone of index value
            }


            var oList = oEvent.getSource(),
                bSelected = oEvent.getParameter("selected");
       	// alert(oEvent.getParameter("listItem") + " ---- " +  oEvent.getSource() + '. bSelected: ' + bSelected);
       	// Element sap.m.ObjectListItem#__item0-__clone0 ---- Element sap.m.List#container-masterdetail---master--list. bSelected: true

            // this is oItem (master row selected) - param passed to _showDetail.
            // there is no master view id of "listItem" so assume it is magical from <ObjectListItem>
            var oItem = oEvent.getParameter("listItem");
            //	alert( oItem.getBindingContext("Objects").getProperty("ObjectID") );  // ObjectID_12
            //		alert( oItem.getBindingContext("Objects").getProperty("Name"));
            // the below will get the row for the master item selected
            //alert( JSON.stringify( oItem.getBindingContext("Objects").getProperty()) );
            //{"ObjectID":"ObjectID_11","Name":"Object 11","Attribute1":"Attribute U","Attribute2":"Attribute V","UnitOfMeasure":"UoM","UnitNumber":61}

            // this gets only 1 row - the master row selected
            let masterRowSelected = oItem.getBindingContext("Objects").getProperty();
            console.log('Master::onSelectionChange. masterRowSelected: ' + JSON.stringify(masterRowSelected) ); // show all master fields

            // skip navigation when deselecting an item in multi selection mode
            if (!(oList.getMode() === "MultiSelect" && !bSelected)) {
                // get the list item, either from the listItem parameter or from the event's source itself (will depend on the device-dependent mode).
                this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
            }
        },

        /**
         * Event handler for the bypassed event, which is fired when no routing pattern matched.
         * If there was an object selected in the master list, that selection is removed.
         * @public
         */
        onBypassed: function () {
            this._oList.removeSelections(true);
        },

        /**
         * Used to create GroupHeaders with non-capitalized caption.
         * These headers are inserted into the master list to
         * group the master list's items.
         * @param {Object} oGroup group whose text is to be displayed
         * @public
         * @returns {sap.m.GroupHeaderListItem} group header with non-capitalized caption.
         */
        createGroupHeader: function (oGroup) {
            return new GroupHeaderListItem({
                title: oGroup.text,
                upperCase: false
            });
        },

        /**
         * Event handler for navigating back.
         * We navigate back in the browser history
         * @public
         */
        onNavBack: function () {
            //alert('nav back');
            //this.getModel("appView").setProperty("/layout", "OneColumn");
            //alert(this.getModel("appView").getProperty("/layout"));

            // i added this to close out detail page first
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);  // may not need this one
            this.getModel("appView").setProperty("/layout", "OneColumn");
            // No item should be selected on master after detail page is closed
            this.getOwnerComponent().oListSelector.clearMasterListSelection(); // need this or will not work if selecting same row twice


            // eslint-disable-next-line sap-no-history-manipulation
            history.go(-1);
        },

        /* =========================================================== */
        /* begin: internal methods                                     */
        /* =========================================================== */


        _createViewModel: function () {
            return new JSONModel({
                isFilterBarVisible: false,
                filterBarLabel: "",
                delay: 0,
                title: this.getResourceBundle().getText("masterTitleCount", [0]),
                noDataText: this.getResourceBundle().getText("masterListNoDataText"),
                sortBy: "Name",
                groupBy: "None"
            });
        },

        // executed once at start off app - route = ""
        _onMasterMatched: function () {

              alert('master::_onMasterMatched');
            //	alert(JSON.stringify( this.getOwnerComponent().getModel().getData()) );
            // error not defined		alert('appView: ' + JSON.stringify( this.getOwnerComponent().getModel('appView').getData()) );
            //	alert('LineItems: ' + JSON.stringify( this.getOwnerComponent().getModel('LineItems').getData()) );
            //		alert('Objects: ' + JSON.stringify( this.getOwnerComponent().getModel('Objects').getData()) );
            //	alert('Objects: ' + JSON.stringify( this.getOwnerComponent().getModel().getData()) );
            //	alert('/Objects: ' + JSON.stringify( this.getOwnerComponent().getModel().getProperty("/Objects")) );


            // not defined		alert('1 master::_onMasterMatched. getdata: ' + this.getModel("appView").getData());
            // not defined		alert('2 master::_onMasterMatched. getdata: ' + this.getModel("appView").getData());


            //Set the layout property of the FCL control to 'OneColumn'

            //alert('typeof this: ' + typeof (this) + ' - ' + typeof ( this.getModel("appView")));


           // if(this){ this.destroy()};

           // this.getModel("appView").setProperty("/layout", "OneColumn");  // if fails then chk errors in App controller


            // Can change Master layout to 2 columns here - but will have to add code to populate column 2 , else it shows empty page.
            //this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");

        },

        /**
         * Shows the selected item on the detail page
         * On phones a additional history entry is created
         * @param {sap.m.ObjectListItem} oItem selected Item
         * @private
         */
        _showDetail: function (oItem) {
            //alert('master row item clicked');

            let masterRowSelected = oItem.getBindingContext("Objects").getProperty();
            console.log('Master::_showDetail. oItem: ' + JSON.stringify(masterRowSelected));
            //_showDetail. oItem: {"ObjectID":"ObjectID_11","Name":"Object 11","Attribute1":"Attribute U","Attribute2":"Attribute V","UnitOfMeasure":"UoM","UnitNumber":61}
            //alert(oItem.getBindingContext("Objects").getProperty("ObjectID"));  //ObjectID_11

            var bReplace = !Device.system.phone;
            // set the layout property of FCL control to show two columns

            // not sure why code breaks when changed to use getOwnerComponent
            //alert(this.getModel("appView").getProperty("/layout"));
            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");  // change layout. appView set in App.js

            // route to detail page (route = 'object') and pass ObjectID to filter on for page view
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext("Objects").getProperty("ObjectID"),
                //         oItem: oItem      // if you do this, need to add parm2 to manifest
            }, bReplace);
        },

        /**
         * Sets the item count on the master list header
         * @param {integer} iTotalItems the total number of items in the list
         * @private
         */
        _updateListItemCount: function (iTotalItems) {
            var sTitle;
            // only update the counter if the length is final
            if (this._oList.getBinding("items").isLengthFinal()) {
                sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
                this.getModel("masterView").setProperty("/title", sTitle);
            }
        },

        /**
         * Internal helper method to apply both filter and search state together on the list binding
         * @private
         */
        _applyFilterSearch: function () {
            var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
                oViewModel = this.getModel("masterView");
            this._oList.getBinding("items").filter(aFilters, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aFilters.length !== 0) {
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
            } else if (this._oListFilterState.aSearch.length > 0) {
                // only reset the no data text to default when no new search was triggered
                oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
            }
        },

        /**
         * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
         * @param {string} sFilterBarText the selected filter value
         * @private
         */
        _updateFilterBar: function (sFilterBarText) {
            var oViewModel = this.getModel("masterView");
            oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
            oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
        },

        onAfterRendering: function () {
            sap.ui.core.BusyIndicator.hide();
        }
    });

});