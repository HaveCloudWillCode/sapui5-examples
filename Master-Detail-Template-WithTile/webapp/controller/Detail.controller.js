sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/m/library"
], function (BaseController, JSONModel, formatter, mobileLibrary) {
    "use strict";

    // shortcut for sap.m.URLHelper
    var URLHelper = mobileLibrary.URLHelper;

    return BaseController.extend("sap.ui.demo.masterdetail.controller.Detail", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        onInit: function () {
            sap.ui.core.BusyIndicator.show();

            // Model used to manipulate control states. The chosen values make sure,
            // detail page is busy indication immediately so there is no break in
            // between the busy indication for loading the view's meta data

            // create view model - and name it detailView
            var oViewModel = new JSONModel({
                busy: false,
                delay: 0,
                lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading")
            });

            this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

            this.setModel(oViewModel, "detailView");

            //this.getOwnerComponent().getModel("Objects").metadataLoaded().then(this._onMetadataLoaded.bind(this));
        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Event handler when the share by E-Mail button has been clicked
         * @public
         */
        onSendEmailPress: function () {
            var oViewModel = this.getModel("detailView");

            URLHelper.triggerEmail(
                null,
                oViewModel.getProperty("/shareSendEmailSubject"),
                oViewModel.getProperty("/shareSendEmailMessage")
            );
        },


        /**
         * Updates the item count within the line item table's header
         * @param {object} oEvent an event containing the total number of items in the list
         * @private
         */
        onListUpdateFinished: function (oEvent) {
            var sTitle,
                iTotalItems = oEvent.getParameter("total"),
                oViewModel = this.getModel("detailView");  // set in this oninit

            // only update the counter if the length is final
            if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
                if (iTotalItems) {
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
                } else {
                    //Display 'Line Items' instead of 'Line items (0)'
                    sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
                }
                oViewModel.setProperty("/lineItemListTitle", sTitle);
            }
        },

        /* =========================================================== */
        /* begin: internal methods                                     */
        /* =========================================================== */

        /**
         * Binds the view to the object path and expands the aggregated line items.
         * @function
         * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
         * @private
         */
        _onObjectMatched: function (oEvent) {
            //alert('_onObjectMatched');
            let sObjectId = oEvent.getParameter("arguments").objectId;
            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
            //   this.getModel("appView").setProperty("/app", "TwoColumnsMidExpanded");

            //let masterRowSelected = oEvent.getParameter("arguments").oItem;

            /* original ------------------  calls this._bindView
            var sObjectId =  oEvent.getParameter("arguments").objectId;
            this.getModel("appView").setProperty("/layout", "TwoColumnsMidExpanded");
            this.getModel().metadataLoaded().then( function() {
                var sObjectPath = this.getModel().createKey("Objects", {
                    ObjectID :  sObjectId
                });
                this._bindView("/" + sObjectPath);
            }.bind(this));
            */


            //alert(JSON.stringify( oEvent.getParameter("arguments")));  // {"objectId":"ObjectID_12"}
            //alert('Detail page._onObjectMatched for _product: ' + oEvent.getParameter("arguments").objectId);
            //alert('this._product: ' + this._product + ". sObjectId: " + sObjectId);   // this._product: ObjectID_12. sObjectId: ObjectID_12

            var allLineItems = this.getOwnerComponent().getModel('LineItems').getData();
            var detailLineItems = [];
            var dFound = allLineItems.filter(function (el, n) {
                if (el.ObjectID === sObjectId) {
                    //alert(JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                    detailLineItems.push(allLineItems[n]);
                    return;
                }
            });
            //console.log("detailRow: " +  JSON.stringify( detailLineItems) );
            //alert("***detailRows: " +  JSON.stringify( detailLineItems) );

            var allObjectRows = this.getOwnerComponent().getModel('Objects').getData();
            var objectRow = [];
            var oFound = allObjectRows.filter(function (el, n) {
                if (el.ObjectID === sObjectId) {
                    //alert(JSON.stringify(el));					//alert('n: ' + n + lineItems[n]);
                    objectRow.push(allObjectRows[n]);
                    return;
                }
            });
            //console.log("objectRow: " +  JSON.stringify( objectRow) );
            //alert("***objectRow: " +  JSON.stringify( objectRow) );

            var detailsModel = new sap.ui.model.json.JSONModel();
            detailsModel.setData(detailLineItems);
            this.getView().setModel(detailsModel, "LineItems");

            var masterModel = new sap.ui.model.json.JSONModel();
            masterModel.setData(objectRow[0]);  // can not be indexed obj
            this.getView().setModel(masterModel, "MasterItem");
            // ***objectRow: [{"ObjectID":"ObjectID_10","Name":"Object 10","Attribute1":"Attribute S","Attribute2":"Attribute T","UnitOfMeasure":"UoM","UnitNumber":106}]

        },

        /**
         * Binds the view to the object path. Makes sure that detail view displays
         * a busy indicator while data for the corresponding element binding is loaded.
         * @function
         * @param {string} sObjectPath path to the object to be bound to the view.
         * @private
         */
        _bindView: function (sObjectPath) {
            alert("bind view");
            // Set busy indicator during view binding
            var oViewModel = this.getModel("detailView");

            // If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
            oViewModel.setProperty("/busy", false);

            this.getView().bindElement({
                path: sObjectPath,
                events: {
                    change: this._onBindingChange.bind(this),
                    dataRequested: function () {
                        oViewModel.setProperty("/busy", true);
                    },
                    dataReceived: function () {
                        oViewModel.setProperty("/busy", false);
                    }
                }
            });
        },

        _onBindingChange: function () {
            alert('on bind change');
            var oView = this.getView(),
                oElementBinding = oView.getElementBinding();

            // No data for the binding
            if (!oElementBinding.getBoundContext()) {
                this.getRouter().getTargets().display("detailObjectNotFound");
                // if object could not be found, the selection in the master list
                // does not make sense anymore.
                this.getOwnerComponent().oListSelector.clearMasterListSelection();
                return;
            }

            var sPath = oElementBinding.getPath(),
                oResourceBundle = this.getResourceBundle(),
                oObject = oView.getModel().getObject(sPath),
                sObjectId = oObject.ObjectID,
                sObjectName = oObject.Name,
                oViewModel = this.getModel("detailView");

            this.getOwnerComponent().oListSelector.selectAListItem(sPath);

            oViewModel.setProperty("/shareSendEmailSubject",
                oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
            oViewModel.setProperty("/shareSendEmailMessage",
                oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
        },

        _onMetadataLoaded: function () {
            // Store original busy indicator delay for the detail view
            var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
                oViewModel = this.getModel("detailView"),
                oLineItemTable = this.byId("lineItemsList"),
                iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

            // Make sure busy indicator is displayed immediately when
            // detail view is displayed for the first time
            oViewModel.setProperty("/delay", 0);
            oViewModel.setProperty("/lineItemTableDelay", 0);

            oLineItemTable.attachEventOnce("updateFinished", function () {
                // Restore original busy indicator delay for line item table
                oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
            });

            // Binding the view will set it to not busy - so the view is always busy if it is not bound
            oViewModel.setProperty("/busy", true);
            // Restore original busy indicator delay for the detail view
            oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
        },

        /**
         * Set the full screen mode to false and navigate to master page
         */
        onCloseDetailPress: function () {
           // alert('close detail press');
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
// i added this to close out detail window
            this.getModel("appView").setProperty("/layout", "OneColumn");

            // No item should be selected on master after detail page is closed
            this.getOwnerComponent().oListSelector.clearMasterListSelection();
            this.getRouter().navTo("master");
        },

        /**
         * Toggle between full and non full screen mode.
         */
        toggleFullScreen: function () {
            var bFullScreen = this.getModel("appView").getProperty("/actionButtonsInfo/midColumn/fullScreen");
            this.getModel("appView").setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
            if (!bFullScreen) {
                // store current layout and go full screen
                this.getModel("appView").setProperty("/previousLayout", this.getModel("appView").getProperty("/layout"));
                this.getModel("appView").setProperty("/layout", "MidColumnFullScreen");
            } else {
                // reset to previous layout
                this.getModel("appView").setProperty("/layout", this.getModel("appView").getProperty("/previousLayout"));
            }
        },

        onAfterRendering: function () {
            sap.ui.core.BusyIndicator.hide();
        },

        onDeleteMaster: function (oEvent, param1) {
            //alert('onDeleteMaster pressed. parm1 '+ param1);
            let objectId = this.byId("masterObjectId").getText();

            let masterModel = this.getOwnerComponent().getModel("Objects");
            let masterData = masterModel.getData();
            let matchedRow = masterData.filter((row => row.ObjectID == objectId));
            let unMatchedRows = masterData.filter((row => row.ObjectID !== objectId));
            //alert('matchedRow: ' + JSON.stringify( matchedRow) ); alert('unMatchedRows: ' + JSON.stringify( unMatchedRows) ); //alert( JSON.stringify( allData) );

            // update master model
            masterModel.setData(unMatchedRows);

            // update detail model
            let detailModel = this.getOwnerComponent().getModel("LineItems");
            let detailData = detailModel.getData();
            matchedRow = detailData.filter((row => row.ObjectID == objectId));
            unMatchedRows = detailData.filter((row => row.ObjectID !== objectId));
            //alert('matchedRow: ' + JSON.stringify( matchedRow) ); alert('unMatchedRows: ' + JSON.stringify( unMatchedRows) ); //alert( JSON.stringify( allData) );

            detailModel.setData(detailData.filter((row => row.ObjectID !== objectId)));
            this.getModel("appView").setProperty("/layout", "OneColumn");
        },

        onEditMaster: function(oEvent){
            alert(this.getModel("appView").getProperty("/layout"));
            this.getModel("appView").setProperty("/layout", "OneColumn");
            alert(this.getModel("appView").getProperty("/layout"));

            // eslint-disable-next-line sap-no-history-manipulation
            history.go(-1);
        }

        //
    });

});