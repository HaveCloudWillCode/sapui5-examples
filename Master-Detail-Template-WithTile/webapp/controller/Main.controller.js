sap.ui.define(['jquery.sap.global', 'sap/ui/core/mvc/Controller', 'sap/m/MessageToast'],
    function(jQuery, Controller, MessageToast) {
        "use strict";

        var PageController = Controller.extend("sap.ui.demo.masterdetail.controller.Main", {

            onInit: function () {
                alert("main page");
            },

            press : function(oEvent, navTo) {
                MessageToast.show("The GenericTile is pressed. " + navTo);


                jQuery.sap.log.info("**BaseController.navTo:" + navTo);
                if (navTo) {
                    console.log('route to: '+ navTo);
                    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                    oRouter.navTo(navTo);
                } else {
                    if (oEvent) {
                        jQuery.sap.log.info("**BaseController.navTo is blank for source: " + oEvent.getSource().getId());
                    } else {
                        jQuery.sap.log.info("**BaseController.navTo is blank");
                    }
                }
            }
        });

        return PageController;
    });