sap.ui.controller("app1.main", {

    /**
     * Called when a controller is instantiated and its View controls (if available) are already created.
     * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
     * @memberOf view.check
     *
     * http://localhost:63342/html/examples/ui5%20list%20and%20table%20bindng%20example/index.html?edit=false
     */

    _constants: {
        table: {
            id: "dynamicTable",
            itemBindingPath: "/businessData",
            columnLabelTexts: ["Sales Month", "Marital Status", "Customer Gender", "Sales Quarter", "Cost", "Unit Price", "Gross Profit", "Sales Revenue"],
            templateCellLabelTexts: ["{Sales_Month}", "{Marital Status}", "{Customer Gender}", "{Sales_Quarter}", "{Cost}", "{Unit Price}", "{Gross Profit}", "{Sales Revenue}"],
            modulePath: "/dynamicTableData.json",
            tableTitle: "Dynamic Table build example",
            growing: "true",
            growingThreshold: 10
        }
    },

    onInit: function () {
        var sUrl = "mock_data.json";  // list data



        // list example data. ajax example to pull local json object: DATA->GROUPS->*rows*. All feed code inside ajax thread.
        jQuery.ajax({
            type: "GET",
            dataType: "json",
            url: sUrl,
            context: this,
            error: function (jqXHR, textStatus, errorThrown) {
                var sMessage = jqXHR.status + " " + jqXHR.statusText + " " + jqXHR.responseText;
                jQuery.sap.log.error("Data loading", sMessage, "index.html");
                sap.m.MessageToast.show(sMessage)
            },
            success: function (oData, textStatus, jqXHR) {
                if (oData === null || oData === undefined) {
                    var sMessage = "WARNING. Received a null or undefined response object.";
                    jQuery.sap.log.warning("Data loading", sMessage, "index.html");
                    sap.m.MessageToast.show(sMessage);
                    return;
                }
                // good data pull
                // get url parm
                let param_edit = jQuery.sap.getUriParameters().get("edit");
                if (typeof param_edit !== 'undefined') {
                    var editable = (param_edit === 'true');
                } else {
                    editable = false;
                }


                // list example - no model name ('/DATA/GROUPS')
                var oModel = new sap.ui.model.json.JSONModel(oData);
                var oList = this.byId("list");
                oList.setModel(oModel);

                // list example - with model name ('/DATA/GROUPS')
                var oListNamedModel = this.byId("listNamedModel");
                oListNamedModel.setModel(oModel, "modelListView");

                // table example - no model name
                var oTable = this.byId("table");
                oTable.setModel(oModel);

                // table example - with model name
                var oTableNamedModel = this.byId("tableNamedModel");
                oTableNamedModel.setModel(oModel, "model");


                // example of setData (model name: modelPath)
                var model = new sap.ui.model.json.JSONModel();
                model.setData({
                    dataCollection: [
                        {firstName: "Albert", lastName: "Einstein", age: 76, born: 1879},
                        {firstName: "Thomas", lastName: "Edison", age: 84, born: 1847},
                        {firstName: "Neil", lastName: "Armstrong", age: 82, born: 1930}
                    ]
                });

                // add model var for Table edit mode (editable)
                model.setProperty("/editable",editable);

                /*
               // good tester examples of adding model attributes
               // replace above with this to test:    var data = {'param1': sParam1, 'apiKey': "12345", 'params': [], 'employeeName': "Bill"};  //  [{'employeeName': "Bill"}, {'filters': []}];   alert(JSON.stringify(data));
               vModel.setProperty("/params/test", {"key":"Acura"});
               // test it  -- Can set a model attribute after model is instantiated
               alert('get params:' + JSON.stringify(vModel.getProperty("/params/test")) );
               var data = JSON.stringify(vModel.getProperty("/params/test"));
               var json = JSON.parse(data);
               alert ('test : ' + json);  alert ('test key: ' + json.key);
               vModel.setProperty("/test", json.key);
               */

                // =============================================================================================================
                // dynamic table build example
                var oTable = this.getView().byId(this._constants.table.id);  // dynamicTable view html element object by id
                var tableMetaData = this._constants.table;  // table json obj
                var oTablePath = jQuery.sap.getModulePath("app1/", tableMetaData.modulePath);  //  alert('path: ' + oTablePath);  // path: https://openui5.hana.ondemand.com/resources//ChartContainerData2.json
                var oTableModel = new sap.ui.model.json.JSONModel(oTablePath);  // get data
                model.setProperty("/dynamicTableTitle",this._constants.table.tableTitle); // alert(this._constants.table.tableTitle);
                model.setProperty("/growing",this._constants.table.growing);
                model.setProperty("/growingThreshold",this._constants.table.growingThreshold);
                //call helper.js function
                _updateTable(oTable, oTableModel, tableMetaData);
                // =============================================================================================================

                // testing only
                /*
                        testMessage('fired from main controller. Checking for valide json data response');

                        oTableModel.attachRequestCompleted(function () {
                            var data = JSON.stringify(oTableModel.getData());
                            var data_json = JSON.parse(data);
                            //alert('data_json: ' + JSON.stringify(data_json));
                        });
                */


                this.getView().setModel(model, "modelView");

            }
        });  // end ajax function
    },

    testMessage: function(message) {
        alert('local test message');
    },

    rowClicked: function(oEventArgs, modelView) {
        //alert('clicked' );

        var mParams = oEventArgs.getParameters();
        jQuery.sap.log.info(mParams);  // no other text will print out object to console
        //console.log('id: ' + mParams.id, ' value: ' + mParams.propertyValue);
        //console.log('changedItem: ' + mParams.changedItem);

        // just gets id --- item 1: id. value: __button2-__table0-0
        for (var property1 in mParams) {
            jQuery.sap.log.info(" item 1: " + property1  + '. value: ' + mParams[property1]);
        }
        var mParams = oEventArgs.getParameters();
        for (var property1 in mParams) {
            jQuery.sap.log.info(" item 2: " + property1  + '. value: ' + mParams[property1]);
        }

        // get item (row) data:
        alert('row data: ' + JSON.stringify(oEventArgs.getSource().getBindingContext(modelView).oModel.getProperty(oEventArgs.getSource().getBindingContext(modelView).sPath)));
        jQuery.sap.log.info('object element name/index: ' + oEventArgs.getSource().getBindingContext(modelView).sPath);  // path: /arrayName/0


        var row = oEventArgs.getSource(); // cyclic error message: alert(JSON.stringify(row));
        var context = row.getBindingContext("modelPath"); // cyclic error message:  alert(JSON.stringify(context));
        // get binding object (reference to an object of the original array)
        var item_obj = context.oModel.getProperty(context.sPath);
        //alert(JSON.stringify(item_obj));
        sap.m.MessageToast.show(item_obj.firstName + " was born in " + item_obj.born + ".")
    }

    /**
     * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
     * (NOT before the first rendering! onInit() is used for that one!).
     * @memberOf publicis_userchecker.check
     */
//	onBeforeRendering: function() {
//
//	},

    /**
     * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
     * This hook is the same one that SAPUI5 controls get after being rendered.
     * @memberOf publicis_userchecker.check
     */
//	onAfterRendering: function() {
//
//	},

    /**
     * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
     * @memberOf publicis_userchecker.check
     */
//	onExit: function() {
//
//	}

});