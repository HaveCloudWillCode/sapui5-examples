sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment"
], function (ManagedObject, Fragment) {
    "use strict";

    return ManagedObject.extend("havecloudwillcode.dividendTrakr.controller.OrganizationDialog", {
        constructor: function (oParentView) {
            // alert('oParentView: ' + oParentView);
            this._oParentView = oParentView;
        },

        onInit: function (oEvent) {
        },

        open: function (record) {
            //alert("OrganizationDialog::open. record: " + JSON.stringify( record));
            const oParentView = this._oParentView;
            const theView = oParentView.byId("idOrganizationDialog");

            function populateAndOpen(theView, record) {
                function clone(obj) {
                    return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj)
                }

                const temp = clone(record);  // i guess this is due to it being mutable
                temp.target = record;
                theView.setModel(new sap.ui.model.json.JSONModel(temp));
                theView.open()
            }

            // Create dialog lazily
            if (!theView) {
                //alert('create dialog');
                const oFragmentController = {

                    onCloseDialog: function () {
                        oParentView.byId("idOrganizationDialog").close();
                    },

                    onSaveDialog: function (oEvent) {
                        //alert("save pressed");
                        const oParentViewModel = oParentView.getModel('products');  // named model instantiated in component

                        console.log('oEvent.getSource(): ' + oEvent.getSource().getModel());  // save button event. assume below is way to get form data/model
                        const oDialogData = oEvent.getSource().getModel().getData();  // ?? works with un-named mode ?? gets form data
                         //alert('oDialogData: ' + JSON.stringify(oDialogData));   // shows new changed data

                        // create binding to data model
                        const target = oDialogData.target;
                        //console.log('target before: ' + JSON.stringify(target));

                        if (oDialogData.bNewRecord) {
                            //alert('new record. orig' + JSON.stringify(oParentViewModel.getData()));
                            oDialogData.bNewRecord = false;
                            oParentViewModel.getData().ProductCollection.push(oDialogData);      // add the JSON parent element here
                            //alert('push record.  ' + JSON.stringify(oDialogData));
                        } else {
                            //alert('update record');
                            Object.keys(oDialogData).forEach(function (key) {
                                console.log('key: '+ key);
                                target[key] = oDialogData[key];  // update json obj
                            });
                            //console.log('target after: ' + JSON.stringify(target));
                        }

                        //alert('oParentViewModel: ' + JSON.stringify(oParentViewModel.getData()));
                        oParentViewModel.setData(oParentViewModel.getData());  // this is the layout  -- view (i.e. same page)
                        oParentView.byId("idOrganizationDialog").close(); // close popup page
                    }
                };

                // load asynchronous XML fragment
                Fragment.load({
                    id: this._oParentView.getId(),
                    name: "havecloudwillcode.dividendTrakr.view.OrganizationDialog",
                    controller: oFragmentController
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    // oParentView.addDependent(oDialog);

                    populateAndOpen(oDialog, record)
                });
            } else {
                populateAndOpen(theView, record)
            }
        },
    });
});