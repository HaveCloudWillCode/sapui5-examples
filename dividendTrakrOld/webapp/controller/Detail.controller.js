sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("havecloudwillcode.dividendTrakr.controller.Detail", {
		onInit: function () {
		//	alert('s1');
			sap.ui.core.BusyIndicator.show();
			this.oOwnerComponent = this.getOwnerComponent();

			this.oRouter = this.oOwnerComponent.getRouter();
			this.oModel = this.oOwnerComponent.getModel();

		//	alert('s4 ' + this._onProductMatched);
			this.oRouter.getRoute("master").attachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detail").attachPatternMatched(this._onProductMatched, this);
		//	alert('s5');
			sap.ui.core.BusyIndicator.hide();
		},

		onObjectListItemPress: function (oEvent) {
			alert("onObjectListItemPress PRESS");
			var supplierPath = oEvent.getSource().getBindingContext("products").getPath(),
				supplier = supplierPath.split("/").slice(-1).pop(),
				oNextUIState = this.oOwnerComponent.getHelper().getNextUIState(2);  // 2 = new page single column

			this.oRouter.navTo("detailDetail", {objectId : objectId, layout: oNextUIState.layout });
		},

		// this updates the Detail view for Product Detail. last one on first time view
		_onProductMatched: function (oEvent) {
			// alert('_onProductMatched for _product: ' + oEvent.getParameter("arguments").product);
			//alert('_onProductMatched for all: ' + JSON.stringify(  oEvent.getParameter("arguments")) );

			this._product = oEvent.getParameter("arguments").product || this._product || "0";
			// path is the json row, model is what we create for view for the data object
			// must use same name as json data model as in component (products). just adding the row id.
			this.getView().bindElement({
				path: "/ProductCollection/" + this._product,
				model: "products"
			});

			// both work
			//alert('product detail: ' + JSON.stringify(this.getView().getModel('products').getData()));
			//alert('product detail: ' + JSON.stringify(this.getOwnerComponent().getModel('products').getData()));
			//alert(' this._product: ' +  this._product);  // json row index
			let row_from_json = this.getView().getModel('products').getData().ProductCollection[this._product];
			//alert('product detail: '				+ JSON.stringify(row_from_json));

// with this way, need to add a slash ???
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData(row_from_json);  // this can not be indexed "[{}]"
			this.getView().setModel(oModel,"productDetail");



		},

		onEditToggleButtonPress: function() {
			var oObjectPage = this.getView().byId("ObjectPageLayout"),
				bCurrentShowFooterState = oObjectPage.getShowFooter();

			oObjectPage.setShowFooter(!bCurrentShowFooterState);
		},

		handleFullScreen: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
			this.oRouter.navTo("detail", {layout: sNextLayout, product: this._product});
		},

		handleExitFullScreen: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
			this.oRouter.navTo("detail", {layout: sNextLayout, product: this._product});
		},

		handleClose: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
			this.oRouter.navTo("master", {layout: sNextLayout});
		},

		onExit: function () {
			this.oRouter.getRoute("master").detachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detail").detachPatternMatched(this._onProductMatched, this);
		},

		onAfterRendering: function() {
			sap.ui.core.BusyIndicator.hide();
		}
	});
}, true);
