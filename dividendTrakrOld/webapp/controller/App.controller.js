sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller"
], function (JSONModel, Controller) {
	"use strict";

	return Controller.extend("havecloudwillcode.dividendTrakr.controller.App", {
		onInit: function () {
			//alert('***** App.js loaded ******');

			this.oOwnerComponent = this.getOwnerComponent();
			this.oRouter = this.oOwnerComponent.getRouter();
			this.oRouter.attachRouteMatched(this.onRouteMatched, this);
			this.oRouter.attachBeforeRouteMatched(this.onBeforeRouteMatched, this);
		},

		onBeforeRouteMatched: function(oEvent) {

			//alert('***** App.js onBeforeRouteMatched ******');


			var oModel = this.oOwnerComponent.getModel(),
				sLayout = oEvent.getParameters().arguments.layout,
				oNextUIState;

			//alert('layout: ' + sLayout); // layout: TwoColumnsMidExpanded

			// If there is no layout parameter, query for the default level 0 layout (normally OneColumn)
			if (!sLayout) {
				//alert('no layout');

				oNextUIState = this.oOwnerComponent.getHelper().getNextUIState(0);
				sLayout = oNextUIState.layout;
			}

			//alert('set layout: '+ sLayout);
			oModel.setProperty("/layout", sLayout);
		},

		onRouteMatched: function (oEvent) {
			//alert('onRouteMatched')
			var sRouteName = oEvent.getParameter("name"),
				oArguments = oEvent.getParameter("arguments");

			var mParams = oEvent.getParameters();
			//alert( JSON.stringify(mParams)  );
			/*
			{"name":"detail","arguments":{"product":"94","layout":"TwoColumnsMidExpanded"},
			"config":{"routerClass":"sap.f.routing.Router","viewType":"XML","viewPath":"havecloudwillcode.dividendTrakr.view",
			"controlId":"flexibleColumnLayout","transition":"slide","bypassed":{},"async":true,"_async":true,
			"targetParent":"__xmlview0","pattern":"detail/{product}/{layout}","name":"detail","target":["master","detail"]}}
			 */

			this._updateUIElements();

			// Save the current route name
			this.currentRouteName = sRouteName;
			this.currentProduct = oArguments.product;
			this.currentSupplier = oArguments.supplier;
		},

		onStateChanged: function (oEvent) {
			var bIsNavigationArrow = oEvent.getParameter("isNavigationArrow"),
				sLayout = oEvent.getParameter("layout");

			this._updateUIElements();

			// Replace the URL with the new layout if a navigation arrow was used
			if (bIsNavigationArrow) {
				this.oRouter.navTo(this.currentRouteName, {layout: sLayout, product: this.currentProduct, supplier: this.currentSupplier}, true);
			}
		},

		// Update the close/fullscreen buttons visibility
		_updateUIElements: function () {
			var oModel = this.oOwnerComponent.getModel();
			var oUIState = this.oOwnerComponent.getHelper().getCurrentUIState();
			oModel.setData(oUIState);
		},

		onExit: function () {
			this.oRouter.detachRouteMatched(this.onRouteMatched, this);
			this.oRouter.detachBeforeRouteMatched(this.onBeforeRouteMatched, this);
		}
	});
}, true);
