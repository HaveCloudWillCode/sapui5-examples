sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    'sap/ui/model/Sorter',
    'sap/m/MessageBox'
], function (JSONModel, Controller, Filter, FilterOperator, Sorter, MessageBox) {
    "use strict";

    return Controller.extend("havecloudwillcode.dividendTrakr.controller.Master", {
        onInit: function () {


            this.oView = this.getView();
            this._bDescendingSort = false;
            this.oProductsTable = this.oView.byId("productsTable");  // get id from view (id=xxx)
            this.oRouter = this.getOwnerComponent().getRouter();


        },

        onSearch: function (oEvent) {
            var oTableSearchState = [],
                sQuery = oEvent.getParameter("query");

            if (sQuery && sQuery.length > 0) {
                oTableSearchState = [new Filter("Name", FilterOperator.Contains, sQuery)];
            }

            this.oProductsTable.getBinding("items").filter(oTableSearchState, "Application");  // gets <items> under this id=xxx
        },

        onAdd: function () {
            MessageBox.show("This functionality is not ready yet.", {
                icon: MessageBox.Icon.INFORMATION,
                title: "Aw, Snap!",
                actions: [MessageBox.Action.OK]
            });
        },

        onSort: function () {
            this._bDescendingSort = !this._bDescendingSort;
            var oBinding = this.oProductsTable.getBinding("items"),
                oSorter = new Sorter("Name", this._bDescendingSort);

            oBinding.sort(oSorter);
        },

// master row clicked
        onListItemPress: function (oEvent) {
            //alert('onListItemPress - master row clicked');

            var productPath = oEvent.getSource().getBindingContext("products").getPath(),
                product = productPath.split("/").slice(-1).pop(),
                oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(1);

            //alert('productPath: '+ productPath + '. product: '+ product + '. oNextUIState: ' + JSON.stringify(oNextUIState));
            // productPath: /ProductCollection/115
            // view table items="{	path: 'products>/ProductCollection',

            //alert('pass product to detail page: ' + product);

            this.oRouter.navTo("detail", {layout: oNextUIState.layout, product: product});
        },

// edit icon fired
        onDetailPress: function (oEvent) {
            //alert('onDetailPress');  // edit icon on item row
            //alert(JSON.stringify(oEvent.getSource().getBindingContext("products").getObject()));  // row data un-named model/context
            //alert( JSON.stringify(this.getView().getModel().getData()));  // this works - gets all json data

            /* this does not work from Item xml - only when fired from Table xml
            let oItem = oEvent.getParameter("listItem");
            let sPath = oItem.getBindingContext().getPath();  // un-named model/context
            alert(JSON.stringify( this.getView().getModel().getProperty(sPath) ));  // row data ***
            */

            // this is the json row data. pass row to openSDialog component function
            this.getOwnerComponent().openSDialog(oEvent.getSource().getBindingContext("products").getObject());
        },

// delete master row
        onDeleteOrganization: function (oEvent) {
            //alert('onDeleteOrganization pressed');
            //alert( JSON.stringify( oEvent.getSource().getBindingContext("products").getModel("xyz").getData()) ); // can be anything ???? if non-named model ??/

            const oCurrentOrganization = oEvent.getSource().getBindingContext("products").getObject(),
                oParentViewModel = oEvent.getSource().getBindingContext("products").getModel("oModel"),// this.oModel,  ?????
                oParentViewData = oParentViewModel.getData();

            // alert('oCurrentOrganization: '+ JSON.stringify( oCurrentOrganization) );  // row data
            let productPath = oEvent.getSource().getBindingContext("products").getPath(); // productPath: /ProductCollection/94
            let productId = productPath.split("/").slice(-1).pop()
            // alert('productId: ' + productId);

            // 1) remove json element    --- a bit slower - like by a second
            //delete oParentViewData.ProductCollection[productId];
            //oParentViewModel.setData(oParentViewData); // update model with new json file sans this row

            // 2) remove json element
            oParentViewData.ProductCollection = oParentViewData.ProductCollection.filter(row => row !== oCurrentOrganization);
            oParentViewModel.setData(oParentViewData);  // update model with new json file sans this row
        },

        openOrganizationDialog: function () {
            //alert('openOrganizationDialog. open dialog');

            /* have no idea what this is for. freeze makes obj immutable. this does not work as is but i assume it is meant to set default values.
            const template = Object.freeze({
                fullName: "?",
                languageTag: "language",
                coc: "coc",
                birthDay: "2019",
                vatNumber: "0123456",
                remarks: "Remarks",
                bNewRecord: true
            });template
            */

            this.getOwnerComponent().openSDialog({bNewRecord: true});  // parm1 is record
        },

        onAfterRendering: function() {
           // sap.ui.core.BusyIndicator.hide();
        }
    });
}, true);
