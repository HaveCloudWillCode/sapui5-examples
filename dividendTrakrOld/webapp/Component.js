sap.ui.define([
	'jquery.sap.global',
	'sap/ui/core/UIComponent',
	'sap/ui/model/json/JSONModel',
	'sap/f/FlexibleColumnLayoutSemanticHelper',
	"./controller/OrganizationDialog",
	"sap/ui/core/mvc/View"

], function(jQuery, UIComponent, JSONModel, FlexibleColumnLayoutSemanticHelper, OrganizationDialog, View ) {
	'use strict';

	return UIComponent.extend('havecloudwillcode.dividendTrakr.Component', {

		metadata: {
			manifest: 'json'
		},

		init: function () {
			console.log("Component::init");

			sap.ui.core.BusyIndicator.show();

			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set dialog. Note: will not work unless UIComponent is run first.
			this._organizationDialog = new OrganizationDialog(this.getRootControl());


			var oModel,
				oProductsModel;

			oModel = new JSONModel();
			this.setModel(oModel);

			// set products demo model on this sample
			//oProductsModel = new JSONModel(jQuery.sap.getModulePath('sap.ui.demo.mock', '/products.json'));
			//oProductsModel.setSizeLimit(1000);
			//this.setModel(oProductsModel, 'products');

			oProductsModel = new JSONModel(sap.ui.require.toUrl("havecloudwillcode/dividendTrakr/mock") + "/products.json");
			this.setModel(oProductsModel, 'products');


			oProductsModel.attachRequestCompleted(function () {
				//  alert('attachRequestCompleted. json obj: ' + JSON.stringify(oProductsModel.getData()));
			});

			this.getRouter().initialize();
		},
// auto runs  -- this is deprecated but changes the layout from single page to 2 column layout on first view
// need this to add the App.js and view functions
		createContent: function () {
			//console.log('Component::createContent');
		    //	App View
			 return sap.ui.xmlview({
				viewName: "havecloudwillcode.dividendTrakr.view.App"
			});

		},

// auto runs  -- sets default layout to 2 columns. flexibleColumnLayout is id from App.view
		getHelper: function () {
			 console.log("Component::getHelper");
			var oFCL = this.getRootControl().byId('flexibleColumnLayout'),
				oSettings = {
					defaultTwoColumnLayoutType: sap.f.LayoutType.TwoColumnsMidExpanded,
					defaultThreeColumnLayoutType: sap.f.LayoutType.ThreeColumnsMidExpanded,
					initialColumnsCount: 2,
					maxColumnsCount: 2
				};

			return FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings);
		},

		openSDialog: function (record) {
		    //alert("openSDialog"); alert( JSON.stringify( record) );
			this._organizationDialog.open(record); // calls js file instantiated in component.js
		}
	});
});