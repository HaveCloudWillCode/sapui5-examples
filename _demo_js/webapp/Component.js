

sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "Misc/model/models","sap/ui/model/json/JSONModel",
    "CommonJS/LoggedOnUser",
    "CommonJS/Custom"

], function(UIComponent, Device, models, JSONModel, LoggedOnUserJS) {
    "use strict";

    return UIComponent.extend("Misc.Component", {

        metadata: {
            manifest: "json"
        },

        /**
         * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
         * @public
         * @override
         */
        init: function() {
            alert('component started');
            // call the base component's init function
            UIComponent.prototype.init.apply(this, arguments);

            // set the device model
            //this.setModel(models.createDeviceModel(), "device");

            //this.getRouter().initialize();

            //<!-------- Component and Router apply init ----------->

            console.log("Component Initialize");

            // Call the init function of the parent
         //   console.log("Component - before prototype.init.apply(this, arguments)");
        //    UIComponent.prototype.init.apply(this, arguments);               console.log("Component - after prototype.init.apply(this, arguments)");

            // Initialize Router
            console.log("Component - Router Initialize");
            this.getRouter().initialize();

            //<!-------- Using the JS functions from libraryApp ----------->

            this.getUser = new LoggedOnUserJS();  // Create new instance. Like new sap.m.Button
            // And now we can call the functons inside the JS file as we please :)
            console.log(this.getUser.getLoggedOnUser());
            alert( 'from Component. hello: ' + this.getUser.getLoggedOnUser());

      jQuery.sap.log.info("**this is deprecated**");
      sap.base.Log("asdfasdf");


            // import custom.js file
            var s = document.createElement("script");
            s.type = "text/javascript";
            s.src = "../../_js/Custom.js";
            $("head").append(s);


            let msg =  message('asdfasdf');
            alert('component message: ' +  msg);


        }
    });
});