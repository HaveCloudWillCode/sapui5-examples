sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function (Controller, MessageToast) {
	"use strict";

	return Controller.extend("sap.ui.demo.wt.controller.HelloPanel", {

		onShowHello : function () {
			alert('HelloPanel::onShowHello');
			// read msg from i18n model
			var oBundle = this.getView().getModel("i18n").getResourceBundle();
			var sRecipient = this.getView().getModel().getProperty("/recipient/name");
			var sMsg = oBundle.getText("helloMsg", [sRecipient]);

			// show message
			alert('MessageToast');
			MessageToast.show(sMsg);
		},

		// this is listener to open fragment popup. HelloPanel.view.xml has press elements to fire these functions
		onOpenDialog : function () {
			alert('HelloPanel::onOpenDialog');
			this.getOwnerComponent().helloDialog.open(this.getView());
		}
	});

});
