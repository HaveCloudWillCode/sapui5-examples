sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";


	alert('app.controller.js start. this runs before component');

	return Controller.extend("sap.ui.demo.wt.controller.App", {

		onOpenDialog : function () {
			this.getOwnerComponent().helloDialog.open(this.getView());
		}
	});

});
