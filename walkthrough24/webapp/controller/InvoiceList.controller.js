sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",

	"sap/ui/demo/wt/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("sap.ui.demo.wt.controller.InvoiceList", {

		formatter: formatter,

		onInit: function () {
			// json data is loaded from component.js using resource local in manifest - under invoiceLocal
			var oViewModel = new JSONModel({
				currency: "USD"
			});
			this.getView().setModel(oViewModel, "view");
		},

		onFilterInvoices : function (oEvent) {
             alert('onFilterInvoices');
			// build filter array
			var aFilter = [];
			var sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("ProductName", FilterOperator.Contains, sQuery));  // filter on json element name
			}

			// filter binding
			var oList = this.getView().byId("invoiceList");  // id in view
			var oBinding = oList.getBinding("items");  // var for json data
			oBinding.filter(aFilter);
		}
	});

});
