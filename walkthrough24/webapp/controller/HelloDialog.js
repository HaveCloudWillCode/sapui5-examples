sap.ui.define([
	"sap/ui/base/Object"
], function (Object) {
	"use strict";

	return Object.extend("sap.ui.demo.wt.controller.HelloDialog", {

		_getDialog : function () {
			// create dialog lazily  -- this adds fragment
			if (!this._oDialog) {
				// create dialog via fragment factory (? .fragment vs .view ?)  -- fragment is popup window
				this._oDialog = sap.ui.xmlfragment("sap.ui.demo.wt.view.HelloDialog", this);
			}
			return this._oDialog;  // create this fragment object (used below in close and open)
		},

		open : function (oView) {
			alert('HelloDialog.js open  ');
			var oDialog = this._getDialog();  // add fragment for window

			// connect dialog to view (models, lifecycle)
			oView.addDependent(oDialog);

			// open dialog
			oDialog.open();
		},

		onCloseDialog : function () {
			alert('HelloDialog.js   onCloseDialog  ');
			this._getDialog().close();  // close this fragment object
		}
	});

});
