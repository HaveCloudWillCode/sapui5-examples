sap.ui.define(['sap/ui/core/UIComponent'],
	function(UIComponent) {
	"use strict";

	var Component = UIComponent.extend("sap.m.sample.Carousel.Component", {


// inline version of a manifest file   === only one mandatory entry is 'rootView' => points to main xml view (App.name from index and name of main view "Carousel"
		metadata : {
			rootView : {
				"viewName": "sap.m.sample.Carousel.Carousel",
				"type": "XML",
				"async": true
			},
			x_dependencies : {
				libs : [
					"sap.m",
					"sap.ui.layout"
				]
			},
			x_config : {
				sample : {
					stretch : true,
					files : [
						"Carousel.view.xml",
						"Carousel.controller.js"
					]
				}
			}
		}
	});

	return Component;

});
