sap.ui.define(['jquery.sap.global', 'sap/ui/core/mvc/Controller', 'sap/ui/model/json/JSONModel'],
    function (jQuery, Controller, JSONModel) {
        "use strict";

        var CarouselController = Controller.extend("sap.m.sample.Carousel.Carousel", {

            onInit: function (evt) {

                // load json data of products
                //var oModel = new JSONModel(sap.ui.require.toUrl("sap/ui/demo/mock") + "/products.json");
                //this.getView().setModel(oModel);

                // load json data if images
                //var oImgModel = new JSONModel( sap.ui.require.toUrl("sap/ui/demo/mock") + "/img.json");  // did not work
                //http://ui5.devlocal.com/examples/Carousel/mockdata/img.json
                var oImgModel = this.getJsonModel("sap/ui/demo/mock", '/img.json');

                // this does not work - diff thread
                //var data = JSON.stringify(oImgModel.getData());
                //console.log('json 1: ' + JSON.stringify(data));
                //var data_json = JSON.parse(data);
                //console.log('json 2: ' + JSON.stringify(data_json));

                this.getView().setModel(oImgModel, "img");
            },

            onAfterRendering: function () {
                console.log('after render');
                this.changeCarouselImage();
            },

            //
            getJsonModel: function (module_path, json_loc) {
                // General note: will get this error if you comment out a line with / vs // ( [Show/hide message details.] SyntaxError: unterminated regular expression literal)
                var oModel = new JSONModel(jQuery.sap.getModulePath(module_path, json_loc));                /* same as doing ajax call */

                // This is used for testing only //  similar to ajax.complete function. can only see data once ajax thread is done.
                oModel.attachRequestCompleted(function () {
                    var json_string = JSON.stringify(oModel.getData());
                    var json_obj = JSON.parse(json_string);
                    console.log('json object: ' + JSON.stringify(json_obj));
                    console.log("row 0: " + JSON.stringify(json_obj.products[0].name) + '  =   ' + JSON.stringify(json_obj.products[0].url));

                    // test it -- may not work due to thread timing
                    //var testModel = this.getView().getModel("img"); // can not do this
                    //var testModel = this.getOwnerComponent().getModel("img");  // can not do here since this ctrl is parent
                    //var json_string = JSON.stringify(testModel.getData());
                    //var json_obj = JSON.parse(json_string);
                    //alert('test get model: ' + JSON.stringify(json_obj));
                });
                return oModel;
            }


          ,  changeCarouselImage: function (oControlEvent) {
                console.log('page changed');

                var idCarousel = this.byId("idCarousel");  //idCarousel.next();
                setTimeout(function() { idCarousel.next() }, 2000);

                var sPageId = oControlEvent.getParameters().id;  console.log ("page id: " + sPageId);  // this works great
                //alert(this.byId("idCarousel") + ' & ' + idCarousel);  // Element sap.m.Carousel#__xmlview0--idCarousel
                //setTimeout(function() { idCarousel.next() }, 2000);  // this is the only way i can get it to work

                //this.byId("idCarousel").next();
                //alert('test ' + this.byId("idCarousel").value());
                //setTimeout(function() { this.byId("idCarousel").next() }, 2500);
                //  sap.ui.getCore().byId("idCarousel").next();  // does not work
                // setTimeout("changeCarouselImage()", 500000);
                //this.byId("idCarousel").next().delay(10000);
                //jQuery.sap.log.info("sap.m.Carousel: loading page " + sPageId  );  // ?? did not work
                //sleep(2000);  // does not work well
               // this.byId("idCarousel").next();  // this works too.  but .delay(5000)  does not work
                //setTimeout("changeCarouselImage()", 2000);  // core js function  --- this would not work ???
            }


//
        });

        return CarouselController;

    });



