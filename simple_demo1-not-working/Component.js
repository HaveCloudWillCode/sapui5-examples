sap.ui.define(['sap/ui/core/UIComponent'],
	function(UIComponent) {
	"use strict";

	var Component = UIComponent.extend("ui5.demo1.Component", {

		/* rootview fires first view - Page.view.xml - which fires Page.controller*/
		metadata : {
			rootView : {
				"viewName": "ui5.demo1.Page",
				"type": "XML",
				"async": true
			},
			x_dependencies : {
				libs : [
					"sap.m",
					"sap.ui.layout"
				]
			},
			x_config : {
				sample : {
					stretch : true,
					files : [
						"Page.view.xml",
						"Page.controller.js",
						"Change.fragment.xml",
						"Display.fragment.xml"
					]
				}
			}
		}
	});

	return Component;

});
