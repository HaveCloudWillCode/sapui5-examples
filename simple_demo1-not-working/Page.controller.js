sap.ui.define([
		'jquery.sap.global',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel'
	], function(jQuery, Fragment, Controller, JSONModel) {
	"use strict";

	var PageController = Controller.extend("ui5.demo1.Page", {

		onInit: function (oEvent) {

			// set explored app's demo model on this sample
		//	var oModel = new JSONModel(jQuery.sap.getModulePath("ui5.demo1.mock", "/webdocs.json"));
	//		this.getView().setModel(oModel);
//			alert(jQuery.sap.getModulePath("sap.ui.demo.mock", "../mockdata/webdocs.json"));

			//var jModel = new JSONModel('../mockdata/webdocs.json');
			var jModel = new JSONModel("http://ui5.devlocal.com/examples/simple_demo1/mockdata/webdocs.json");
			//this.setModel(jModel, "invoices");
			this.getView().setModel(jModel);

			jModel.attachRequestCompleted(function () {
				var data = JSON.stringify(jModel.getData());
				var data_json = JSON.parse(data);
				 alert('attachRequestCompleted json obj: ' + JSON.stringify(data_json));
			});

			// pre-populate page fields - also creates bisync binding
			this.getView().bindElement("/Product_catalog/0"); // bind first json row to view

			// Set the initial form to be the display one. Streams back to div id "content" set in index.html
			this._showFormFragment("Display");  // replaces any content in Page.view
			this._showFormFragment("Change");
		},

		xxonExit : function () {
			for (var sPropertyName in this._formFragments) {
				if (!this._formFragments.hasOwnProperty(sPropertyName) || this._formFragments[sPropertyName] == null) {
					return;
				}

				this._formFragments[sPropertyName].destroy();
				this._formFragments[sPropertyName] = null;
			}
		},

		handleEditPress : function () {

			//Clone the data
			this._oSupplier = jQuery.extend({}, this.getView().getModel().getData().Product_catalog[0]);
			this._toggleButtonsAndView(true);

		},

		handleCancelPress : function () {

			//Restore the data
			var oModel = this.getView().getModel();
			var oData = oModel.getData();

			oData.Product_catalog[0] = this._oSupplier;

			oModel.setData(oData);
			this._toggleButtonsAndView(false);

		},

		handleSavePress : function () {

			this._toggleButtonsAndView(false);

		},

		_formFragments: {},

		_toggleButtonsAndView : function (bEdit) {
			var oView = this.getView();

			// Show the appropriate action buttons
			oView.byId("edit").setVisible(!bEdit);
			oView.byId("save").setVisible(bEdit);
			oView.byId("cancel").setVisible(bEdit);

			// Set the right form type
			this._showFormFragment(bEdit ? "Change" : "Display");
		},

		/* cache fragment object */
		_getFormFragment: function (sFragmentName) {

			alert('get frag ' + sFragmentName);

			var oFormFragment = this._formFragments[sFragmentName];

			if (oFormFragment) {
				return oFormFragment;
			}

			// instead of this, below uses the fragment name - both frags will then get json data
			oFormFragment = sap.ui.xmlfragment(this.getView().getId(),  "../" + sFragmentName);
			//
			//oDialog = sap.ui.xmlfragment(sDialogFragmentName, this);
			// create fragment view object (dialog). must use this to have this model available in fragment view

			this._formFragments[sFragmentName] = oFormFragment;
			return this._formFragments[sFragmentName];
		},

		_showFormFragment : function (sFragmentName) {
			alert('show frag');
			var oPage = this.byId("page");

			oPage.removeAllContent();  // clear out page's content
			oPage.insertContent(this._getFormFragment(sFragmentName));
		}

	});


	return PageController;

});
