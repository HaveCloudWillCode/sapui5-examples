jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/pages/App",
	"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/pages/Browser",
	"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/pages/Master",
	"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/pages/Detail",
	"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.sap.offlinecruddemo.OfflineCRUDDemo.view."
	});

	sap.ui.require([
		"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/NavigationJourneyPhone",
		"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/NotFoundJourneyPhone",
		"com/sap/offlinecruddemo/OfflineCRUDDemo/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});