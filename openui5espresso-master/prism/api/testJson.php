
<?php
/**
 * Created by PhpStorm.
 * User: bsteiner
 * Date: 9/21/18
 * Time: 9:09 AM
 */


// added this to fix cross domain blocking errors
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");



$data = '{
	"TileCollage" : [
    {
        "icon" : "line-chart",
      "number": "5800",
      "numberUnit": "US$",
      "title" : "Q1 Revenue $\'000",
      "info" : "Loyalty Campaign",
      "infoState" : "Warning"
    },
    {
        "icon" : "bar-chart",
      "number" : "5108",
      "numberUnit" : "# Cust",
      "title" : "Q1 Number of New Customers",
      "info" : "Loyalty Campaign",
      "infoState" : "Warning"
    } 
  ]
 
}';

echo $data;
