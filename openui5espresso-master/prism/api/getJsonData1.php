<?php
/**
 * Created by PhpStorm.
 * User: bsteiner
 * Date: 9/21/18
 * Time: 9:09 AM
 */

$data = '{
	"TileCollage" : [
    {
        "icon" : "line-chart",
      "number": "1800",
      "numberUnit": "US$",
      "title" : "Q1 Revenue $\'000",
      "info" : "Loyalty Campaign",
      "infoState" : "Warning"
    },
    {
        "icon" : "bar-chart",
      "number" : "1108",
      "numberUnit" : "# Cust",
      "title" : "Q1 Number of New Customers",
      "info" : "Loyalty Campaign",
      "infoState" : "Warning"
    } 
  ]
 
}';

echo $data;