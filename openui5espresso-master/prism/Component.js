sap.ui.define([
   "sap/ui/core/UIComponent",
   "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel"
], function (UIComponent, JSONModel,ResourceModel) {
   "use strict";
   return UIComponent.extend("sap.ui.demo.Component", {
        metadata : {
		//rootView: "sap.ui.demo.view.App"
        manifest: "json"
	},
      init : function () {
         //   alert("component init");
         // call the init function of the parent
         UIComponent.prototype.init.apply(this, arguments);
         
         // set data model -- i moved this to App controller since not a global data model
         //   var oModel = new JSONModel("model/data.json");
         //   this.setModel(oModel);  // json data has name of model data: "TileCollage". Used in App.view.xml to build tiles

          // set i18n model (properties file)
         var i18nModel = new ResourceModel({
            bundleName : "sap.ui.demo.i18n.i18n"
         });
         this.setModel(i18nModel, "i18n");  // sets prefix name for properties. i.e. {i18n>subHeaderBarContentMidText}

      }
   });
});

