sap.ui.define([
		'jquery.sap.global',
		'sap/ui/core/mvc/Controller',
        'sap/m/MessageToast'
	], function(jQuery, Controller, MessageToast) {
	"use strict";
 
	var Controller = Controller.extend("sap.ui.demo.controller.App", {
 
		onInit: function () {
          //  alert("App.controller.js :: onInit. Called from Component.js instantiation");

			// http://ui5.devlocal.com/prism/model/data.json             //var url = 'http://ui5.devlocal.com/prism/model/data.json';
            var url = 'http://ui5.devlocal.com/examples/openui5espresso-master/prism/api/getJsonData.php';
            var oBusy = new sap.m.BusyDialog();
            var oModel = new sap.ui.model.json.JSONModel();
            // my js api function
            getJsonData(oModel, url, oBusy);  // my js ajax function

            // bind model to view
            this.getView().setModel(oModel);

        },
        // Listener. tied in App.view.xml to person icon
        onPressMsg: function(oEvent) {
			var msg = 'Updating page from cross domain server';
			MessageToast.show(msg);

			// update model data on view
            // same domain
            // var url = 'http://ui5.devlocal.com/prism/api/getJsonData1.php';
            // cross domain
            var url ='http://www.owi.com/testJson.php';
            var oModel = new sap.ui.model.json.JSONModel();
            var oBusy = new sap.m.BusyDialog();

            getJsonData(oModel, url, oBusy);  // my ajax function
            this.getView().setModel(oModel);  // bind model to view

		}
	});
 
	return Controller;
 
});
