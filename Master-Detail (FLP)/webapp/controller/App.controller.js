sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel"
], function (BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("sap.ui.demo.masterdetail.controller.App", {

		onInit : function () {
			//console.log('app step 1.');
			var oViewModel,
				fnSetAppNotBusy,
				iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();

			// $metadata error - changed below to false until i figure that out
			oViewModel = new JSONModel({
				busy : false,
				delay : 0,
				layout : "OneColumn",
				previousLayout : "",
				actionButtonsInfo : {
					midColumn : {
						fullScreen : false
					}
				}
			});
			//console.log('app step 2.');
			this.setModel(oViewModel, "appView");


			fnSetAppNotBusy = function() {
				oViewModel.setProperty("/busy", false);
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			};

			//console.log('app step 3.');
			// since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
			this.getOwnerComponent().getModel().metadataLoaded().then(fnSetAppNotBusy);  // turn off busy dots when data is loaded
			this.getOwnerComponent().getModel().attachMetadataFailed(fnSetAppNotBusy);  // turn off busy dots if data load failed

			//console.log('app step 4.');
			// apply content density mode to root view
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());

			//console.log('app done.');
		}

	});
});