sap.ui.require([
	"sap/m/Shell",
	"sap/m/App",
	"sap/m/Page",
	"sap/ui/core/ComponentContainer"
], function(
	Shell, App, Page, ComponentContainer) {
	"use strict";

	sap.ui.getCore().attachInit(function() {
		new Shell ({
			app : new App ({
				pages : [
					new Page({
						title : "Table - View Settings & Context Menu",
						enableScrolling : true,
						content : [
							new ComponentContainer({
								name : "sap.m.sample.TableViewSettingsDialog",
								settings : {
									id : "sap.m.sample.TableViewSettingsDialog"
								}
							})
						]
					})
				]
			})
		}).placeAt("content");
	});
});