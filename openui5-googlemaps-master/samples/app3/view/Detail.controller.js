//sap.ui.controller("testapp.view.Detail", {
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageBox',
    'testapp/util/Formatter'
], function (Controller, MessageBox, formatter) {
    "use strict";

    return Controller.extend("testapp.controller.Detail", {
        formatter: formatter,  // need this to bind formatter to view object

    onInit: function(oEvent) {
        sap.ui.getCore().getEventBus().subscribe("listSelected", this.onListSelected, this);
        this.oPage = this.byId("page1");
        this.oMap = this.byId("map1");
       // this.oChart = this.byId("chart1");
       // this.showPolyline = false;
        //this.showPolygon = false;
    },

    onAfterRendering: function () {
        //alert('onAfterRendering');
        //alert('onAfterRendering' + JSON.stringify(oView.getModel().getData()));
    },

    onMapReady: function(oEvent) {
        // alert('onMapReady. this.selectedLocation: ' +  JSON.stringify( this.selectedLocation));


        //alert(this.getView().createId("map1"));

        if (this.selectedLocation === undefined) {

            //var aBeaches = this.getView().getModel().getData().beaches;
            //alert('detail get data' + JSON.stringify(this.getView().getModel().getData()));

            var aBeaches = this.getView().getModel().getData();
            aBeaches.icon = this._getImage();

            //console.log('aBeaches 1: ' + JSON.stringify(aBeaches));

            //var test = JSON.parse(aBeaches);
            //console.log('aBeaches 2: ' + JSON.stringify(test));

            // set flag image -- returns from in here
            /*
            var nBeaches = aBeaches.map(function(oBeach){
                oBeach.icon = this._getImage();

                alert('icon ' + oBeach.icon);
                return oBeach;
            }.bind(this));

             */

// this code never hits with above uncommented//

            this._styleMap();

            this.selectedLocation = aBeaches.beaches[aBeaches.beaches.length - 1];

            //alert('  length: ' + aBeaches.length);
            //alert('onMapReady. aBeaches: ' + JSON.stringify( aBeaches)  );
            //alert('onMapReady. selectedLocation: ' + JSON.stringify( this.selectedLocation)  );


            //this.getView().getModel().setData({beaches: nBeaches});  // set view model with data (can add filter logic here)
            this.getView().getModel().setData(aBeaches);  // already has beaches parent  - nBeaches does not - just {} data


            //alert('data icon:  ' + JSON.stringify( this.getView().getModel().getData().icon ));

            //this.setupPolylines();
            //this.setupPolygons();
        }


        //alert('step omr 1');
        this.setLocation();
    },

    setLocation: function(bPublish) {
        //alert('setLocation');

        this.markerWindowOpen(this.selectedLocation);
    //    this.setChartData(this.selectedLocation.columns);

        //alert( JSON.stringify( this.selectedLocation) );

        // updates master list row selection - class: sapMLIBSelected
        console.log('setLocation: ' + JSON.stringify( this.selectedLocation) );
        sap.ui.getCore().getEventBus().publish("placeSelected", {
            location: this.selectedLocation
        });
    },

    markerWindowOpen: function(oData) {
        console.log( 'markerWindowOpen oData: ' + JSON.stringify(oData));


// find map on page and open
        if(oData !== undefined){
            var that = this;
            this.oMap.getMarkers().forEach(function (oMarker) {
                if (oMarker.getLat() === oData.lat && oMarker.getLng() === oData.lng) {

                    //alert(oMarker.getInfo());

                   // that.oPage.setTitle(oMarker.getInfo());
                    that.oPage.setTitle(oData.store_name);

                    // open pin window on map
                    oMarker.infoWindowOpen();
                } else {
                    oMarker.infoWindowClose();
                }
            });
        }
    },

    /*
    setChartData: function(aData) {
        this.oChart.setVisible(true);
        this.oChart.setColumns(aData);
    },

     */

    onListSelected: function(sChannelId, sEventId, oData) {
        alert('onListSelected');

        // set location variable on row clicked
        this.selectedLocation = oData.context.getObject();

        this.setLocation();
    },

    onMarkerClick: function(oEvent) {
        this.selectedLocation = oEvent.getParameter('context').getObject();

        alert('onMarkerClick. this.selectedLocation: ' + JSON.stringify( this.selectedLocation));
        //alert(this.getView().createId("map1"));

        this.setLocation();
    },

    getPaths: function() {
        alert('getPaths');
        var aPaths = [];

        //alert('get data');

        this.getView().getModel().getData().beaches.forEach(function(obj) {
            aPaths.push({
                lat: obj.lat,
                lng: obj.lng
            });
        });

        return aPaths;
    },
    setupPolylines: function() {
        if (this.oMap.getPolylines().length > 0) {
            return;
        }

        var lineSymbol = {
            path: 'M 0,-1 0,1',
            strokeOpacity: 0.5,
            scale: 4
        };

        this.oMap.addPolyline(new openui5.googlemaps.Polyline({
            path: this.getPaths(),
            strokeColor: "#0000FF",
            strokeOpacity: 0.5,
            strokeWeight: 0.2,
            visible: this.showPolyline,
            icons: [{
                icon: lineSymbol,
                offset: '0',
                repeat: '20px'
            }]
        }));

    },

    setupPolygons: function() {
        if (this.oMap.getPolygons().length > 0) {
            return;
        }

        var center = {
            lat: this.oMap.getLat(),
            lng: this.oMap.getLng()
        };
        this.oMap.addPolygon(new openui5.googlemaps.Polygon({
            paths: jQuery.merge([center], this.getPaths()),
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            visible: this.showPolygon
        }));
    },

    onShowPolyline: function(oEvent) {
        this.showPolyline = !this.showPolyline;
        that = this;
        if (this.oMap.getPolylines()) {
            this.oMap.getPolylines().forEach(function(oControl) {
                oControl.setVisible(that.showPolyline);
            });
        }
    },

    onShowPolygon: function(oEvent) {
        this.showPolygon = !this.showPolygon;
        that = this;
        if (this.oMap.getPolygons()) {
            this.oMap.getPolygons().forEach(function(oControl) {
                oControl.setVisible(that.showPolygon);
            });
        }
    },

    // pin image on google map
    _getImage: function() {
        // url: "./images/beachflag.png",
        return {
            url: "./images/blue-dot.png",
          //  size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32)
        };
    },

    _styleMap: function(){
        //style the map
        var styledMapType = new google.maps.StyledMapType(this._aMapStyle);
        this.oMap.map.mapTypes.set('styled_map', styledMapType);
        this.oMap.map.setMapTypeId('styled_map');
    },

    _aMapStyle:[
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "on"
      }
    ]
  }
]
});
}, true);