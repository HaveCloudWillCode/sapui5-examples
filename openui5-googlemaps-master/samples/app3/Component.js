sap.ui.define([
    "sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel"
], function(UIComponent,JSONModel) {
    "use strict";
    return UIComponent.extend("testapp.Component", {
        init: function() {
            //alert('app2 comp init');

            // W3C googlemaps key test
            // window.GMAPS_API_KEY = 'AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM';

            //load googlemaps library
            sap.ui.getCore().loadLibrary("openui5.googlemaps", "../../dist/openui5/googlemaps/");

            // openui5.googlemaps.ScriptsUtil.setApiKey('AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM');

            //register controls library
            jQuery.sap.registerResourcePath("controls", "../controls");

            UIComponent.prototype.init.apply(this, arguments);
        },

        createContent: function() {
            // create root view
            var oView = sap.ui.view({
                id: "idViewRoot",
                viewName: "testapp.view.Root",
                type: "XML",
                viewData: {
                    component: this
                }
            });

            // set data model on root view -- DATA --
            //oView.setModel(new JSONModel("model/mock.json"));  // un-named global data model
            //let locationsModel = new JSONModel("model/mock.beaches.json");
            let locationsModel = new JSONModel("model/mock.json");

            //alert('get data');
            //let locationsModel = new JSONModel("model/store_locations.json");
            oView.setModel(locationsModel);

            locationsModel.attachRequestCompleted(function () {
                  //alert('attachRequestCompleted. oObjects json obj: ' + JSON.stringify(locationsModel.getData()));

                  //alert(JSON.stringify(oView.getModel().getData()));
            });

            // set i18n model
            var i18nModel = new sap.ui.model.resource.ResourceModel({
                bundleUrl: "i18n/messageBundle.properties"
            });
            oView.setModel(i18nModel, "i18n");

            // done
            return oView;
        }
    });
});