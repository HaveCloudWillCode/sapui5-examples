sap.ui.define([
	"sap/ui/core/mvc/XMLView"
], function (XMLView) {
	"use strict";

	XMLView.create({
		viewName: "sap.ui.xmlview"
	}).then(function (xoView) {

		alert('view created');
		xoView.placeAt("content");
	});
});
