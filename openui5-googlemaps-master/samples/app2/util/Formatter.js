sap.ui.define(['jquery.sap.global'],
    function(jQuery) {
        "use strict";

        var Formatter = {};

        /*
        https://maps.google.com/maps?saddr=current+location&daddr=O%27Reilly%20Auto%20Parts%20797%20Civic%20Center%20Drive,%20Niles,%20IL%2060714-3206

        <a target="_blank" href="https://maps.google.com/maps?saddr=current+location&amp;daddr=O'Reilly%20Auto%20Parts%20797%20Civic%20Center%20Drive,%20Niles,%20IL%2060714-3206">Get Directions</a>

       https://www.google.com/maps?saddr=current+location&daddr=O%27Reilly+Auto+Parts+797+Civic+Center+Drive,+Niles,+IL+60714-3206
         */


        Formatter.info = function(name, address, city, state, zip, phone) {

           console.log('<a target="_blank" href="https://www.google.com/maps?saddr=current+location&amp;daddr=' + address + ',' + city + ',' + state  + ',' + zip + '">Get Directions</a>');

            var directions = encodeURI('https://www.google.com/maps?saddr=current+location&daddr=' + name + ' ' + address + ',+' + city + ',+' + state  + ',+' + zip);
            console.log('directions:' + directions);

            var str = jQuery('<div class="marker-info-win">' +
                '<div class="marker-inner-win"><span class="info-content">' +
                '<h1 class="marker-heading"> ' + name + '</h1>'
                + address + '<br>' + city + ', ' +state + '  ' + zip
                + "</br> phone: " + phone
                + "</br>   "
                + '<a target="_blank" href="' + directions  + '">Get Directions</a>'
                + '</span></div></div>');
            return str[0].outerHTML;
        };

        Formatter.desc = function(oV1, oV2, oV3, oV4) {
            //alert(oV1 + oV2 + oV3 + oV4);


          return  oV1 + ", " + oV2 +  " " + oV3    ;



        };

        Formatter.formatMapUrl = function(sStreet, sZip, sCity, sCountry) {
            return "https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=500x300&markers="
                + jQuery.sap.encodeURL(sStreet + ", " + sZip +  " " + sCity + ", " + sCountry);
        };



        return Formatter;
    }, true);