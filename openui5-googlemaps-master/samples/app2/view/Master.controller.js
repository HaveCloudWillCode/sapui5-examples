//  sap.ui.controller("testapp.view.Master", {
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageBox',
    'testapp/util/Formatter',
    "sap/ui/model/json/JSONModel"
], function (Controller, MessageBox, formatter, JSONModel) {
    "use strict";

    return Controller.extend("testapp.controller.Master", {
        formatter: formatter,  // need this to bind formatter to view object

        onInit: function () {
            this.list = this.byId('places'); // view id for List

            // bind publish event to function onPlaceSelected
            sap.ui.getCore().getEventBus().subscribe("placeSelected", this.onPlaceSelected, this);



           this.getJson(60068);



        },

        getJson: function (zip) {
            // lng lat data must not be a string - must be numeric (float)
            let jData=null;
            let locationsModel =  new JSONModel();
            let url = "http://peak-alt-service.local/slv1/get/storeLocations/"
                 + "42.0085328/-87.84515270000003/25/117/20";
            console.log ('get data: ' + url);
            jQuery.ajax({
                url: url,
                dataType: "json",
                success: function(data) {
                    console.log(JSON.stringify(data));
                    alert('ajax resposne: ' + JSON.stringify( data.results ) );
                    jData = data;
                },
                async: false
            });

            console.log('jData: ' +  JSON.stringify( jData.results ) );
            locationsModel.setProperty("/locations", jData.results);
            alert('get view model data: ' + JSON.stringify( locationsModel.getData() ) );


            //alert('get data');
            //let locationsModel = new JSONModel("model/store_locations.json");
            // ??? not sure why we are not using .getView() here but it works without it only ???
            this.getOwnerComponent().setModel(locationsModel); // json obj has parent element /locations

            // this does not fire
            locationsModel.attachRequestCompleted(function () {
                alert('attachRequestCompleted. oObjects json obj: ' + JSON.stringify(locationsModel.getData()));
            });
        },

        // master row clicked. will fire onPlaceSelected from setLocation function in detail controller
        // can not change names: listSelected or listItem -- assume ui5 specific to <List
        handleListSelect: function (oEvent) {
            //alert('activate pin info cloud on map');
            //alert('handleListSelect'); // listItem ref StandardListItem
            sap.ui.getCore().getEventBus().publish("listSelected", {
                context: oEvent.getParameter("listItem").getBindingContext()
            });
        },

        findByZip: function (oEvent) {

            var    mParams = oEvent.getParameters();
            for (let property in mParams) {
                console.log("onSelectionChange: " + property  + '. value: ' + mParams[property]);
                // has listItem, and listItems - clone of index value
            }
            let zip =  this.getView().byId("idZip").getValue();
            //console.log('this: ' + this); // this: EventProvider testapp.controller.Master
            //alert('zip: '+ zip);
            this.getJson(zip);

        },

        // this function selects the master list row when a pin is clicked
        onPlaceSelected: function (sChannelId, sEventId, oData) {
            //  alert('onPlaceSelected. oData: ' + JSON.stringify(oData));            // alert(Object.keys(oData).length);            // oData.store_name + oData.address  + oData.city + ', ' + oData.state, oData.zip
            if (oData !== undefined && Object.keys(oData).length >= 1) {
                //alert('step1');
                console.log('onPlaceSelected. oData: ' +  JSON.stringify( oData) );
                //console.log('onPlaceSelected. oData length: ' + Object.keys(oData).length);
                var that = this;


// this will highlight the master row when a pin is clicked
                this.list.getItems().forEach(function (item) {
                    ////alert(item.getDescription());                    alert(item.getId());
                    //console.log('getActiveIcon:' + item.getActiveIcon() + '. oData.location.id:' + oData.location.id + '.png');
                    //console.log('master item properties: ' + item);  // mProperties has title for the item
                    //console.log('find master row. ' + item.getTitle() + ' =  ' + oData.location.store_name);
                    //console.log('find master row. ' + Number(item.getInfo()) + ' =  ' + oData.location.id);
                    //if (item.getTitle() === oData.location.store_name  && that.list.getSelectedItem() !== item) {
                    //if (Number(item.getInfo()) === oData.location.id  && that.list.getSelectedItem() !== item) {

                    // todo but back  && that.list.getSelectedItem() !== item
                    if (item.getActiveIcon() === oData.location.id + '.png') {
                        //alert('found');
                        console.log('found: ' + oData.location.store_name + ' ' + oData.location.city);
                        that.list.setSelectedItem(item, true);
                    }
                });
            }
        }

    });
}, true);