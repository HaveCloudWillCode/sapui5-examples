sap.ui.define([
    "sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel"
], function(UIComponent,JSONModel) {
    "use strict";
    return UIComponent.extend("testapp.Component", {
        init: function() {
            //alert('app2 comp init');

            // W3C googlemaps key test
            // window.GMAPS_API_KEY = 'AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM';

            //load googlemaps library
            sap.ui.getCore().loadLibrary("openui5.googlemaps", "../../dist/openui5/googlemaps/");  // api key is in xml



            // this does not work
            //openui5.googlemaps.ScriptsUtil.setApiKey('AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM');



            //register controls library
            //jQuery.sap.registerResourcePath("controls", "../controls"); // ???

            UIComponent.prototype.init.apply(this, arguments);
        },

        createContent: function() {
            // create root view


            var oView = sap.ui.view({
                id: "idViewRoot",
                viewName: "testapp.view.Root",
                type: "XML",
                viewData: {
                    component: this
                }
            });




            //// set data model on root view -- DATA --
            ////oView.setModel(new JSONModel("model/mock.json"));  // un-named global data model
            ////let locationsModel = new JSONModel("model/mock.locations.json");


           // let locationsModel = new JSONModel("model/mock.json");

// http://peak-alt-service.local/slv1/get/storeLocationsByZip/60068/25/42.0085328/-87.84515270000003/117/20\
            //alert('get ajax');
// lng lat data must not be a string - must be numeric (float)
/*
            let jData=null;
            let locationsModel =  new JSONModel();
            jQuery.ajax({
                url: "http://peak-alt-service.local/slv1/get/storeLocationsByZip/60068/25/42.0085328/-87.84515270000003/117/20",
                dataType: "json",
                success: function(data) {
                    console.log(JSON.stringify(data));
                    //alert('ajax resposne: ' + JSON.stringify( data.results ) );
                    jData = data;
                },
                async: false
            });

            console.log('jData: ' +  JSON.stringify( jData.results ) );
            locationsModel.setProperty("/locations", jData.results);
            //alert('get view model data: ' + JSON.stringify( locationsModel.getData() ) );


            //alert('get data');
            //let locationsModel = new JSONModel("model/store_locations.json");
            oView.setModel(locationsModel); // json obj has parent element /locations

            locationsModel.attachRequestCompleted(function () {
                  //alert('attachRequestCompleted. oObjects json obj: ' + JSON.stringify(locationsModel.getData()));
            });

 */

            // set i18n model
            var i18nModel = new sap.ui.model.resource.ResourceModel({
                bundleUrl: "i18n/messageBundle.properties"
            });
            oView.setModel(i18nModel, "i18n");

            // done
            return oView;
        }
    });
});