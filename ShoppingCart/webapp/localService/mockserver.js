sap.ui.define([
	"sap/ui/core/util/MockServer"
], function (MockServer) {
	"use strict";

	var oMockServer,
		_sMetadataPath        = "sap/ui/demo/cart/localService/metadata",
		_sJsonFilesModulePath = "sap/ui/demo/cart/localService/mockdata";

	return {
		/**
		 * Initializes the mock server.
		 * You can configure the delay with the URL parameter "serverDelay".
		 * The local mock data in this folder is returned instead of the real data for testing.
		 * @public
		 */

		init : function () {
			console.log("mockserver.js started");
			// return;  // this will cause no data to be loaded - setup for mockdata only

			var oUriParameters = jQuery.sap.getUriParameters(),
				sJsonFilesUrl = jQuery.sap.getModulePath(_sJsonFilesModulePath),
				sMetadataUrl = jQuery.sap.getModulePath(_sMetadataPath, ".xml");

			oMockServer = new MockServer({
				rootUri : "/sap/opu/odata/IWBEP/EPM_DEVELOPER_SCENARIO_SRV/",
				recordRequests: false
			});

			// configure mock server with a delay of 100ms
			MockServer.config({
				autoRespond : true,
				autoRespondAfter : (oUriParameters.get("serverDelay") || 100)
			});

			oMockServer.simulate(sMetadataUrl, {
				sMockdataBaseUrl : sJsonFilesUrl,
				bGenerateMissingMockData : true
			});

			//alert('mockserver. ' + sJsonFilesUrl + ' ' + sMetadataUrl);

			oMockServer.start();
            //jQuery.sap.log.setLevel((jQuery.sap.log.Level.INFO));
			jQuery.sap.log.info("Running the app with mock data");  // does not work unless level 3
			//alert('log level: ' +  jQuery.sap.log.getLevel() );
            //console.info("Running the app with mock data");
            //jQuery.sap.log.error("Running the app with mock data"); // using default 1 = error
			// set it back to ERROR
            //jQuery.sap.log.setLevel((jQuery.sap.log.Level.ERROR));
            //jQuery.sap.log.debug("Running the app with mock data - debug message");
        }
	};

});

