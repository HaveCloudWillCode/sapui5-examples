// My js set globals
var pathArray = window.location.pathname.split('/');
var base_url = window.location.protocol + "//" + window.location.host + "/" + pathArray[1]; // + "/";

$(document).ready(function () {

   // alert('my js started');

});

$(document).on('click','.sapMStdTileInfo', function(e) {
    e.preventDefault();
    alert("clicked '" + $(this).val() + $(this).text() + "'");
});


// ----------------------- Function Section -----------------------//

/* Get Json Data*/
function getJsonData(oModel, url, oBusy) {
    console.log('getting data from: ' +url);
    oModel.loadData(url);

    oModel.attachRequestCompleted(function(data) {
        console.log("attachRequest start model data: "+ JSON.stringify(oModel.getData()));
        //alert('attachRequest start model data: ' + JSON.stringify(oModel.getData()));
        oBusy.close();
    });
}





// ----------- testing ----------------- //
function getJsonData1() {
    alert('getJsonData1');
    var url = base_url + '/model/data.json';
    //   alert('getJsonData url: '+ url);

    $.ajax({
        dataType: 'json',
        //url: url+'api/getData.php',
        url: url,
        //data: {page:page}
    }).done(function (data) {
        alert('json data 1: ' + JSON.stringify(data));
        // manageRow(data.data);

    }).error(function(data){
        alert('ajax failed: ' + JSON.stringify(data));
    });

}



function getJsonData2(params){
    alert('getJsonData2');
    var url = base_url + '/model/data.json';

    //$.mobile.loading('show');
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        dataType: 'json',
        success: function(data, status, xhr) {
            alert('json data 2: ' + JSON.stringify(data));
           // $.mobile.loading('hide');
           // addViewData(data, params);
        },
        fail: function(xhr, textStatus, errorThrown) {
           // $.mobile.loading('hide');
            alert('ajax fail: ');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('ajax error: ');
            //$.mobile.loading('hide');
            //window.location.href = 'login.html?logout=1';
            //  $("#multi-msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
        }
    });
    return false;
}
