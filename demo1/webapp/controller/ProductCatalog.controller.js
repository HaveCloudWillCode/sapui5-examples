sap.ui.define(["com/owi/customer5/controller/BaseController",
    "sap/ui/core/mvc/Controller", "sap/ui/model/json/JSONModel", "sap/base/i18n/ResourceBundle", "sap/ui/core/routing/History"
], function (BaseController, Controller, JSONModel, ResourceBundle, History) {
    "use strict";

    return BaseController.extend("com.owi.customer5.controller.ProductCatalog", {
        onInit: function (oEvent) {

            try {
                //const uri = "https://www.mocky.io/v2/5c6ae41b33000007397f4dd9?ftpRoot=product_catalog&docName=";
                let owi = ResourceBundle.create({url: "./model/owi.properties", async: false});  // CHANGE BACK TO ../ TO PUSH
                let rootUri = owi.getText("rootUri");
                const uri = rootUri + owi.getText("productCatalog") + "?ftpRoot=productCatalog&docName=";

                let oModel = new JSONModel(uri);
                this.getView().setModel(oModel, 'ftp_docs');  // named model
                //let oList = this.byId("listId");
                //oList.setModel(oModel); // bind model to view object
                // test only:
                //oModel.attachRequestCompleted(function () {
                //  alert('attachRequestCompleted json obj: ' + JSON.stringify(oModel.getData()));
                //});
                /*
                let uiModel = new JSONModel();
                this.getView().setModel(uiModel, 'uiModel');
                uiModel.setProperty("/webservice_uri", uri);
                */

            } catch (e) {
                alert('error ' + e);
            }
        },

        handlePress: function (oEvent) {
            try {
                // alert('item pressed: ' + oEvent.getSource().getText());    alert("handlePress." + this.getView().getModel("uiModel").getProperty("/webservice_uri")); // this does not work
                //let sSrc = oEvent.getSource().getTarget();  //let uri = this.getView().getModel("uiModel").getProperty("/webservice_uri") +  oEvent.getSource().getText();
                let owi = ResourceBundle.create({url: "../model/owi.properties", async: false});
                let rootUri = owi.getText("rootUri");
                const fileName = oEvent.getSource().getText();
                const uri = rootUri + owi.getText("productCatalog") + "?ftpRoot=productCatalog&docName=" + fileName;
                const fileExt =  fileName.split(".").pop().toLowerCase();

                $.ajax({
                    url: uri,
                  //  dataType: 'binary',
                   // headers:{'Content-Type':'application/pdf',
                   //     'X-Requested-With':'XMLHttpRequest',
                  //  },
                    success: function (response,status,xhr) {
                        //alert('fileExt: ' + fileExt);

                        switch(fileExt) {
                            case "txt":
                                $(window.open().document.body).html(response);
                                break;
                            case "pdf":
                                //alert('pdf it');
                                window.open("data:application/pdf;base64, " + response);
                                break;
                            case "base64":
                                //alert('base64 it');
                                //window.open("data:application/pdf;" + response);
                                window.open("data:application/pdf;base64, " + response);
                                break;
                            default:
                                $(window.open().document.body).html(response);
                        }
                    }
                });

            } catch (e) {
                alert('press error ' + e);
            }
        },

        // Page back <
        onNavBack: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                //oRouter.navTo("xyz", true);
            }
        }
        //
    });
});



