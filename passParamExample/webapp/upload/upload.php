<?php
/**
 * Created by PhpStorm.
 * User: bsteiner
 * Date: 10/18/18
 * Time: 4:34 PM
 */

// upload.php

/*
 response:{"myFileUpload1":{"name":"IMG_7810.jpg","type":"image\/jpeg","tmp_name":"\/tmp\/phpxoaQV6","error":0,"size":90112}}<div style="display: none;"></div>
 */

$target_dir = "images/";
$uploaded_file = $_FILES["myFileUpload1"];
$file = basename($_FILES["myFileUpload1"]["name"]);
$tmp_name = $_FILES["myFileUpload1"]["tmp_name"];
$output = '';

$timestamp= date("Y-m-d h:i:sa");
error_log("\n $timestamp: SapUi5 Upload web service started. \n", 3, './sapui5_error.log');

/* if this does not work - check permission on log file and also upload dir
-- should be 664
stat -c "%a %n"  /var/log/apache2/sapui5_error.log
sudo chmod 664 /var/log/apache2/sapui5_error.log


ls /var/www/sapUI5/b2c/nightvision/webapp -al
-- check owner of upload dir
ls -al
-- +++++++++++ this is mandatory to upload an image - permission +++++++++++++
sudo chown -R www-data:www-data upload
-- make sure upload owner is www-data


to debug add this code above 1st error_log stmt

error_reporting(E_ALL); // Error engine - always ON!
ini_set('display_errors', TRUE); // Error display - OFF in production env or real server
ini_set('log_errors', TRUE); // Error logging
*/

if (empty($file)) {
    $file = basename($_FILES["myFileUpload2"]["name"]);
    $tmp_name = $_FILES["myFileUpload2"]["tmp_name"];
}

$uploadOk = 1;
$imageFileType = strtolower(pathinfo($file, PATHINFO_EXTENSION));

// give file a unique name extension
if (empty($file)) {
    $output .= "No Image file selected for upload. ";
    $uploadOk = 0;
} else {
    $uuid = uniqid();
    $file = str_replace(".{$imageFileType}", "_id{$uuid}.$imageFileType", $file);
    $target_file = $target_dir . $file;
}

// Check if image file is a actual image or fake image
//if(isset($_POST["submit"])) {
$check = getimagesize($tmp_name);
if ($uploadOk) {
    if ($check !== false) {
        $msg = "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $output .= "File is not an image. ";
        $uploadOk = 0;
    }
}
//}

// Check if file already exists
if ($uploadOk And file_exists($target_file)) {
    $output .= "Sorry, file '$file' already exists. ";
    $uploadOk = 0;
}
// Check file size - set here at 1 meg (1,000,000 bytes). changed to 10 meg (10000000)
if ($uploadOk And $uploaded_file["size"] > 10000000) {
    $output .= "Sorry, your file '$file' is too large (>10meg). ";
    $uploadOk = 0;
}
// Allow certain file formats
if ($uploadOk And $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif") {
    $output .= "Sorry, only JPG, JPEG, PNG & GIF files are allowed. ";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $output .= "Sorry, your file was not uploaded. ";
// if everything is ok, try to upload file
} else {
    //error_reporting(E_ALL);    ini_set('display_errors',1);
    //>//$test1 = $_FILES["myFileUpload1"]["tmp_name"];    //$test2 = $target_file;    //$test3 = getcwd();
    //>//$status = move_uploaded_file($_FILES["myFileUpload1"]["tmp_name"], $target_file);
    /* any issues:
    -- +++++++++++ this is mandatory to upload an image - permission on upload dest dir +++++++++++++
        sudo chown -R www-data:www-data upload
    */
    if (move_uploaded_file($tmp_name, $target_file)) {
        $output .= "The file '$file' has been uploaded. ";
    } else {
        $output .= "Sorry, there was an error uploading your file: $file. ";
    }
}

error_log("sapui5 upload webservice output: $output \n", 3, './sapui5_error.log');

$json_response = json_encode(array('newFileName' => $file, 'message' => $output, 'uploadOk' => $uploadOk));
echo $json_response;
//return;