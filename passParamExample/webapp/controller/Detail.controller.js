sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function (Controller, History) {
	"use strict";

	return Controller.extend("sap.ui.demo.wt.controller.Detail", {

		onInit: function () {
			alert('**Detail controller onInit');

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
<<<<<<< HEAD
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);  // <<<<<<<<<< secret param2 'this' >>>>>>>>>>>>>>>>>>>>
=======
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);  // <<<<<<<<<< secret param 'this' >>>>>>>>>>>>>>>>>>>>
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e
			// calls func below and *** passes this obj ???????
			// NOTE: if this is not passed and secret param 2 to func below then you can not use this in that func
		},

<<<<<<< HEAD
		// this will get the row index passed (arguments) as param1 to the comp model named: invoices. path is row index of json obj
		// invoicePath is the name of parm1 passed from InvoiceList::onPress
		// NOTE: 'this' must be passed to below as func parm 2 !!!
		// manifest route entry has param entry to accept below passed param (...."pattern": "detail/{invoicePath}",...)
		// names view model = invoices
=======
		// this will get the row index passed as param1 to the comp model named: invoices. path is row index of json obj
		// invoicePath is the name of parm1 passed from InvoiceList::onPress
		// NOTE: 'this' must be passed to below as func parm 2 !!!
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e
		_onObjectMatched: function (oEvent) {
			this.getView().bindElement({
				path: "/" + oEvent.getParameter("arguments").invoicePath,
				model: "invoices"
			});
		},
		onAfterRendering() {
<<<<<<< HEAD
			alert("onAfterRendering");
=======
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e
            // feed Rule Name to Page input field
		 	this.getView().byId("ruleName").setValue(getRuleName(data,{filter1:filter1, filter2:filter2}));
		},

		//
		onAccept: function() {
<<<<<<< HEAD
			alert("onAccept" + this.getView().byId("ruleName"));
=======
			//alert(this.getView().byId("ruleName"));
>>>>>>> 447480e7793133e5e5bd0cf2ea99da5fe2dcdd4e
			var value = this.getView().byId("ruleName").getValue();  // use this since input not bound to model property
			alert("Accepted value: " +  value);

		},
		// Page back <
		onNavBack: function () {
			alert('on nav back fired');
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				//oRouter.navTo("xyz", true);
			}
		}
		// ---------------------------------------------------------------------------------------------------------------------- //
	});
});
