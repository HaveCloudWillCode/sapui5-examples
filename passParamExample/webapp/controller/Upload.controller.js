sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function (Controller, MessageToast) {
	"use strict";


	return Controller.extend("sap.ui.demo.wt.controller.Upload", {

		onInit: function () {
		//	alert('Upload onInit');
		},

		handleUploadComplete: function (oEvent) {
			// alert("function handleUploadComplete");			 alert('event: ' + oEvent.getSource()); // event: Element sap.ui.unified.FileUploader#__component0---app--idImageReceipt
			var params = oEvent.getParameters();
			console.log('params: ' + JSON.stringify(params));
			var response = oEvent.getParameter("response");
			//var event = oEvent.getSource();
			//var words = event.getId().split("app--");
			 console.log('response: ' + response);
			var n = response.lastIndexOf("<div"); // for some reason this gets added by ui5: '<div style="display: none;"></div>'
			//alert("n " + n + ". "+  response.substring(0, n + 1)); alert("2 " +response.substring(0, n - 1)); alert("3 " +response.substring(0, n));
			if (n !== -1){
				response = response.substring(0, n);
			}
			var data   = JSON.parse(response);			//alert ('data: ' +  JSON.stringify(data)); alert('message: ' + data.message + '. new file name: ' + data.newFileName);
			MessageToast.show(data.message);

			var i18nModel = this.getOwnerComponent().getModel("i18n");
			var savedImageDirectory = i18nModel.getResourceBundle().getText("savedImageDirectory");
			var pathArray = window.location.pathname.split('/');
			var app_url = window.location.protocol + "//" + window.location.host + "/" + pathArray[1] + "/" + pathArray[2] + "/" + pathArray[3];

			//alert('savedImageDirectory: ' + savedImageDirectory);			// alert(app_url + savedImageDirectory + "/" + data.newFileName);
			this.byId("idImageReceiptPic").setSrc(app_url + savedImageDirectory +  "/" + data.newFileName);
		},

		showHidePress:
			function (oEvent) {
				//alert("showHidePress. " + this.byId("idImageReceiptButton").getText());
				let newText = this.byId("idImageReceiptButton").getText()==="Show Uploader" ? "Hide Uploader" : "Show Uploader";
				this.byId("idImageReceiptButton").setText(newText);

				let showHide = this.byId("idUploaderContent").getVisible()===true ? false : true;
				this.byId("idUploaderContent").setVisible(showHide);
			}

		//
	});

});
