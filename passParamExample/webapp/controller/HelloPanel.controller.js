sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function (Controller, MessageToast) {
	"use strict";

	//alert('HelloPanel controller');

	return Controller.extend("sap.ui.demo.wt.controller.HelloPanel", {

		onInit: function () {
			//alert('HelloPanel onInit');

			//********** example of model existence timing ---- comp model not bound to view until after onInit function **************//

			// get component variable
		//	alert('HelloPanel 1: ' + JSON.stringify(this.getOwnerComponent().myComponentVar));   // model not named, i.e. "yourModelName"

			// get component model property
			// can not do this - this.view (component) is not defined (bound) until after onInit function
			//alert('HelloPanel 2: ' +this.getView().getModel().getProperty("/recipient/name"));
			//alert('HelloPanel 2: ' + this.getOwnerComponent().getModel().getProperty("/recipient/name"));

		},

		// button clicked
		onShowHello: function () {

			alert('say hello');

			// read msg from i18n model
			var oBundle = this.getView().getModel("i18n").getResourceBundle();

			// get input value from model set in component. no named model in Component
			var sRecipient = this.getView().getModel().getProperty("/recipient/name");  // pulled from un-named model set in Component
			alert('HelloPanel recipient: ' + sRecipient);

			// build message from i18 template and toast it
			var sMsg = oBundle.getText("helloMsg", [sRecipient]);  // msg i18n templated: helloMsg=Hello {0}, 0 = 1 param passed
			MessageToast.show(sMsg);


		//	alert('step1');
			alert(this.getView().getModel('view'));
			var currency = this.getView().getModel('view').getProperty("/currency");
			alert('currency: ' + currency);

		},

		// fragment example
		onOpenDialog: function () {
			this.getOwnerComponent().helloDialog.open(this.getView());
		}
	});

});
