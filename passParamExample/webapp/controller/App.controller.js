sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	//alert('app controller');

	return Controller.extend("sap.ui.demo.wt.controller.App", {

		onOpenDialog: function () {
			alert('App controller::onOpenDialog. this is: ' +this);
			this.getOwnerComponent().helloDialog.open(this.getView());
		}
	});

});
