sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/demo/wt/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/routing/History"
], function (Controller, JSONModel, formatter, Filter, FilterOperator, History) {
	"use strict";

	return Controller.extend("sap.ui.demo.wt.controller.InvoiceList", {

		formatter: formatter,

		onInit: function () {
		//	alert('InvoiceList Controller');

			// NOTE: invoices json list set in Component: this.setModel(iModel, "invoices"); <<<<<<<<<<<<<<<<

			// due to timing of model binding can not use this.getView().getModel(). Must ref controller directly

			// get component model
			let componentModel = this.getOwnerComponent().getModel();  // model not named, i.e. getModel("yourModelName")
		//	alert("InvoiceList comp default model data: "+  JSON.stringify(componentModel.getData()));  // {"recipient":{"name":"World"}}

			let testModel = this.getOwnerComponent().getModel("testModel");
		//	alert("testModel data: "+  JSON.stringify(testModel.getData()));  // testModel data: {"params":{"test1":"12345","test2":"abcde"}}
		//	alert("testModel data test2: "+   testModel.getData().params.test2);  // testModel data: {"params":{"test1":"12345","test2":"abcde"}}


			// get component model property
			//alert(this.getView().getModel().getProperty("/recipient/name"));  // can not do this - not this.view is not defined
		//	alert("InvoiceList 2: "+ this.getOwnerComponent().getModel().getProperty("/recipient/name"));  // pulled from model set in Component

			// bind Named model to view--- model name: "view"
			let oViewModel = new JSONModel({
				currency: "EUR"
			});
			this.getView().setModel(oViewModel, "view");  // bind "view" model to this controller's view.

			// get component variable
		//	alert("InvoiceList 3: "+ JSON.stringify(this.getOwnerComponent().myComponentVar));   // model not named, i.e. "yourModelName"

			// example -- add property after model set. Add var from comp model to this model "view"
			this.getView().getModel("view").setProperty("/var1",this.getOwnerComponent().myComponentVar);

			// get invoice json data model
		//	alert("InvoiceList json data (empty since not bound yet): "+  JSON.stringify(this.getOwnerComponent().getModel("invoices").getData()));


		},

		onFilterInvoices: function (oEvent) {
            alert('onFilterInvoices');

			let mParams = oEvent.getParameters();
			for (let property1 in mParams) {
				console.log(" item 2: " + property1  + '. value: ' + mParams[property1]);
				/*
				 	item 2: query. value: milk
 					item 2: refreshButtonPressed. value: false
 					item 2: clearButtonPressed. value: false
 					item 2: id. value: __field0
				 */
			}

			// build filter array
			let aFilter = [];
			let sQuery = oEvent.getParameter("query");
			if (sQuery) {
				aFilter.push(new Filter("ProductName", FilterOperator.Contains, sQuery));
			}

			// filter binding
			let oList = this.getView().byId("invoiceList");  // Html List id="invoiceList"
			let oBinding = oList.getBinding("items");  // child items element
			oBinding.filter(aFilter);  // apply filter
		},

		onPress: function (oEvent) {
			 alert("InvoiceList::onPress route to detail page. recipient: " + this.getView().getModel().getProperty("/recipient/name"));

			let oItem = oEvent.getSource();
			let oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			let item_path = oItem.getBindingContext("invoices").getPath();

			alert ('passing 1 param invoice item path/index: (' + item_path + ') to detail page/controller');

			let oModel = oEvent.getSource().getBindingContext("invoices").oModel;
			alert(JSON.stringify( oModel.getProperty(item_path) ));  // getProperty(oEvent.getSource().getBindingContext("invoices").sPath)


			alert('row data: ' + JSON.stringify(oEvent.getSource().getBindingContext("invoices").oModel.getProperty(oEvent.getSource().getBindingContext("invoices").sPath)));
			jQuery.sap.log.info('object element name/index: ' + oEvent.getSource().getBindingContext("invoices").sPath);
			alert(oEvent.getSource().getBindingContext("invoices").sPath); // json row / element index


            alert('passing parameter invoicePath: ' + oItem.getBindingContext("invoices").getPath().substr(1));
            // manifest route entry has param entry to accept below passed param (...."pattern": "detail/{invoicePath}",...)
			// passes ie.: invoicePath: 2

			oRouter.navTo("detail", {
				invoicePath: oItem.getBindingContext("invoices").getPath().substr(1)
			});
		},

		// Page back <
		onNavBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				//oRouter.navTo("xyz", true);
			}
		}
		// ---------------------------------------------------------------------------------------------------------------------- //
	});

});
