

/* add local fragment object 'HelloDialog' to this callback
* then other controllers can access using:
*      this.getOwnerComponent().helloDialog.open(this.getView());
* */

sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel",
	"sap/ui/demo/wt/controller/HelloDialog",
	"sap/base/Log"
], function (UIComponent, JSONModel, HelloDialog, Log) {
	"use strict";

	return UIComponent.extend("sap.ui.demo.wt.Component", {

		metadata: {
			manifest: "json"
		},

		init: function () {
		//	alert('Component init');

			// call the init function of the parent
			UIComponent.prototype.init.apply(this, arguments);

			// set data model (this is a global/component model)
			var oData = {
				recipient: {
					name: "World"
				}
			};
			var oModel = new JSONModel(oData);
			this.setModel(oModel);                    // binding un-named model to global component

			// set named model example
			var testData = {
				params: {
					test1: "12345",
					test2: "abcde"
				}
			};
			var testModel = new JSONModel(testData);
			this.setModel(testModel, "testModel");


			// example of setting a component variable
			this.myComponentVar = 'Comp test var';

			// disable batch grouping for v2 API of the northwind service
			//this.getModel("invoice").setUseBatch(false);

			//alert('get invoice data');
			// walkthru has Invoices setup to be a model with no name - as ref in the xml as Invoices - which is the name of the json file.
			var iModel = new JSONModel("http://ui5.devlocal.com/examples/passParamExample/webapp/test/service/Invoices.json");
			this.setModel(iModel, "invoices");

			// testing to make sure json is being pulled
			iModel.attachRequestCompleted(function () {
				var data = JSON.stringify(iModel.getData());
				console.log('Component iModel. attachRequestCompleted json obj 1: ' + JSON.stringify(iModel.getData()));
				 var data_json = JSON.parse(data); alert('****Component iModel. attachRequestCompleted json obj 2: ' + JSON.stringify(data_json));
			});

			// set dialog  --- Fragment. Bind frag to this component -- just like any global/object variable can be
			this.helloDialog = new HelloDialog();

			// create the views based on the url/hash
			//alert('create route views');
			//Log.setlevel = "All";
			//Log.info("Log Info TEST 1"); Log.error("Log Error TEST 2"); Log.debug("Log Debug TEST 3");
			// manifest loads Overview view first which loads HelloPanel and InvoiceList views.
			this.getRouter().initialize();



			// open support window (only for demonstration purpose)
/* Debugger
			if (sap.ui.Device.system.desktop) {
				setTimeout(function () {
					Log.info("opening support window");
					sap.ui.require(["sap/ui/core/support/Support"], function (Support) {
						var oSupport = Support.getStub("APPLICATION");
						oSupport.openSupportTool();
					});
				}, 3000);
			}
*/


		}
	});

});
