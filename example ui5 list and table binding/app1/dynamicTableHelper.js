$(document).ready(function () {

    // alert('my js helper started');

});

function testMessage(message) {
  alert("helper test message: " + message);
}

function _updateTable (oTable, oTableModel, tableMetaData) {
    //alert('helper class. _updateTable. ' );
    // build table columns
    var aColumns = this._createTableColumns(tableMetaData.columnLabelTexts);

    //jQuery.sap.log.debug("DEBUG: message");
    // table passed in for Table container from view
    for (var i = 0; i < aColumns.length; i++) {
        //console.log(aColumns[i]);
        oTable.addColumn(aColumns[i]);
    }

    // build table rows of data         // MobileLibrary.ListType.Active: Active         // type: MobileLibrary.ListType.Active,
    var oTableTemplate = new sap.m.ColumnListItem({
        type: "Active",
        cells: this._createLabels(tableMetaData.templateCellLabelTexts)  // this creates the cell fields with variable {} values to be feed from data object
    });

    // table passed in for Table container from view. bind data to {template variables}
    console.log('table data path: ' + tableMetaData.itemBindingPath);  // table data path: /businessData
    oTable.bindItems(tableMetaData.itemBindingPath, oTableTemplate);  // /businessData from Data2.json, data row template
    oTable.setModel(oTableModel);
}

/**
 * Creates table columns with labels as headers.
 *
 * @private
 * @param {String[]} labels Column labels
 * @returns {sap.m.Column[]} Array of columns
 */
function _createTableColumns (labels) {
    console.log("_createTableColumns. labels: " + labels);
    var aLabels = this._createLabels(labels);
    return this._createControls(sap.m.Column, "header", aLabels);
}

/**
 * Creates label control array with the specified texts.
 *
 * @private
 * @param {String[]} labelTexts text array
 * @returns {sap.m.Column[]} Array of columns
 */
function _createLabels (labelTexts) {
    console.log("_createLabels. labelTexts: " + labelTexts);
    return this._createControls(sap.m.Label, "text", labelTexts);
}

/**
 * Creates an array of controls with the specified control type, property name and value.
 *
 * @private
 * @param {sap.ui.core.Control} Control Control type to create
 * @param {String} prop Property name
 * @param {Array} propValues Value of the control's property
 * @returns {sap.ui.core.Control[]} array of the new controls
 */
function _createControls (Control, prop, propValues) {
    console.log("_createControls");
    var aControls = [];
    var oProps = {};
    for (var i = 0; i < propValues.length; i++) {
        oProps[prop] = propValues[i];
        aControls.push(new Control(oProps));
    }
    return aControls;
}