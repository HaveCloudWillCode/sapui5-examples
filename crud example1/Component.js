sap.ui.define([
    "./controller/OrganizationDialog",
    "./model/models",
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel"
], function (OrganizationDialog, models, UIComponent, Device, JSONModel, ResourceModel) {
    "use strict";

    return UIComponent.extend("OrganizationList.Component", {

        metadata: {
            manifest: "json"
        },

        /**
         * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
         * @public
         * @override
         */
        init: function () {
            // call the base component's init function
            UIComponent.prototype.init.apply(this, arguments);

            // set the device model
            this.setModel(models.createDeviceModel(), "device");

            // set dialog
        //    this._organizationDialog = new OrganizationDialog(this.getRootControl());

            this.data = {
                students: [
                    {
                        "GUID": "Build\u0192orce Digital",
                        "PRT_VersionNumber": 0,
                        fullName: "Build\u0192orce Digital i.o.",
                        languageTag: "EN",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": "https://docs.google.com/drawings/d/e/2PACX-1vRrTAqVqWwwA6ffWGd_h6CqSSIu-DtbhPEHZPz-r1mS1Oi0IsRgzWW2ZcXKFtg60cY1xYKKSSeRYAii/pub?w=336&h=336",
                        birthDay: null,
                        coc: "00000000",
                        vatNumber: "NL123456789B01"
                    }, {
                        "GUID": "WH&FF",
                        "PRT_VersionNumber": 0,
                        fullName: "Whale herding' and fancy footwork",
                        languageTag: "ENL",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": "sap-icon://crm-sales",
                        birthDay: null,
                        coc: "00000000",
                        vatNumber: "NL123456789B01"
                    }, {
                        "GUID": "T\u00dcV Nederland",
                        "PRT_VersionNumber": 0,
                        fullName: "T\u00dcV Nederland QA bv",
                        languageTag: "EN",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": "https://www.tuv.nl/Resources/Public/Media/TuevNederlandLogo.svg",
                        birthDay: null,
                        coc: "17091102",
                        vatNumber: "NL008440116B01"
                    }, {
                        "GUID": "Heijmans Infra",
                        "PRT_VersionNumber": 0,
                        fullName: "Heijmans Infra bv",
                        languageTag: "EN",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": "https://www.heijmans.nl/static/img/logo.png",
                        birthDay: null,
                        coc: "17104126",
                        vatNumber: "NL007837185B01"
                    }, {
                        "GUID": "GreenTrak",
                        "PRT_VersionNumber": 0,
                        fullName: "GreenTrak i.o",
                        languageTag: "EN",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": "https://docs.google.com/drawings/d/e/2PACX-1vQ-KSekpcM5Sb6B_arWdJ1V1HbOSE_7MD5gNFEL_NtRYBrAzANOjMISii0qZRddi1F2blOReweJLRTS/pub?w=345&h=314",
                        birthDay: null,
                        coc: "00000000",
                        vatNumber: "NL123456789B01"
                    }, {
                        "GUID": "Ditum",
                        "PRT_VersionNumber": 0,
                        fullName: "Ditum Digital vof",
                        languageTag: "EN",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": null,
                        birthDay: "2017-10-20",
                        coc: "69574979",
                        vatNumber: "NL857924606B01"
                    }, {
                        "GUID": "Covw Opleidingen",
                        "PRT_VersionNumber": 0,
                        fullName: "Covw Opleidingen bv",
                        languageTag: "EN",
                        "Nationality": "US",
                        remarks: null,
                        "TillDate": null,
                        "imageURL": "http://covw.nl/wp-content/themes/yoo_nano2_wp(1)/images/logo.png",
                        birthDay: null,
                        coc: "14093058",
                        vatNumber: "NL812244771B01"
                    }, {
                        "GUID": "Bricksburg Construction",
                        "PRT_VersionNumber": 0,
                        fullName: "Bricksburg Construction",
                        languageTag: "EN",
                        "Nationality": "EU",
                        remarks: "Last record",
                        "TillDate": null,
                        "imageURL": "https://shop.lego.com/static/images/svg/lego-logo.svg",
                        birthDay: null,
                        coc: "0",
                        vatNumber: "12345"
                    }
                ]
            };
            this.oModel = new sap.ui.model.json.JSONModel(this.data);
            this.setModel(this.oModel);  // un-named model

            // does not work ??? idk, maybe since it is not pulling from a real js file
            this.oModel.attachRequestCompleted(function () {
                alert('attachRequestCompleted. this.oModel json obj: ' + JSON.stringify(this.oModel.getData()));
            });


            var oData = {
                "Selections": [
                    {
                        "ProductId": "HT-1000",
                        "Name": "Notebook Basic 15"
                    },
                    {
                        "ProductId": "HT-1001",
                        "Name": "Notebook Basic 17"
                    },
                    {
                        "ProductId": "HT-1002",
                        "Name": "Notebook Basic 18"
                    },
                    {
                        "ProductId": "HT-1003",
                        "Name": "Notebook Basic 19"
                    },
                    {
                        "ProductId": "HT-1007",
                        "Name": "ITelO Vault"
                    },
                    {
                        "ProductId": "HT-1010",
                        "Name": "Notebook Professional 15"
                    },
                    {
                        "ProductId": "HT-1011",
                        "Name": "Notebook Professional 17"
                    },
                    {
                        "ProductId": "HT-1020",
                        "Name": "ITelO Vault Net"
                    },
                    {
                        "ProductId": "HT-1021",
                        "Name": "ITelO Vault SAT"
                    },
                    {
                        "ProductId": "HT-1022",
                        "Name": "Comfort Easy"
                    },
                    {
                        "ProductId": "HT-1023",
                        "Name": "Comfort Senior"
                    }
                ]
            };


            // set explored app's demo model on this sample
            var selectionsModel = new JSONModel(oData);
            //this.getView().setModel(oModel);  // will break this page data model
            this.setModel(selectionsModel, "SelectionsModel");



            // ??? this does not catch
            selectionsModel.attachRequestCompleted(function () {
                alert('xxxxxx');
        //        sap.ui.core.BusyIndicator.hide();
                 alert('attachRequestCompleted. json obj: ' + JSON.stringify(selectionsModel.getData()));
            });

            /*
                      var dataObject = [
                          {part: "Power Projector 4713", desc: "33"},
                          {part: "Gladiator MX", desc: "33"},
                          {part: "Hurricane GX", desc: "45"},
                          {part: "Webcam", desc: "33"},
                          {part: "Monitor Locking Cable", desc: "41"},
                          {part: "Laptop Case", desc: "64"}];


                      // selections
                      var model = new sap.ui.model.json.JSONModel();
                      model.setData({
                          modelData: {
                              productsData : []
                          }
                      });
                      this.setModel(model, 'selections');
                      this.getModel('selections').setProperty("/modelData/productsData", dataObject);
          //
          */

            this.getRouter().initialize();  // open view
        },

        xxxopenSDialog: function (record) {
            let selections = this.getModel("selections").getData();
           // alert( JSON.stringify( selections ));

            this._organizationDialog.open(record, selections)
        }

    });
});