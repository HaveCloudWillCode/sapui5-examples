sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel"
], function (ManagedObject, Fragment, JSONModel) {
    "use strict";

    return ManagedObject.extend("OrganizationList.controller.OrganizationDialog", {
        constructor: function (oParentView) {
            // alert('oParentView: ' + oParentView);
            this._oParentView = oParentView;

           // alert (this._oParentView.getModel("SelectionsModel").getData() );  // does not work
           // var oModel = this.getOwnerComponent().getModel("SelectionsModel");  // nope
           // alert(oParentView.getView().getData()); // nope
           // alert(oParentView.getData());  //nope


        },

        onInit: function (oEvent) {

alert('onInit');  // ??? never fired

        },


        open: function (record, selections) {

            //alert('open');
         //   alert( JSON.stringify( selections) );

            // gets the fragment xml obj byId - idOrganizationDialog. so oParentView must bind with fragment at .load time
            const oParentView = this._oParentView, theView = oParentView.byId("idOrganizationDialog");

            // gets all the json rows for controller un-named model (students)
            alert(' open. unnamed main model: ' + JSON.stringify(oParentView.getModel().getData()));
            //gets all the json rows for selections model
            alert(' open. selections main model: ' + JSON.stringify(oParentView.getModel('selections').getData()));


            function populateAndOpen(theView, record) {
                function clone(obj) {
                    return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj)
                }

                const temp = clone(record);  // i guess this is due to it being mutable
                temp.target = record;
                theView.setModel(new sap.ui.model.json.JSONModel(temp));


               //  alert( JSON.stringify( selections) );
                var oModel = new JSONModel(selections);
                theView.setModel(oModel, "SelectionsModel");


                // open dialog
                theView.open()
            }

            // Create dialog lazily
            if (!theView) {
                const oFragmentController = {

                    // xml dialog
                    onCloseDialog: function () {
                        oParentView.byId("idOrganizationDialog").close();
                    },
// save
                    onSaveDialog: function (oEvent) {
                        const oParentViewModel = oParentView.getModel();

                        // form data - one row only
                        const oDialogData = oEvent.getSource().getModel().getData();
                        const target = oDialogData.target;

                        if (oDialogData.bNewRecord) {
                           //  alert('New record. ' + JSON.stringify(oDialogData));
                            oDialogData.bNewRecord = false;
                            oParentViewModel.getData().students.push(oDialogData);
                        } else {
                           // alert('Updated record. ' + JSON.stringify(oDialogData));
                            Object.keys(oDialogData).forEach(function (key) {
                                target[key] = oDialogData[key];
                            });

                        }
                        oParentViewModel.setData(oParentViewModel.getData());  // this forces a view refresh to show new values
                        oParentView.byId("idOrganizationDialog").close()
                    } ,

// drop down changed
                    onSelectionChanged: function(oEvent) {

                      //  alert(oEventArgs.getSource().getSelectedItem().getKey());
                        alert(oEvent.getSource().getSelectedItem().getText());

                   //     let productPath = oEventArgs.getSource().getBindingContext("SelectionsModel").getPath(); // productPath: /ProductCollection/94
                    //    let productId = productPath.split("/").slice(-1).pop()
                    //     alert('productId: ' + productId);

                    //    var selectedItem = oEventArgs.getSource().getSelectedItem(); alert(selectedItem);
                      //  var context = selectedItem.getBindingContext("SelectionsModel");
                        // get binding object (reference to an object of the original array)
                      //  var arrayObject = context.oModel.getProperty(context.sPath);
                     //   var value = arrayObject.value;
                     //   alert(JSON.stringify(arrayObject));
                     //   alert(value);  //

                        //let selectId = this.byId("selectId");
                        //let part = this._getSelectedPartJson();
                        //minOrderQty.setText(part.palletQty + " " + part.inventoryUom);
                        //selectId.setValue(SelectionsModel.part);

                     //   var button = oEvent.getSource();
                     //   var context = button.getBindingContext("SelectionsModel"); //context not defined
                     //   var selectId = context.getProperty("selectId");

                      //  alert('selectId: ' + selectId);

                        //let selectId =  oParentView.byId("selectId");  alert(selectId);
                        //selectId.setText(oEventArgs.getSource().getSelectedItem().getText());

                        // this.getView().getModel("SelectionsModel").getProperty("/part");  // not a function

                        const oDialogData = oEvent.getSource().getModel().getData();
                        oDialogData.coc = oEvent.getSource().getSelectedItem().getText();

                        alert('Updated record. ' + JSON.stringify(oDialogData));

                    }

                };

              //  alert( JSON.stringify( selections) );

                // load asynchronous XML fragment
                Fragment.load({
                    id: this._oParentView.getId(),
                    name: "OrganizationList.view.OrganizationDialog",
                    controller: oFragmentController
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    // oParentView.addDependent(oDialog);

                    // connect dialog to the root view of this component (models, lifecycle)
                    oParentView.addDependent(oDialog);
                    //oParentView.open();

                    populateAndOpen(oDialog, record); // this will open dialog
                });
            } else {
                populateAndOpen(theView, record)
            }
        }


    });
});