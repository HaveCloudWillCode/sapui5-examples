sap.ui.define(['sap/m/MessageToast', 'sap/ui/core/mvc/Controller',"sap/ui/model/json/JSONModel"
        ,"../controller/OrganizationDialog"],

    function (MessageToast, Controller, JSONModel, OrganizationDialog) {
        "use strict";

        return Controller.extend("OrganizationList.controller.Main", {

            onInit: function () {
                sap.ui.core.BusyIndicator.show();

            // this works
            // alert( JSON.stringify( this.getView().getModel().getData()) );
            // alert( JSON.stringify( this.getView().getModel("SelectionsModel").getData()) );

                // set dialog. not sure what root control really does but it gets parent view (oParentView)
                //   oParentView: Element sap.ui.core.mvc.XMLView#__component0---app === Main.view.xml
                //   with that the below can get the contoller and data models
                this._organizationDialog = new OrganizationDialog(this.getOwnerComponent().getRootControl());

                // this is cool
                alert('component var this.data: ' + JSON.stringify(this.getOwnerComponent().data));


                var dataObject = [
                    {part: "Power Projector 4713", desc: "33"},
                    {part: "Gladiator MX", desc: "33"},
                    {part: "Hurricane GX", desc: "45"},
                    {part: "Webcam", desc: "33"},
                    {part: "Monitor Locking Cable", desc: "41"},
                    {part: "Laptop Case", desc: "64"}];


                // selections
                var model = new sap.ui.model.json.JSONModel();
                model.setData({
                    modelData: {
                        productsData : []
                    }
                });

                // moved from controller to here - use this.getView()
              //  this.setModel(model, 'selections');
              //  this.getModel('selections').setProperty("/modelData/productsData", dataObject);

                this.getView().setModel(model, 'selections');
                this.getView().getModel('selections').setProperty("/modelData/productsData", dataObject);


            },


            onListSelect : function(event) {
                console.log(event.getSource().getSelectedItem().getKey()); // works ok
                var path = event.getSource().getSelectedItem().getBindingContext().getPath(); // Fails
                console.log("Path=" + path);

                var oModel = sap.ui.getCore().getModel();
                var theName = oModel.getProperty(path);
                console.log("You selected " + theName.lastName);

            },
// Add a new row
            openOrganizationDialog: function () {
                 alert('**openOrganizationDialog. open dialog');

                /* have no idea what this is for --- will have to fix if used as a record template - but xml has placeholders so not really needed?
                const template = Object.freeze({
                    fullName: "?",
                    languageTag: "language",
                    coc: "coc",
                    birthDay: "2019",
                    vatNumber: "0123456",
                    remarks: "Remarks",
                    bNewRecord: true
                });template
                */

                this.getOwnerComponent().openSDialog({bNewRecord: true});
            },
// edit icon fired
            onDetailPress: function (oEvent) {
                 alert('onDetailPress');  // edit icon on item row
                //alert(JSON.stringify(oEvent.getSource().getBindingContext().getObject()));  // row data un-named model/context
                //alert( JSON.stringify(this.getView().getModel().getData()));  // this works - gets all json data

                /* this does not work from Item xml - only when fired from Table xml
                let oItem = oEvent.getParameter("listItem");
                let sPath = oItem.getBindingContext().getPath();  // un-named model/context
                alert(JSON.stringify( this.getView().getModel().getProperty(sPath) ));  // row data ***
                */

                // this is the json row data. pass row to openSDialog component function
                // component.js function
       //         this.getOwnerComponent().openSDialog(oEvent.getSource().getBindingContext().getObject());
// vs. this function
                this.openSDialog(oEvent.getSource().getBindingContext().getObject());
            },


        onItemPressed: function (oEvent) {alert("onItemPressed Pressed")},

        onObjectListItemPress: function (oEvent) {alert("onObjectListItemPress Pressed")},

        onDelete0: function (oEvent) {
            alert('onDelete0');
                oEvent.getSource().removeItem(oEvent.getParameter("listItem"))
        },
// delete
            onDelete: function (oEvent) {
                //alert('onDelete pressed');
                var oList = oEvent.getSource(),
                    oItem = oEvent.getParameter("listItem"),
                    sPath = oItem.getBindingContext().getPath();  // un-named model/context

                 alert('json row index/sPath: ' + sPath);
                //alert('oItem: ' + oItem);

                //  this does not work when fired from Table xml object
               //// this.getOwnerComponent().openSDialog(oEvent.getSource().getBindingContext().getObject());
               // this does not work from Table xml object
               //// alert(oEvent.getSource().getBindingContext().getObject());

                // after deletion put the focus back to the list
                oList.attachEventOnce("updateFinished", oList.focus, oList);

              // alert( JSON.stringify(this.getView().getModel().getData()));  // this works gets all data
                let oModel = this.getView().getModel();
                //alert ('passing 1 param invoice item path/index: (' + sPath + ') to detail page/controller');
                //alert(JSON.stringify( oModel.getProperty(sPath) ));  // row data ***
                let row_data = oModel.getProperty(sPath);

                // let oCurrentOrganization = oEvent.getSource().getBindingContext().getObject();
             //   let oCurrentOrganization = oItem.getBindingContext().getPath().getObject();
                let oParentViewModel = this.getView().getModel();//oEvent.getSource().getBindingContext().getModel("oModel");// this.oModel,
                let oParentViewData = oParentViewModel.getData();
                oParentViewData.students = oParentViewData.students.filter(row => row !== row_data);
                oParentViewModel.setData(oParentViewData);

               // this.getView().getModel().remove(sPath);  // is not a function
                // send a delete request to the odata service
              //  this.oModel.remove(sPath);  does not work
               // this.getView().getModel().remove(sPath);  does not work
               // let oParentViewModel = oEvent.getSource().getBindingContext().getModel("oModel");
               // oParentViewModel.remove(sPath);
               // this.onDeleteOrganization(oEvent);  // does not work - just dont use table delete - use item delete
            },

            onDelete2: function (oEvent) {
                alert('onDelete2');
                oEvent.getSource().removeItem(oEvent.getParameter("listItem"))
            },

            onDelete1: function (oEvent) {
                alert('onDelete1');
                const msg = 'Deze ronde knop moet de regel wissen maar dat gaat met problemen. Het prullenbakje werk wel.';
                MessageToast.show(msg);
            },

            onDeleteOrganization: function (oEvent) {
                //alert('onDeleteOrganization pressed');
                //alert( JSON.stringify( oEvent.getSource().getBindingContext().getModel("xyz").getData()) ); // can be anything ???? if non-named model ??/

              // alert(sap.ui.getCore().getModel("oModel").getData()); // this does not work - same as ..getView()..

                const oCurrentOrganization = oEvent.getSource().getBindingContext().getObject(),
                    oParentViewModel = oEvent.getSource().getBindingContext().getModel("xyz"),// oModel ??? xyz
                    oParentViewData = oParentViewModel.getData();

                //alert('oCurrentOrganization: '+ JSON.stringify( oCurrentOrganization) );  // row data

                //let productPath = oEvent.getSource().getBindingContext("products").getPath(); // productPath: /ProductCollection/94
                //let productId = productPath.split("/").slice(-1).pop()
                // alert('productId: ' + productId);
                // 1) remove json element    --- a bit slower - like by a second
                //delete oParentViewData.ProductCollection[productId];
                //oParentViewModel.setData(oParentViewData); // update model with new json file sans this row


                // this.getModel("oModel").setProperty("/path", null) .setProperty("/path", []);
                oParentViewData.students = oParentViewData.students.filter(row => row !== oCurrentOrganization);
                oParentViewModel.setData(oParentViewData);  // update model with new json file sans this row
            },

            openSDialog: function (record) {
               // alert("openSDialog");
              //  let selections = this.getModel("selections").getData();  // moved from component model - use getView()
                let selections = this.getView().getModel("selections").getData();
               // alert( JSON.stringify( selections ));

                this._organizationDialog.open(record, selections)
            },

            onAfterRendering: function() {
                sap.ui.core.BusyIndicator.hide();
            }
        });
    });