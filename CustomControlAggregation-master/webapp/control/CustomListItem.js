sap.ui.define(["sap/ui/core/Control"], function(Control) {
	"use strict";
	return Control.extend("DemoCustomControl.control.CustomListItem", {
		"metadata": {
			"properties": {
				"href1": "string",
				"docName": "string"
			},
			"events": {}
		},

		init: function() {
		//	alert('CustomListItem::init' );
		},

		// render item call - from items loop in CustomList.js
		renderer: function(oRm, oControl) {
			//alert('CustomListItem::renderer' );  // not in scope: + ' skip: '+ oControl.getExcludeExts()
			oRm.write("<li");
			oRm.writeControlData(oControl);
			oRm.write(">");
			oRm.write("<a");
			//oRm.writeAttributeEscaped("id='"+oControl.getDocName() + "'" +  "href", oControl.getHref1());
			oRm.writeAttributeEscaped("href", oControl.getHref1());
			oRm.write(' target="_blank" ');
			oRm.write(">" + oControl.getDocName());
			oRm.write("</a>");
			oRm.write("</li>");
		},

		onAfterRendering: function(evt) {
		},

		// property setters. bind to associated xml element. uses value in xml element to update this
		setHref1: function(value) {
			//console.log("CustomListItem::setHref1: " + value);
			this.setProperty("href1", value , true);
			return this;
		},

		// bind this property with element in xml view
		setDocName: function(value) {
			this.setProperty("docName", value, true);
			return this;
		}

	});
});