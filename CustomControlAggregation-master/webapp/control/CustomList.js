sap.ui.define(["sap/ui/core/Control",
    "sap/ui/model/json/JSONModel"], function (Control, JSONModel) {
    "use strict";
    return Control.extend("DemoCustomControl.control.CustomList", {
        "metadata": {
            "properties": {
                "ftpRoot": "string",
                "excludeExts": "string",
                //        "items": "sap.ui.model.json.JSONModel"   // only need if pulling value from xml element
            },

            "events": {},

            /* bind control "items" to CustomListItem.js file below. used in xml for items iteration */
            "aggregations": {
                "items": {
                    "type": "DemoCustomControl.control.CustomListItem",
                    "multiple": true,
                    "singularName": "item"
                }
            }
        },

        init: function () {
            // alert('CustomList::init ' + 'getFtpRoot: ' + this.getFtpRoot());  // getters not defined yet
            // this works. Call webservice to get Json data
            //  let oModel = new JSONModel("http://ui5.devlocal.com/examples/CustomControlAggregation-master/webapp/mockdata/webdocs.json");
            //  this.setModel(oModel, 'ftpdocs');  // named model
            // no view object - setting model to this control object (just like with Component.js::init)
        },

        renderer: function (oRm, oControl) {
            //alert('renderer. getExcludeExts: ' + oControl.getExcludeExts());

            oRm.write("<div");
            oRm.writeControlData(oControl);
            oRm.addClass("customlist");
            oRm.writeClasses();
            oRm.write(">");
            //oRm.write("<ul");			oRm.write(">");
            oRm.write("<ul>");
            //  nope   alert('model json: ' + JSON.stringify ( this.getModel('ftp_docs').getData()) );

            let exclude_extension = oControl.getExcludeExts();
            // element items in xml to loop thru. this aggregration of items is bound to control: CustomListItem
            $.each(oControl.getItems(), function (key, value) {
                //console.log('key: ' + key + '.value: ' + value );  If exclude extensions exist then omit those rows.
                if (exclude_extension && oRm.getHTML(value).toLowerCase().indexOf("." + exclude_extension) >= 0) {
                    return;
                }

                oRm.renderControl(value);
            });
            oRm.write("</ul>");
            oRm.write("</div>");
        },

        onAfterRendering: function (evt) {
            //alert('model json: ' + JSON.stringify(this.getModel('ftp_docs').getData()));
            /*

                        let json = this.getModel('ftp_docs').getData();
                        if (json) {
                            $( "#CustomListItemId" ).append("<ul>");
                            $.each(json.ftp_listing, function (key, value) {
                                //console.log("value: " + JSON.stringify (value));
                                console.log(value.hyperlink);
                                //$( "#newListId" ).append( "<li> <a href = " + value.hyperlink + " target=_blank >" + value.docName + " </a> </li>" );

                                //
                                $( "#CustomListItemId" ).append( "<li> <a href = " + value.hyperlink + " target=_blank >" + value.docName + " </a> </li>" );

                            });
                            $( "#CustomListItemId" ).append("</ul>");
                        }
                        */
        },

        setFtpRoot: function (value) {
            //alert("setFtpRoot: " + value  );
            this.setProperty("ftpRoot", value, true);  // now you can use the getter function
            //let oModel = new JSONModel("http://ui5.devlocal.com/examples/CustomControlAggregation-master/webapp/mockdata/" + value + ".json");
            let oModel = new JSONModel("https://www.mocky.io/v2/5c6ae41b33000007397f4dd9?ftpRoot/"+value);
            console.log("https://www.mocky.io/v2/5c6ae41b33000007397f4dd9?ftpRoot/"+value);
            this.setModel(oModel, 'ftp_docs');  // named model
            return this;
        },

        setExcludeExts: function (value) {
            //alert("setExcludeExts: " + value);
            this.setProperty("excludeExts", value, true);
            //alert("getExcludeExts: " + this.getExcludeExts()  + ". getFtpRoot: " + this.getFtpRoot());
            return this;
        }
    });
});