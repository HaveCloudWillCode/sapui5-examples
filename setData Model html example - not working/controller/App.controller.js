sap.ui.getCore().attachInit(function() {

    // define controller
    sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageToast"
    ], function (Controller, MessageToast) {
        "use strict";
        return Controller.extend("com.timodenk.sapui5.example.controller.App", {
            onInit: function() {
                alert('oninit');
                var model = new sap.ui.model.json.JSONModel();
                model.setData({
                    arrayName: [
                        { firstName: "Albert", lastName: "Einstein", age: 76, born: 1879 },
                        { firstName: "Thomas", lastName: "Edison", age: 84, born: 1847 },
                        { firstName: "Neil", lastName: "Armstrong", age: 82, born: 1930 }
                    ]
                });
                this.getView().setModel(model, "modelPath");
            },

            rowClicked: function(oEventArgs) {
                var row = oEventArgs.getSource();
                var context = row.getBindingContext("modelPath");

                // get binding object (reference to an object of the original array)
                var person = context.oModel.getProperty(context.sPath);

                MessageToast.show(person.firstName + " was born in " + person.born + ".")
            }
        });
    });

    // add XML view
    sap.ui.xmlview({
        viewContent: jQuery("#myView").html()
    }).placeAt("content");
});